$(function() {
    var bg = chrome.extension.getBackgroundPage();
    $("#generateBtn").click(function() {
        bg.createAdzone({
            tag: $('#tag').val(),
            gcid: $('#gcid').val(),
            siteId: $("#siteId").val(),
            taobaoUnionId: $("#taobaoUnionId").val()
        })
    })
    $("#batchGenerateBtn").click(function() {
        bg.batchCreateAdzone({
            tag: $('#tag').val(),
            gcid: $('#gcid').val(),
            siteId: $("#siteId").val(),
            total: $("#totalNum").val(),
            taobaoUnionId: $("#taobaoUnionId").val(),
            interval: $("#interval").val()
        }, $("#totalNum").val())
    })

    $("#batchGenerateStopBtn").click(function() {
        bg.batchCreateAdzoneStop()
    })

})

chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.type == 'adzoneCreateMsg') {
            $("#resultMsg").val( request.msg + '\n' + $("#resultMsg").val())
        }
    }
);