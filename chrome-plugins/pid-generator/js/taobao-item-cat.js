$(function () {
    $("#parseCatPicBtn").click(function () {
        chrome.tabs.getSelected(null, function (tab) {
            chrome.tabs.sendMessage(tab.id, {type: "getHtml"}, function (response) {

            });
        });
    })
    $('#clearCatPicBtn').click(function() {
        localStorage.removeItem("taobaoShopcatIconList")
    })
    chrome.extension.onMessage.addListener(
        function (request, sender, sendMessage) {
            chrome.tabs.getSelected(null, function (tab) {
                if (request.type == "getHtml") {
                    var picList = parseTaobaoHtml(request.html)
                    if(tab.url.indexOf('.taobao.com') != -1) {
                        var picList = parseTaobaoHtml(request.html)
                    }
                    if(tab.url.indexOf('.jd.com') != -1) {
                        var picList = parseJdHtml(request.html)
                    }
                    var idx = 0
                    var folder = $('#folder').val()
                    var iconList = JSON.parse(localStorage.getItem("taobaoShopcatIconList") || '[]');
                    for(var i in picList ) {
                        var pic = picList[i]
                        console.info(pic.filename + ' - ' + pic.src)
                        if($.inArray(pic.src, iconList) != -1) {
                            return
                        }
                        chrome.downloads.download({
                            url: pic.src,
                            conflictAction: 'uniquify',
                            saveAs: false,
                            filename: 'shopcat/' + folder + "/" + pic.filename.replace(/\//g, '_')
                        }, function() {
                            iconList.push(pic.src)
                            localStorage.setItem("taobaoShopcatIconList", JSON.stringify(iconList))
                            idx++
                            if(idx == spans.size() - 1) {
                                alert('下载完成')
                            }
                        });
                    }
                }
            });

        });
//branch_
    function parseJdHtml(html) {
        html = $(html)
        var aList = html.find('a[id^="branch_"]')
        var picList = []
        aList.each(function() {
            var a = $(this)
            var img = a.find('img')
            if(img.size() != 1) {
                return
            }
            var src = img.attr('src')
            if(src.startsWith('data:image')) {
                alert('图片未装载完成');
                return false
            }
            // src = src.substring(0, src.indexOf('_140x'))
            if(src.startsWith("//")) {
                src = "http:" + src
            }
            var filename = a.text() + src.substring(src.lastIndexOf("."))
            picList.push({
                filename: filename,
                src: src
            })
        })
        return picList
    }
    function parseTaobaoHtml(html) {
        html = $(html)
        var spans = html.find('span')

        var picList = []
        spans.each(function() {
            var span = $(this)
            var parent = span.parent().parent();
            var img = parent.find('img')
            if(img.size() != 1) {
                return
            }
            var src = img.attr('src')
            if(src.startsWith('data:image')) {
                alert('图片未装载完成');
                return false
            }
            src = src.substring(0, src.indexOf('_140x'))
            if(src.startsWith("//")) {
                src = "http:" + src
            }


            var filename = span.html() + src.substring(src.lastIndexOf("."))
            picList.push({
                filename: filename,
                src: src
            })
        })
        return picList
    }
})