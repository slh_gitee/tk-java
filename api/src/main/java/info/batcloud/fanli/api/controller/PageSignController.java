package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.SignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/page/sign")
public class PageSignController {

    @Inject
    private SignService signService;

    @GetMapping("/data")
    public Object data() {
        return BusinessResponse.ok(signService.findByUserId(SecurityHelper.loginUserId()));
    }

}
