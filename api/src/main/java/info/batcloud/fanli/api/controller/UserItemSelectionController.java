package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.UserItemSelectionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user-item-selection")
public class UserItemSelectionController {

    @Inject
    private UserItemSelectionService userItemSelectionService;

    @PostMapping()
    @PreAuthorize("hasRole('USER')")
    public Object add(UserItemSelectionService.ItemSelectionCreateParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        long id = userItemSelectionService.createUserItemSelection(param);
        return BusinessResponse.ok(id);
    }

    @GetMapping("/share/{id}")
    @PreAuthorize("hasRole('USER')")
    public Object share(@PathVariable long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("userId", SecurityHelper.loginUserId());
        userItemSelectionService.addShareTimes(id, 1);
        return BusinessResponse.ok(map);
    }

    @GetMapping("/recommendSearch")
    public Object recommendSearch(UserItemSelectionService.SearchParam param) {
        return BusinessResponse.ok(userItemSelectionService.search(param));
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('USER')")
    public Object search(UserItemSelectionService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(userItemSelectionService.search(param));
    }

    @DeleteMapping("/{id}")
    public Object deleteById(@PathVariable long id) {
        userItemSelectionService.deleteById(SecurityHelper.loginUserId(), id);
        return BusinessResponse.ok(true);
    }
}
