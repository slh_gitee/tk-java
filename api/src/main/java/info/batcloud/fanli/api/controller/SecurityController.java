package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/security")
public class SecurityController {

    @GetMapping("/current")
    @PreAuthorize("hasRole('USER')")
    public Object current() {

        Map<String, Object> map = new HashMap<>();
        map.put("userId", SecurityHelper.loginUserId());
        return BusinessResponse.ok(map);

    }

}
