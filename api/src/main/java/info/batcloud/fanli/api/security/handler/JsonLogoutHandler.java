package info.batcloud.fanli.api.security.handler;

import com.alibaba.fastjson.JSON;
import info.batcloud.fanli.core.domain.BusinessResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JsonLogoutHandler implements LogoutHandler {
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(JSON.toJSONString(BusinessResponse.ok(true)));
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
