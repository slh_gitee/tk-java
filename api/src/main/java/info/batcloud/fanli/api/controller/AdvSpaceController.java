package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.AdvSpaceService;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/adv-space")
public class AdvSpaceController {

    @Inject
    private AdvSpaceService advSpaceService;

    @GetMapping("")
    public Object info(Form form) {
        Map<String, Object> model = new HashMap<>();
        if (form.getId() != null) {
            model.put("advList", advSpaceService.advListByAdvSpaceId(form.getId()));
        }
        if (StringUtils.isNotBlank(form.getCode())) {
            model.put("advList", advSpaceService.advListByAdvSpaceCode(form.getCode()));
        }
        return BusinessResponse.ok(model);
    }

    public static class Form {
        private Long id;
        private String code;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }
    }

}

