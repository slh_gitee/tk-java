package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.domain.MaterialConfig;
import info.batcloud.fanli.core.entity.Material;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import info.batcloud.fanli.core.service.MaterialService;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/material")
public class MaterialController {

    @Inject
    private MaterialService materialService;

    @Inject
    private CommissionItemSearchService commissionItemSearchService;

    @GetMapping("/item/{id}/{page}")
    public Object itemSearch(@PathVariable long id, @PathVariable int page) {
        Material material = materialService.findById(id);
        CommissionItemSearchService.SearchParam param = new CommissionItemSearchService.SearchParam();
        MaterialConfig config = material.getConfig();
        BeanUtils.copyProperties(config, param);
        param.setPage(page);
        return BusinessResponse.ok(commissionItemSearchService.search(param));
    }

}
