package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserService;
import info.batcloud.fanli.core.settings.InvitationGallerySetting;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/invite-user")
@PreAuthorize("hasRole('USER')")
public class PageInviteUserController {

    @Inject
    private UserService userService;

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        map.put("invitationCode", userService.findUserInvitationCode(SecurityHelper.loginUserId()));
        map.put("picList", systemSettingService.findActiveSetting(InvitationGallerySetting.class).getPicList());
        return BusinessResponse.ok(map);
    }

}
