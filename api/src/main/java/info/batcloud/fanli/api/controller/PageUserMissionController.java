package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.UserMissionDTO;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.UserMissionService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/page/user-mission")
@PreAuthorize("hasRole('USER')")
public class PageUserMissionController {

    @Inject
    private UserMissionService userMissionService;

    @GetMapping("/data")
    public Object data() {

        long userId = SecurityHelper.loginUserId();
        List<UserMissionDTO> missionDtoList
                = userMissionService.findInProgressOrCompleteInDay(userId, new Date());
        Map<String, Object> rs = new HashMap<>();
        rs.put("userMissionList", missionDtoList);
        return BusinessResponse.ok(rs);

    }

}
