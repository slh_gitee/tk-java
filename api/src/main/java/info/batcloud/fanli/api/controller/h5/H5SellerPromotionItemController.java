package info.batcloud.fanli.api.controller.h5;

import info.batcloud.fanli.core.dto.SellerPromotionItemDTO;
import info.batcloud.fanli.core.helper.StringHelper;
import info.batcloud.fanli.core.service.SellerPromotionItemService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Inject;

@Controller
@RequestMapping("/h5/seller-promotion-item")
public class H5SellerPromotionItemController {

    @Inject
    private SellerPromotionItemService sellerPromotionItemService;

    @GetMapping("/search-buy/{id}")
    public String searchBuy(@PathVariable long id, ModelMap modelMap) {
        SellerPromotionItemDTO item = sellerPromotionItemService.findById(id);
        modelMap.put("item", item);
        modelMap.put("shopTitle", StringHelper.protect(item.getShopTitle().trim()));
        modelMap.put("shopTitleLength", item.getShopTitle().trim().length());
        return "/h5/seller-promotion-item-search-buy";
    }

}
