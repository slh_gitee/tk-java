package info.batcloud.fanli.api.domain;

import info.batcloud.fanli.core.domain.Result;

public class LoginResult extends Result {

    private boolean phoneBound;

    private boolean regionBound;

    private boolean weixinBound;

    private boolean superUserBind;

    private String weixinBindOpenId;

    private boolean weixinNeitherRegisterNorBind;

    private Long userId;

    public boolean isWeixinNeitherRegisterNorBind() {
        return weixinNeitherRegisterNorBind;
    }

    public void setWeixinNeitherRegisterNorBind(boolean weixinNeitherRegisterNorBind) {
        this.weixinNeitherRegisterNorBind = weixinNeitherRegisterNorBind;
    }

    public boolean isSuperUserBind() {
        return superUserBind;
    }

    public void setSuperUserBind(boolean superUserBind) {
        this.superUserBind = superUserBind;
    }

    public String getWeixinBindOpenId() {
        return weixinBindOpenId;
    }

    public void setWeixinBindOpenId(String weixinBindOpenId) {
        this.weixinBindOpenId = weixinBindOpenId;
    }

    public boolean isWeixinBound() {
        return weixinBound;
    }

    public void setWeixinBound(boolean weixinBound) {
        this.weixinBound = weixinBound;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isPhoneBound() {
        return phoneBound;
    }

    public void setPhoneBound(boolean phoneBound) {
        this.phoneBound = phoneBound;
    }

    public boolean isRegionBound() {
        return regionBound;
    }

    public void setRegionBound(boolean regionBound) {
        this.regionBound = regionBound;
    }
}
