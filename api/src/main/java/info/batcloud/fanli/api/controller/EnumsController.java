package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.api.controller.vo.EnumVo;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/enums")
public class EnumsController {

    @GetMapping("/ecomPlat")
    public Object ecomPlat() {
        List<EnumVo> list = new ArrayList<>();
        for (EcomPlat ecomPlat : EcomPlat.values()) {
            list.add(new EnumVo(ecomPlat.getTitle(), ecomPlat.name()));
        }
        return BusinessResponse.ok(list);
    }

}
