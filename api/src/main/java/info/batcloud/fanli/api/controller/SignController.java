package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.security.constants.AuthorityConstants;
import info.batcloud.fanli.core.service.SignService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/sign")
public class SignController {

    @Inject
    private SignService signService;

    @PostMapping
    @PreAuthorize("hasRole('USER')")
    public Object sign() {
        return BusinessResponse.ok(signService.userSign(SecurityHelper.loginUserId()));
    }

}
