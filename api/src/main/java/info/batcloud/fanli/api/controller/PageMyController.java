package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.domain.stat.UserCommissionEarning;
import info.batcloud.fanli.core.dto.AgentDTO;
import info.batcloud.fanli.core.dto.UserDTO;
import info.batcloud.fanli.core.dto.WalletDTO;
import info.batcloud.fanli.core.enums.UserLevel;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.mapper.stat.UserCommissionEarningMapper;
import info.batcloud.fanli.core.service.AgentService;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.UserService;
import info.batcloud.fanli.core.service.WalletService;
import info.batcloud.fanli.core.settings.IntegralSetting;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.*;

@RestController
@RequestMapping("/page/my")
public class PageMyController {

    @Inject
    private WalletService walletService;

    @Inject
    private UserService userService;

    @Inject
    private UserCommissionEarningMapper userCommissionEarningMapper;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private AgentService agentService;

    @GetMapping("/data")
    @PreAuthorize("hasRole('USER')")
    public Object data() {
        UserDTO user = userService.findById(SecurityHelper.loginUserId());
        Map<String, Object> model = new HashMap<>();
        Long userId = user.getId();
        WalletDTO wallet = walletService.findByUserId(userId);
        model.put("userNickname", userService.determineUserNickname(user.getNickname()));
        model.put("userInvitationCode", user.getInvitationCode());
        model.put("userAvatar", userService.determineUserAvatar(user.getAvatarUrl()));
        model.put("withdrawMoney", wallet.getWithdrawMoney());
        model.put("obtainedMoney", wallet.getObtainedMoney());
        model.put("integral", wallet.getIntegral());
        model.put("userLevel", user.getLevel());
        model.put("userCityId", user.getCityId());
        model.put("taobaoSpecialId", user.getTaobaoSpecialId());
        UserLevel nextUserLevel = user.getLevel().next;
        if (nextUserLevel == null) {
            nextUserLevel = UserLevel.CHIEF;
        }
        model.put("nextUserLevel", nextUserLevel);
        model.put("nextUserLevelTitle", nextUserLevel == null ? null : nextUserLevel.getTitle());
        model.put("userLevelTitle", user.getLevel().getTitle());
        model.put("directMemberNum", userService.countUserBySuperUserId(SecurityHelper.loginUserId()));
        List<AgentDTO> agentList = agentService.findAgentByUserId(userId);
        model.put("agentList", agentList);

        float todayCommissionOrderNum = 0, todayEstimateCommissionFee = 0,
                todayEstimateRewardFee = 0, monthlyEstimateRewardFee = 0,
                monthlyEstimateCommissionFee = 0, monthlyCommissionOrderNum = 0;
        //统计当天的预估收益
        Date now = new Date();
        Date startTime = DateUtils.truncate(now, Calendar.DATE);
        UserCommissionEarningMapper.UserEstimateStatParam dayEstimateStateParam = new UserCommissionEarningMapper.UserEstimateStatParam();
        dayEstimateStateParam.setUserId(SecurityHelper.loginUserId());
        dayEstimateStateParam.setStartTime(startTime);
        dayEstimateStateParam.setEndTime(now);
        dayEstimateStateParam.setTimeDimension(UserCommissionEarningMapper.TimeDimension.DAY);
        List<UserCommissionEarning> todayUserCommissionEarningList = userCommissionEarningMapper.userCommissionEarning(dayEstimateStateParam);
        if (todayUserCommissionEarningList.size() > 0) {
            UserCommissionEarning userCommissionEarning = todayUserCommissionEarningList.get(0);
            todayCommissionOrderNum = userCommissionEarning.getOrderNum();
            todayEstimateCommissionFee = userCommissionEarning.getEstimateCommissionFee();
            todayEstimateRewardFee = userCommissionEarning.getEstimateRewardFee();
        }
        //统计当月的
        startTime = DateUtils.truncate(now, Calendar.MONTH);
        dayEstimateStateParam = new UserCommissionEarningMapper.UserEstimateStatParam();
        dayEstimateStateParam.setUserId(SecurityHelper.loginUserId());
        dayEstimateStateParam.setStartTime(startTime);
        dayEstimateStateParam.setEndTime(now);
        dayEstimateStateParam.setTimeDimension(UserCommissionEarningMapper.TimeDimension.MONTH);
        List<UserCommissionEarning> monthUserCommissionEarningList = userCommissionEarningMapper.userCommissionEarning(dayEstimateStateParam);
        if (monthUserCommissionEarningList.size() > 0) {
            UserCommissionEarning userCommissionEarning = monthUserCommissionEarningList.get(0);
            monthlyCommissionOrderNum = userCommissionEarning.getOrderNum();
            monthlyEstimateCommissionFee = userCommissionEarning.getEstimateCommissionFee();
            monthlyEstimateRewardFee = userCommissionEarning.getEstimateRewardFee();
        }
        model.put("todayCommissionOrderNum", todayCommissionOrderNum);
        model.put("money", wallet.getMoney());
        model.put("obtainedMoney", wallet.getObtainedMoney());
        model.put("todayEstimateCommissionFee", todayEstimateCommissionFee);
        model.put("todayEstimateRewardFee", todayEstimateRewardFee);
        model.put("monthlyEstimateCommissionFee", monthlyEstimateCommissionFee);
        model.put("monthlyEstimateRewardFee", monthlyEstimateRewardFee);
        model.put("monthlyCommissionOrderNum", monthlyCommissionOrderNum);
        IntegralSetting integralSetting = systemSettingService.findActiveSetting(IntegralSetting.class);
        model.put("inviteEachUserIntegral", integralSetting.getInviteEachUserIntegral());
        //处理用户title
        return BusinessResponse.ok(model);
    }

}
