package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.placeholder.ReplaceContext;
import info.batcloud.fanli.core.service.PlaceholderService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/placeholder")
public class PlaceholderController {

    @Inject
    private PlaceholderService placeholderService;

    @PostMapping("/clean")
    public Object clean(ReplaceContext param) {
        param.setUserId(SecurityHelper.loginUserId());
        String text = placeholderService.clean(param);
        return BusinessResponse.ok(text);
    }

}
