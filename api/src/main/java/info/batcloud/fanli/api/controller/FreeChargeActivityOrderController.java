package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.FreeChargeActivityOrderService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/free-charge-activity-order")
public class FreeChargeActivityOrderController {

    @Inject
    private FreeChargeActivityOrderService freeChargeActivityOrderService;

    @GetMapping("/search")
    public Object search(FreeChargeActivityOrderService.SearchParam param) {
        param.setUserId(SecurityHelper.loginUserId());
        return BusinessResponse.ok(freeChargeActivityOrderService.search(param));
    }


}
