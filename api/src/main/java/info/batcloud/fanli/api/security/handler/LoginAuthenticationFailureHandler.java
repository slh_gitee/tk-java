package info.batcloud.fanli.api.security.handler;

import com.alibaba.fastjson.JSON;
import info.batcloud.fanli.api.domain.LoginResult;
import info.batcloud.fanli.api.exception.WeixinNeitherRegisterNorBindException;
import info.batcloud.fanli.api.security.filter.PhoneLoginAuthenticationProcessingFilter;
import info.batcloud.fanli.core.constants.ExceptionKeyConstants;
import info.batcloud.fanli.core.domain.BusinessResponse;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static info.batcloud.fanli.core.constants.MessageKeyConstants.BAD_CREDENTIALS;

@Component
public class LoginAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        exception.printStackTrace();
        response.setStatus(HttpServletResponse.SC_OK);
        response.setContentType("application/json;charset=UTF-8");
        response.setCharacterEncoding("UTF-8");
        LoginResult loginResult = new LoginResult();
        loginResult.setSuccess(false);
        String code = exception.getLocalizedMessage();
        loginResult.setCode(code);
        if(exception instanceof PhoneLoginAuthenticationProcessingFilter.VerifyCodeNotValidException) {
        } else if(exception instanceof WeixinNeitherRegisterNorBindException) {
            loginResult.setWeixinNeitherRegisterNorBind(true);
        } else if (exception instanceof BadCredentialsException) {
            loginResult.setCode(BAD_CREDENTIALS);
        }

        response.getWriter().write(JSON.toJSONString(BusinessResponse.ok(loginResult)));
        response.getWriter().flush();

    }
}
