package info.batcloud.fanli.api.controller;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.AdvDTO;
import info.batcloud.fanli.core.dto.UserRedPacketDTO;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.enums.AdvSpaceCode;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.service.AdvService;
import info.batcloud.fanli.core.service.AdvSpaceService;
import info.batcloud.fanli.core.service.RedPacketService;
import info.batcloud.fanli.core.service.UserRedPacketService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/home-popup")
public class HomePopupController {

    @Inject
    private RedPacketService redPacketService;

    @Inject
    private UserRedPacketService userRedPacketService;

    @Inject
    private AdvSpaceService advSpaceService;

    @Inject
    private AdvService advService;

    @GetMapping("/data")
    public Object data() {
        Long userId = SecurityHelper.loginUserId();
        Map<String, Object> model = new HashMap<>();
        if (userId != null) {
            redPacketService.grantToUser(userId);
            UserRedPacketService.SearchParam param = new UserRedPacketService.SearchParam();
            param.setUserId(userId);
            param.setDraw(false);
            Paging<UserRedPacketDTO> rs = userRedPacketService.search(param);
            model.put("userRedPacketList", rs.getResults());
        }
        List<AdvDTO> advList = advService.filterByUserId(userId,
                advSpaceService.advListByAdvSpaceCode(AdvSpaceCode.INDEX_POPUP_ADV.name()));
        model.put("advList", advList);
        return BusinessResponse.ok(model);
    }

}
