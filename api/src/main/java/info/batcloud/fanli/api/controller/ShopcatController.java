package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.service.ShopcatService;
import info.batcloud.fanli.core.dto.ShopcatDTO;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/shopcat")
public class ShopcatController {

    @Inject
    private ShopcatService shopcatService;

    @GetMapping("/list/{parentIds}")
    public Object listByParentId(@PathVariable String parentIds,
                                 @RequestParam(defaultValue = "true", required = false) boolean children) {
        List<ShopcatDTO> list = shopcatService.findEnabledByParentIds(parentIds);
        if (list.size() > 0 && children) {
            ShopcatDTO first = list.get(0);
            first.setChildren(shopcatService.findEnabledTreeByPath(first.getId() + ""));
        }
        return BusinessResponse.ok(list);
    }

    @GetMapping("/list/tree")
    public Object listTreeByPath(String path) {
        List<ShopcatDTO> list = shopcatService.findEnabledTreeByPath(path);
        return BusinessResponse.ok(list);
    }

}
