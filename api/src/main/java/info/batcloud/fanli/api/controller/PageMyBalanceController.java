package info.batcloud.fanli.api.controller;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.WalletDTO;
import info.batcloud.fanli.core.entity.UserWithdraw;
import info.batcloud.fanli.core.helper.SecurityHelper;
import info.batcloud.fanli.core.repository.UserWithdrawRepository;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.WalletService;
import info.batcloud.fanli.core.settings.WithdrawSetting;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/page/my-balance")
@PreAuthorize("hasRole('USER')")
public class PageMyBalanceController {

    @Inject
    private WalletService walletService;

    @Inject
    private UserWithdrawRepository userWithdrawRepository;

    @Inject
    private SystemSettingService systemSettingService;

    @GetMapping("/data")
    public Object data() {
        Map<String, Object> map = new HashMap<>();
        WalletDTO wallet = walletService.findByUserId(SecurityHelper.loginUserId());
        map.put("wallet", wallet);
        UserWithdraw userWithdraw = userWithdrawRepository.findTopByUserIdOrderByIdDesc(SecurityHelper.loginUserId());
        if(userWithdraw != null) {
            map.put("payeeAccount", userWithdraw.getPayeeAccount());
            map.put("payeeName", userWithdraw.getPayeeName());
        }
        WithdrawSetting setting = systemSettingService.findActiveSetting(WithdrawSetting.class);
        map.put("openWithdraw", setting.isOpenWithdraw());
        map.put("minWithdrawValue", setting.getMinWithdrawValue());
        return BusinessResponse.ok(map);
    }

}
