# 淘客系统

#### 介绍
一个完整的淘客app系统，接入了淘宝客（淘宝客私域营销）、京东联盟、多多客。

#### 在线体验
app store 搜索 辣小客

华为、应用宝市场搜索 辣小客

后台：http://kelede.laxiaoke.com  

用户名：kelede 

密码：kelede@20150728

#### 客户端代码
ios端：https://gitee.com/175309744/tk-ios

安卓端：https://gitee.com/175309744/tk-android

bundlejs: https://gitee.com/175309744/tk-bundlejs

主要功能说明
1. 对接了淘宝客，并且对了淘宝客私域营销功能。
2. 对接京东联盟和多多客
3. 3级分佣、省市代理功能
4. 对接了微信登录
5. 对接了支付宝支付
6. app运营管理，app首页运营，可以很方便配置不同类型的楼层显示
7. 广告模块
8. app动态更新（ios、android），可以很方便安全的绕过app的审核机制即时更新app
9. 红包模块（新人红包，可以根据不同人群定义发放相应的红包）
10. 免单模块
11. 会员登记模块（可以通过购买会员服务进行升级)

#### 软件架构
软件架构说明
1. 后台使用spring boot开发
2. app采用weex开发，代码框架使用vue
3. 后端ui采用ismartjs，https://www.ismartjs.com
4. 使用spring cloud

#### 源码说明
1. admin 后台管理模块
2. api 为app提供api服务的模块
3. background.task 定时任务模块（主要定时的执行订单同步、订单分拥等功能）
4. config 配置文件目录
5. core 核心依赖目录
6. open 开放平台，将淘宝客、京东联盟、多多客的功能作为一个开发服务独立了出来，但是在后续的运营过程当中去除掉了
7. ppd.sdk 多多客的api sdk
8. sql db.sql 数据库sql
9. ucenter 用户中心（可忽略）

最主要的4个module是：admin,api,background.task,core，其他module可以忽略

#### 启动说明

1.  启动spring cloud，config文件在config目录内
2.  启动api，启动admin，启动backgroundtask提供服务

#### 源码支持
请加qq：175309744
