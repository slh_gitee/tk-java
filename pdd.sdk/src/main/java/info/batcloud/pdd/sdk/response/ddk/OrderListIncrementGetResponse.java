package info.batcloud.pdd.sdk.response.ddk;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.domain.ddk.Order;
import info.batcloud.pdd.sdk.PddResponse;

import java.util.List;

public class OrderListIncrementGetResponse extends PddResponse {

    @JSONField(name = "order_list_get_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "order_list")
        private List<Order> orderList;
        @JSONField(name = "total_count")
        private int totalCount;

        public List<Order> getOrderList() {
            return orderList;
        }

        public void setOrderList(List<Order> orderList) {
            this.orderList = orderList;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }
    }

}
