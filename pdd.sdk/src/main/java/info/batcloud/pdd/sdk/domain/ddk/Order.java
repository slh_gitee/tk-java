package info.batcloud.pdd.sdk.domain.ddk;

import com.alibaba.fastjson.annotation.JSONField;

public class Order {

    @JSONField(name = "custom_parameters")
    private String customParameters;

    @JSONField(name = "order_receive_time")
    private long orderReceiveTime;

    @JSONField(name = "type")
    private int type;

    @JSONField(name = "order_verify_time")
    private long orderVerifyTime;

    @JSONField(name = "order_pay_time")
    private long orderPayTime;

    @JSONField(name = "order_group_success_time")
    private long orderGroupSuccessTime;

    @JSONField(name = "order_modify_at")
    private long orderModifyAt;

    @JSONField(name = "order_status_desc")
    private String orderStatusDesc;

    @JSONField(name = "p_id")
    private String pid;

    @JSONField(name = "order_status")
    private int orderStatus;

    @JSONField(name = "promotion_amount")
    private float promotionAmount;

    @JSONField(name = "promotion_rate")
    private float promotionRate;

    @JSONField(name = "order_create_time")
    private long orderCreateTime;

    @JSONField(name = "order_amount")
    private float orderAmount;

    @JSONField(name = "goods_price")
    private float goodsPrice;

    @JSONField(name = "goods_quantity")
    private int goodsQuantity;

    @JSONField(name = "goods_thumbnail_url")
    private String goodsThumbnailUrl;

    @JSONField(name = "goods_name")
    private String goodsName;

    @JSONField(name = "goods_id")
    private String goodsId;

    @JSONField(name = "order_sn")
    private String orderSn;

    public String getCustomParameters() {
        return customParameters;
    }

    public void setCustomParameters(String customParameters) {
        this.customParameters = customParameters;
    }

    public long getOrderReceiveTime() {
        return orderReceiveTime;
    }

    public void setOrderReceiveTime(long orderReceiveTime) {
        this.orderReceiveTime = orderReceiveTime;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getOrderVerifyTime() {
        return orderVerifyTime;
    }

    public void setOrderVerifyTime(long orderVerifyTime) {
        this.orderVerifyTime = orderVerifyTime;
    }

    public long getOrderPayTime() {
        return orderPayTime;
    }

    public void setOrderPayTime(long orderPayTime) {
        this.orderPayTime = orderPayTime;
    }

    public long getOrderGroupSuccessTime() {
        return orderGroupSuccessTime;
    }

    public void setOrderGroupSuccessTime(long orderGroupSuccessTime) {
        this.orderGroupSuccessTime = orderGroupSuccessTime;
    }

    public long getOrderModifyAt() {
        return orderModifyAt;
    }

    public void setOrderModifyAt(long orderModifyAt) {
        this.orderModifyAt = orderModifyAt;
    }

    public String getOrderStatusDesc() {
        return orderStatusDesc;
    }

    public void setOrderStatusDesc(String orderStatusDesc) {
        this.orderStatusDesc = orderStatusDesc;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }

    public float getPromotionAmount() {
        return promotionAmount;
    }

    public void setPromotionAmount(float promotionAmount) {
        this.promotionAmount = promotionAmount;
    }

    public float getPromotionRate() {
        return promotionRate;
    }

    public void setPromotionRate(float promotionRate) {
        this.promotionRate = promotionRate;
    }

    public long getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(long orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public float getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(float orderAmount) {
        this.orderAmount = orderAmount;
    }

    public float getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(float goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public int getGoodsQuantity() {
        return goodsQuantity;
    }

    public void setGoodsQuantity(int goodsQuantity) {
        this.goodsQuantity = goodsQuantity;
    }

    public String getGoodsThumbnailUrl() {
        return goodsThumbnailUrl;
    }

    public void setGoodsThumbnailUrl(String goodsThumbnailUrl) {
        this.goodsThumbnailUrl = goodsThumbnailUrl;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }
}
