package info.batcloud.pdd.sdk.response;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.PddResponse;
import info.batcloud.pdd.sdk.domain.GoodsCat;

import java.util.List;

public class GoodsCatsGetResponse extends PddResponse {

    @JSONField(name = "goods_cats_get_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "goods_cats_list")
        private List<GoodsCat> goodsCatList;

        public List<GoodsCat> getGoodsCatList() {
            return goodsCatList;
        }

        public void setGoodsCatList(List<GoodsCat> goodsCatList) {
            this.goodsCatList = goodsCatList;
        }
    }

}
