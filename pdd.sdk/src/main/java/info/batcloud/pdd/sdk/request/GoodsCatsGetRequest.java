package info.batcloud.pdd.sdk.request;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.GoodsCatsGetResponse;

public class GoodsCatsGetRequest implements PddRequest<GoodsCatsGetResponse> {

    @Param("parent_cat_id")
    private long parentCatId;

    @Override
    public String getType() {
        return "pdd.goods.cats.get";
    }

    @Override
    public Class<GoodsCatsGetResponse> getResponseClass() {
        return GoodsCatsGetResponse.class;
    }

    public long getParentCatId() {
        return parentCatId;
    }

    public void setParentCatId(long parentCatId) {
        this.parentCatId = parentCatId;
    }
}
