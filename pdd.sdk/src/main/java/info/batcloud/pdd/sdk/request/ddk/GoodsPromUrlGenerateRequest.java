package info.batcloud.pdd.sdk.request.ddk;

import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.response.ddk.GoodsPromUrlGenerateResponse;
import info.batcloud.pdd.sdk.annotation.Param;

import java.util.List;

public class GoodsPromUrlGenerateRequest implements PddRequest<GoodsPromUrlGenerateResponse> {

    @Param("p_id")
    private String pid;

    @Param("goods_id_list")
    private List<Long> goodsIdList;

    @Param("generate_short_url")
    private boolean generateShortUrl;

    @Param("custom_parameters")
    private String customParameters;

    public String getCustomParameters() {
        return customParameters;
    }

    public void setCustomParameters(String customParameters) {
        this.customParameters = customParameters;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public List<Long> getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(List<Long> goodsIdList) {
        this.goodsIdList = goodsIdList;
    }

    public boolean isGenerateShortUrl() {
        return generateShortUrl;
    }

    public void setGenerateShortUrl(boolean generateShortUrl) {
        this.generateShortUrl = generateShortUrl;
    }

    @Override
    public String getType() {
        return "pdd.ddk.goods.prom.url.generate";
    }

    @Override
    public Class<GoodsPromUrlGenerateResponse> getResponseClass() {
        return GoodsPromUrlGenerateResponse.class;
    }
}
