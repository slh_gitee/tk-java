package info.batcloud.pdd.sdk.response.ddk;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.PddResponse;
import info.batcloud.pdd.sdk.domain.ddk.Pid;

import java.util.List;

public class PidGenerateResponse extends PddResponse {

    @JSONField(name = "p_id_generate_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "p_id_list")
        private List<Pid> pidList;

        public List<Pid> getPidList() {
            return pidList;
        }

        public void setPidList(List<Pid> pidList) {
            this.pidList = pidList;
        }
    }

}
