package info.batcloud.pdd.sdk.response.ddk;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.PddResponse;
import info.batcloud.pdd.sdk.domain.ddk.GoodsPromotionUrl;

import java.util.List;

public class GoodsPromUrlGenerateResponse extends PddResponse {

    @JSONField(name = "goods_promotion_url_generate_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "goods_promotion_url_list")
        private List<GoodsPromotionUrl> goodsPromotionUrlList;

        public List<GoodsPromotionUrl> getGoodsPromotionUrlList() {
            return goodsPromotionUrlList;
        }

        public void setGoodsPromotionUrlList(List<GoodsPromotionUrl> goodsPromotionUrlList) {
            this.goodsPromotionUrlList = goodsPromotionUrlList;
        }
    }
}
