package info.batcloud.pdd.sdk.response.ddk;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.pdd.sdk.PddResponse;
import info.batcloud.pdd.sdk.domain.ddk.Goods;

import java.util.List;

public class GoodsDetailResponse extends PddResponse {

    @JSONField(name = "goods_detail_response")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        @JSONField(name = "goods_details")
        private List<Goods> goodsList;

        public List<Goods> getGoodsList() {
            return goodsList;
        }

        public void setGoodsList(List<Goods> goodsList) {
            this.goodsList = goodsList;
        }
    }
}
