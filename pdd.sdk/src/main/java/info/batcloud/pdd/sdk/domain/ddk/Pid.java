package info.batcloud.pdd.sdk.domain.ddk;

import com.alibaba.fastjson.annotation.JSONField;

public class Pid {

    @JSONField(name = "p_id_name")
    private String pidName;
    @JSONField(name = "p_id")
    private String pid;

    public String getPidName() {
        return pidName;
    }

    public void setPidName(String pidName) {
        this.pidName = pidName;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
