package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.operation.center.domain.PicAdv;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BootPageSetting implements Serializable {

    private List<PicAdv> picList = new ArrayList<>();

    private int timeout;

    private String enterBtnText;

    public String getEnterBtnText() {
        return enterBtnText;
    }

    public void setEnterBtnText(String enterBtnText) {
        this.enterBtnText = enterBtnText;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public List<PicAdv> getPicList() {
        return picList;
    }

    public void setPicList(List<PicAdv> picList) {
        this.picList = picList;
    }
}
