package info.batcloud.fanli.core.operation.center.domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.io.Serializable;

public class PageJump implements Serializable {

    @Enumerated(EnumType.STRING)
    private Page page;
    private String params;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public String getPageTitle() {
        return page == null ? null : page.getTitle();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }
}
