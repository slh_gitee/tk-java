package info.batcloud.fanli.core.settings;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;
import java.util.Date;

@Single
public class DdkContextSetting implements Serializable {

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastOrderFetchEndTime;

    public Date getLastOrderFetchEndTime() {
        return lastOrderFetchEndTime;
    }

    public void setLastOrderFetchEndTime(Date lastOrderFetchEndTime) {
        this.lastOrderFetchEndTime = lastOrderFetchEndTime;
    }
}
