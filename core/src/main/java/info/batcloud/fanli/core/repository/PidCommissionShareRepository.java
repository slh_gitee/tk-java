package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.PidCommissionItemShare;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;
import java.util.List;

public interface PidCommissionShareRepository extends CrudRepository<PidCommissionItemShare, Long> {

    PidCommissionItemShare findByCommissionItemIdAndPidAndExpireTimeGreaterThan(long commissionItemId, String pid, Date expireTime);

    List<PidCommissionItemShare> findByCommissionItemIdAndPidAndExpireTimeGreaterThanAndKlIsNotNull(long commissionItemId, String pid, Date expireTime);
}
