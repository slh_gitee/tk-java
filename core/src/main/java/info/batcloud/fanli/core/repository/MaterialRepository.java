package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Material;
import info.batcloud.fanli.core.enums.MaterialStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface MaterialRepository extends PagingAndSortingRepository<Material, Long>, JpaSpecificationExecutor<Material> {

    List<Material> findByStatusNotOrderByIdDesc(MaterialStatus status);
    List<Material> findByStatusOrderByIdDesc(MaterialStatus status);

}
