package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum SeckillItemStatus implements EnumTitle{

    WAIT_PAY, WAIT_VERIFY, ONSALE, INSTOCK, DELETED;
    public String title;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
