package info.batcloud.fanli.core.red.packet.domain;

import java.util.Date;

public class NewerTypeConfig {

    private Date minCreateTime;

    public Date getMinCreateTime() {
        return minCreateTime;
    }

    public void setMinCreateTime(Date minCreateTime) {
        this.minCreateTime = minCreateTime;
    }
}
