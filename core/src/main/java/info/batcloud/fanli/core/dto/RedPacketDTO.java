package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.RedPacketStatus;
import info.batcloud.fanli.core.enums.RedPacketType;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

import java.util.Date;

public class RedPacketDTO {

    private Long id;

    private String name;

    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    /**
     * 总红包数量
     */
    private int totalNum;

    /**
     * 已领取的数量
     */
    private int drawNum;

    //已发放的数量
    private int grantNum;

    private int validDayNum;

    private int maxUserGrantNum;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastDrawTime;

    private float integral;

    private float money;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endTime;

    private RedPacketStatus status;

    private RedPacketType type;

    private ActivityFreq freq;

    private Long crowdId;

    private String crowdName;

    private WalletFlowDetailType moneyWalletFlowDetailType;

    private WalletFlowDetailType integralWalletFlowDetailType;

    public Long getCrowdId() {
        return crowdId;
    }

    public void setCrowdId(Long crowdId) {
        this.crowdId = crowdId;
    }

    public String getCrowdName() {
        return crowdName;
    }

    public void setCrowdName(String crowdName) {
        this.crowdName = crowdName;
    }

    public int getValidDayNum() {
        return validDayNum;
    }

    public void setValidDayNum(int validDayNum) {
        this.validDayNum = validDayNum;
    }

    public int getMaxUserGrantNum() {
        return maxUserGrantNum;
    }

    public void setMaxUserGrantNum(int maxUserGrantNum) {
        this.maxUserGrantNum = maxUserGrantNum;
    }

    public String getFreqTitle() {
        return freq == null ? null : freq.getTitle();
    }

    public ActivityFreq getFreq() {
        return freq;
    }

    public void setFreq(ActivityFreq freq) {
        this.freq = freq;
    }

    public float getIntegral() {
        return integral;
    }

    public void setIntegral(float integral) {
        this.integral = integral;
    }

    public int getGrantNum() {
        return grantNum;
    }

    public void setGrantNum(int grantNum) {
        this.grantNum = grantNum;
    }

    public WalletFlowDetailType getMoneyWalletFlowDetailType() {
        return moneyWalletFlowDetailType;
    }

    public void setMoneyWalletFlowDetailType(WalletFlowDetailType moneyWalletFlowDetailType) {
        this.moneyWalletFlowDetailType = moneyWalletFlowDetailType;
    }

    public WalletFlowDetailType getIntegralWalletFlowDetailType() {
        return integralWalletFlowDetailType;
    }

    public void setIntegralWalletFlowDetailType(WalletFlowDetailType integralWalletFlowDetailType) {
        this.integralWalletFlowDetailType = integralWalletFlowDetailType;
    }

    public RedPacketType getType() {
        return type;
    }

    public void setType(RedPacketType type) {
        this.type = type;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public String getTypeTitle() {
        return type == null ? null : type.getTitle();
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(int drawNum) {
        this.drawNum = drawNum;
    }

    public Date getLastDrawTime() {
        return lastDrawTime;
    }

    public void setLastDrawTime(Date lastDrawTime) {
        this.lastDrawTime = lastDrawTime;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public RedPacketStatus getStatus() {
        return status;
    }

    public void setStatus(RedPacketStatus status) {
        this.status = status;
    }
}
