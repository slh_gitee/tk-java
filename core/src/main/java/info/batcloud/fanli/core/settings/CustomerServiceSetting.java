package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.helper.OSSImageHelper;

import java.io.Serializable;

public class CustomerServiceSetting implements Serializable {

    private String weixin;

    private String weixinQrcode;

    private String weixinDesc;

    private String servicePhone; //客服电话

    private String serviceTime;

    public String getWeixinQrcodeUrl() {
        return weixinQrcode == null ? null : OSSImageHelper.toUrl(weixinQrcode);
    }

    public String getWeixinDesc() {
        return weixinDesc;
    }

    public void setWeixinDesc(String weixinDesc) {
        this.weixinDesc = weixinDesc;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getWeixinQrcode() {
        return weixinQrcode;
    }

    public void setWeixinQrcode(String weixinQrcode) {
        this.weixinQrcode = weixinQrcode;
    }

    public String getServicePhone() {
        return servicePhone;
    }

    public void setServicePhone(String servicePhone) {
        this.servicePhone = servicePhone;
    }

    public String getServiceTime() {
        return serviceTime;
    }

    public void setServiceTime(String serviceTime) {
        this.serviceTime = serviceTime;
    }
}
