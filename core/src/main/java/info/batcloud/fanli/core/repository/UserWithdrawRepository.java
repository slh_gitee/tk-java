package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserWithdraw;
import info.batcloud.fanli.core.enums.UserWithdrawStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserWithdrawRepository extends PagingAndSortingRepository<UserWithdraw, Long>, JpaSpecificationExecutor<UserWithdraw> {

    List<UserWithdraw> findByWalletFlowDetailIdIn(List<Long> detailIds);

    UserWithdraw findTopByUserIdOrderByIdDesc(long userId);

    List<UserWithdraw> findByStatusOrderByIdAsc(UserWithdrawStatus status);
}
