package info.batcloud.fanli.core.operation.center.domain;

import info.batcloud.fanli.core.context.StaticContext;
import info.batcloud.fanli.core.enums.EnumTitle;

import java.io.Serializable;

public enum Page implements Serializable, EnumTitle {

    FREE_CHARGE_ACTIVITY, COMMISSION_ITEM_SEARCH, COMMISSION_ITEM_EP_SEARCH,COMMISSION_ITEM, WEB_PAGE, PAGE, URL, NONE, FLASH_SALE;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
