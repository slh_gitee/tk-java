package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.dto.WeixinOfficialAccountDTO;
import info.batcloud.fanli.core.constants.CacheNameConstants;
import info.batcloud.fanli.core.entity.WeixinOfficialAccount;
import info.batcloud.fanli.core.repository.WeixinOfficialAccountRepository;
import info.batcloud.fanli.core.service.SystemSettingService;
import info.batcloud.fanli.core.service.WeixinOfficialAccountService;
import info.batcloud.fanli.core.settings.LaxiaokeOpenSetting;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

@Service
@CacheConfig(cacheNames = CacheNameConstants.WEIXIN_OFFICIAL_ACCOUNT)
public class WeixinOfficialAccountServiceImpl implements WeixinOfficialAccountService {

    @Inject
    private WeixinOfficialAccountRepository weixinOfficialAccountRepository;

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public List<WeixinOfficialAccountDTO> findByUserId(long userId) {
        return weixinOfficialAccountRepository.findByUserIdAndDeletedIsFalse(userId)
                .stream().map(o -> of(o)).collect(Collectors.toList());
    }

    @Override
    @Cacheable(key = "'WEIXIN_OFFICIAL_ACCOUNT' + #id")
    public WeixinOfficialAccountDTO findById(long id) {
        return of(weixinOfficialAccountRepository.findOne(id));
    }

    @Override
    public WeixinOfficialAccountDTO addWeixinOfficialAccount(WeixinOfficalAccountAddParam param) {
        WeixinOfficialAccount account = new WeixinOfficialAccount();
        BeanUtils.copyProperties(param, account);
        weixinOfficialAccountRepository.save(account);
        return of(account);
    }

    @Override
    @CacheEvict(key = "'WEIXIN_OFFICIAL_ACCOUNT' + #id")
    public void updateWeixinOfficialAccount(long id, WeixinOfficalAccountUpdateParam param) {
        WeixinOfficialAccount e = weixinOfficialAccountRepository.findOne(id);
        Assert.isTrue(param.getUserId().equals(e.getUserId()), "UserId不匹配");
        BeanUtils.copyProperties(param, e);
        weixinOfficialAccountRepository.save(e);
    }

    @Override
    @CacheEvict(key = "'WEIXIN_OFFICIAL_ACCOUNT' + #id")
    public void deleteByUserIdAndId(long userId, long id) {
        weixinOfficialAccountRepository.deleteByUserIdAndId(userId, id);
    }

    private WeixinOfficialAccountDTO of(WeixinOfficialAccount account) {
        WeixinOfficialAccountDTO bo = new WeixinOfficialAccountDTO();
        BeanUtils.copyProperties(account, bo);
        LaxiaokeOpenSetting setting = systemSettingService.findActiveSetting(LaxiaokeOpenSetting.class);
        bo.setServerUrl(setting.getApiUrl() + "/weixin-official-account/receiver/" + account.getId());
        return bo;
    }
}
