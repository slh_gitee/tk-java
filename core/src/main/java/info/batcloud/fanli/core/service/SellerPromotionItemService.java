package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.SellerPromotionItemDTO;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemFilter;
import info.batcloud.fanli.core.enums.SellerPromotionItemSort;
import info.batcloud.fanli.core.enums.SellerPromotionItemStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;
import java.util.function.Function;

public interface SellerPromotionItemService {

    void saveSellerPromotionItem(SellerPromotionItemSaveParam param);

    void updateSellerPromotionItem(SellerPromotionItemUpdateParam param);

    //追加商品推广数量
    void addPromotionNum(long sellerPromotionItemId, int num, float depositAmount);

    Paging<SellerPromotionItemDTO> search(SearchParam param);

    SellerPromotionItemDTO findById(long id);

    void deleteById(long id, Function<Long, Boolean> check);

    void delistById(long id, Function<Long, Boolean> check);

    void listById(long id, Function<Long, Boolean> check);

    void verify(long id, VerifyParam param);

    class VerifyParam {
        private boolean success;
        private String verifyRemark;

        private SellerPromotionItemStatus status;

        public SellerPromotionItemStatus getStatus() {
            return status;
        }

        public void setStatus(SellerPromotionItemStatus status) {
            this.status = status;
        }

        public String getVerifyRemark() {
            return verifyRemark;
        }

        public void setVerifyRemark(String verifyRemark) {
            this.verifyRemark = verifyRemark;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }

    class SearchParam extends PagingParam {
        private String keywords;
        private EcomPlat ecomPlat;

        private Long shopcatId;

        private SellerPromotionItemStatus status;

        private SellerPromotionItemSort sort;

        private SellerPromotionItemFilter filter;

        private Long userId;

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public SellerPromotionItemFilter getFilter() {
            return filter;
        }

        public void setFilter(SellerPromotionItemFilter filter) {
            this.filter = filter;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public SellerPromotionItemSort getSort() {
            return sort;
        }

        public void setSort(SellerPromotionItemSort sort) {
            this.sort = sort;
        }

        public SellerPromotionItemStatus getStatus() {
            return status;
        }

        public void setStatus(SellerPromotionItemStatus status) {
            this.status = status;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }
    }

    class SellerPromotionItemUpdateParam {

        private Long id;
        private String title;
        private String keywords;
        private String shopTitle;
        private Long shopcatId;
        private float originPrice;
        private float rebateFee;
        private int dayLimitNum;
        private String logisticsCompany;
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date startTime;
        private Long userId;
        private String description;

        private List<String> screenshotList;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getScreenshotList() {
            return screenshotList;
        }

        public void setScreenshotList(List<String> screenshotList) {
            this.screenshotList = screenshotList;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getShopTitle() {
            return shopTitle;
        }

        public void setShopTitle(String shopTitle) {
            this.shopTitle = shopTitle;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public float getOriginPrice() {
            return originPrice;
        }

        public void setOriginPrice(float originPrice) {
            this.originPrice = originPrice;
        }

        public float getRebateFee() {
            return rebateFee;
        }

        public void setRebateFee(float rebateFee) {
            this.rebateFee = rebateFee;
        }

        public int getDayLimitNum() {
            return dayLimitNum;
        }

        public void setDayLimitNum(int dayLimitNum) {
            this.dayLimitNum = dayLimitNum;
        }

        public String getLogisticsCompany() {
            return logisticsCompany;
        }

        public void setLogisticsCompany(String logisticsCompany) {
            this.logisticsCompany = logisticsCompany;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    class SellerPromotionItemSaveParam {
        private String title;
        private String sourceItemUrl;
        private String picUrl;
        private List<String> imgs;
        private String keywords;
        private String shopTitle;
        private Long shopcatId;
        private float originPrice;
        private float rebateFee;
        private int dayLimitNum;
        private String logisticsCompany;
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date startTime;
        private EcomPlat ecomPlat;
        private Long userId;

        private List<String> screenshotList;

        private String description;

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<String> getScreenshotList() {
            return screenshotList;
        }

        public void setScreenshotList(List<String> screenshotList) {
            this.screenshotList = screenshotList;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSourceItemUrl() {
            return sourceItemUrl;
        }

        public void setSourceItemUrl(String sourceItemUrl) {
            this.sourceItemUrl = sourceItemUrl;
        }

        public String getPicUrl() {
            return picUrl;
        }

        public void setPicUrl(String picUrl) {
            this.picUrl = picUrl;
        }

        public List<String> getImgs() {
            return imgs;
        }

        public void setImgs(List<String> imgs) {
            this.imgs = imgs;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getShopTitle() {
            return shopTitle;
        }

        public void setShopTitle(String shopTitle) {
            this.shopTitle = shopTitle;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public float getOriginPrice() {
            return originPrice;
        }

        public void setOriginPrice(float originPrice) {
            this.originPrice = originPrice;
        }

        public float getRebateFee() {
            return rebateFee;
        }

        public void setRebateFee(float rebateFee) {
            this.rebateFee = rebateFee;
        }

        public int getDayLimitNum() {
            return dayLimitNum;
        }

        public void setDayLimitNum(int dayLimitNum) {
            this.dayLimitNum = dayLimitNum;
        }

        public String getLogisticsCompany() {
            return logisticsCompany;
        }

        public void setLogisticsCompany(String logisticsCompany) {
            this.logisticsCompany = logisticsCompany;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }
    }

}
