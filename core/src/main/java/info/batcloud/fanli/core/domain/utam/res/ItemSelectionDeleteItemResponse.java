package info.batcloud.fanli.core.domain.utam.res;

import info.batcloud.fanli.core.domain.ApiResponse;

public class ItemSelectionDeleteItemResponse extends ApiResponse<ItemSelectionDeleteItemResponse.Data> {


    public static class Data {
        private int delFailNum;
        private int delSuccessNum;

        public int getDelFailNum() {
            return delFailNum;
        }

        public void setDelFailNum(int delFailNum) {
            this.delFailNum = delFailNum;
        }

        public int getDelSuccessNum() {
            return delSuccessNum;
        }

        public void setDelSuccessNum(int delSuccessNum) {
            this.delSuccessNum = delSuccessNum;
        }
    }

}
