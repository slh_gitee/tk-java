package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum SellerStatus implements EnumTitle{

    WAIT_VERIFY, VERIFY_FAIL, VALID, INVALID, DELETED;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
