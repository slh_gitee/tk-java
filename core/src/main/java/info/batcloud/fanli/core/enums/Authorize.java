package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

import java.io.Serializable;

public enum Authorize implements Serializable, EnumTitle {
    ITEM_SELECTION_MANAGER,
    ;//选品管理

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
