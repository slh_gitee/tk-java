package info.batcloud.fanli.core.domain.stat;

public class UserSettledCommissionEarning {

    private Long userId;

    private String userAvatar;

    private String userNickname;

    private int orderNum;

    private float settledFee;

    private float settledRewardFee;

    private float settledCommissionFee;

    private float selfBuySettledCommissionFee;

    private float directSettledCommissionFee;

    private float indirectSettledCommissionFee;

    private float carrierSettledCommissionFee;

    private float cityAgentSettledCommissionFee;

    private float districtAgentSettledCommissionFee;

    private float relationAgentSettledCommissionFee;

    private float selfBuySettledRewardFee;

    private float directSettledRewardFee;

    private float indirectSettledRewardFee;

    private float carrierSettledRewardFee;

    private float cityAgentSettledRewardFee;

    private float districtAgentSettledRewardFee;

    private float relationAgentSettledRewardFee;

    private int selfBuyOrderNum;

    private int directOrderNum;

    private int indirectOrderNum;

    private int carrierOrderNum;

    private int cityAgentOrderNum;

    private int districtAgentOrderNum;

    private int relationAgentOrderNum;

    public float getSelfBuySettledCommissionFee() {
        return selfBuySettledCommissionFee;
    }

    public void setSelfBuySettledCommissionFee(float selfBuySettledCommissionFee) {
        this.selfBuySettledCommissionFee = selfBuySettledCommissionFee;
    }

    public float getDirectSettledCommissionFee() {
        return directSettledCommissionFee;
    }

    public void setDirectSettledCommissionFee(float directSettledCommissionFee) {
        this.directSettledCommissionFee = directSettledCommissionFee;
    }

    public float getIndirectSettledCommissionFee() {
        return indirectSettledCommissionFee;
    }

    public void setIndirectSettledCommissionFee(float indirectSettledCommissionFee) {
        this.indirectSettledCommissionFee = indirectSettledCommissionFee;
    }

    public float getCarrierSettledCommissionFee() {
        return carrierSettledCommissionFee;
    }

    public void setCarrierSettledCommissionFee(float carrierSettledCommissionFee) {
        this.carrierSettledCommissionFee = carrierSettledCommissionFee;
    }

    public float getCityAgentSettledCommissionFee() {
        return cityAgentSettledCommissionFee;
    }

    public void setCityAgentSettledCommissionFee(float cityAgentSettledCommissionFee) {
        this.cityAgentSettledCommissionFee = cityAgentSettledCommissionFee;
    }

    public float getDistrictAgentSettledCommissionFee() {
        return districtAgentSettledCommissionFee;
    }

    public void setDistrictAgentSettledCommissionFee(float districtAgentSettledCommissionFee) {
        this.districtAgentSettledCommissionFee = districtAgentSettledCommissionFee;
    }

    public float getRelationAgentSettledCommissionFee() {
        return relationAgentSettledCommissionFee;
    }

    public void setRelationAgentSettledCommissionFee(float relationAgentSettledCommissionFee) {
        this.relationAgentSettledCommissionFee = relationAgentSettledCommissionFee;
    }

    public float getSelfBuySettledRewardFee() {
        return selfBuySettledRewardFee;
    }

    public void setSelfBuySettledRewardFee(float selfBuySettledRewardFee) {
        this.selfBuySettledRewardFee = selfBuySettledRewardFee;
    }

    public float getDirectSettledRewardFee() {
        return directSettledRewardFee;
    }

    public void setDirectSettledRewardFee(float directSettledRewardFee) {
        this.directSettledRewardFee = directSettledRewardFee;
    }

    public float getIndirectSettledRewardFee() {
        return indirectSettledRewardFee;
    }

    public void setIndirectSettledRewardFee(float indirectSettledRewardFee) {
        this.indirectSettledRewardFee = indirectSettledRewardFee;
    }

    public float getCarrierSettledRewardFee() {
        return carrierSettledRewardFee;
    }

    public void setCarrierSettledRewardFee(float carrierSettledRewardFee) {
        this.carrierSettledRewardFee = carrierSettledRewardFee;
    }

    public float getCityAgentSettledRewardFee() {
        return cityAgentSettledRewardFee;
    }

    public void setCityAgentSettledRewardFee(float cityAgentSettledRewardFee) {
        this.cityAgentSettledRewardFee = cityAgentSettledRewardFee;
    }

    public float getDistrictAgentSettledRewardFee() {
        return districtAgentSettledRewardFee;
    }

    public void setDistrictAgentSettledRewardFee(float districtAgentSettledRewardFee) {
        this.districtAgentSettledRewardFee = districtAgentSettledRewardFee;
    }

    public float getRelationAgentSettledRewardFee() {
        return relationAgentSettledRewardFee;
    }

    public void setRelationAgentSettledRewardFee(float relationAgentSettledRewardFee) {
        this.relationAgentSettledRewardFee = relationAgentSettledRewardFee;
    }

    public int getSelfBuyOrderNum() {
        return selfBuyOrderNum;
    }

    public void setSelfBuyOrderNum(int selfBuyOrderNum) {
        this.selfBuyOrderNum = selfBuyOrderNum;
    }

    public int getDirectOrderNum() {
        return directOrderNum;
    }

    public void setDirectOrderNum(int directOrderNum) {
        this.directOrderNum = directOrderNum;
    }

    public int getIndirectOrderNum() {
        return indirectOrderNum;
    }

    public void setIndirectOrderNum(int indirectOrderNum) {
        this.indirectOrderNum = indirectOrderNum;
    }

    public int getCarrierOrderNum() {
        return carrierOrderNum;
    }

    public void setCarrierOrderNum(int carrierOrderNum) {
        this.carrierOrderNum = carrierOrderNum;
    }

    public int getCityAgentOrderNum() {
        return cityAgentOrderNum;
    }

    public void setCityAgentOrderNum(int cityAgentOrderNum) {
        this.cityAgentOrderNum = cityAgentOrderNum;
    }

    public int getDistrictAgentOrderNum() {
        return districtAgentOrderNum;
    }

    public void setDistrictAgentOrderNum(int districtAgentOrderNum) {
        this.districtAgentOrderNum = districtAgentOrderNum;
    }

    public int getRelationAgentOrderNum() {
        return relationAgentOrderNum;
    }

    public void setRelationAgentOrderNum(int relationAgentOrderNum) {
        this.relationAgentOrderNum = relationAgentOrderNum;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public int getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(int orderNum) {
        this.orderNum = orderNum;
    }

    public float getSettledFee() {
        return settledFee;
    }

    public void setSettledFee(float settledFee) {
        this.settledFee = settledFee;
    }

    public float getSettledRewardFee() {
        return settledRewardFee;
    }

    public void setSettledRewardFee(float settledRewardFee) {
        this.settledRewardFee = settledRewardFee;
    }

    public float getSettledCommissionFee() {
        return settledCommissionFee;
    }

    public void setSettledCommissionFee(float settledCommissionFee) {
        this.settledCommissionFee = settledCommissionFee;
    }
}
