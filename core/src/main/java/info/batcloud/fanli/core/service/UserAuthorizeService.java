package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.enums.Authorize;

import java.util.List;

public interface UserAuthorizeService {

    void addAuthorize(long userId, Authorize authorize);

    boolean checkAuthorize(long userId, Authorize authorize);

    List<Authorize> findByUserId(long userId);

    void deleteAuthorize(long userId, Authorize authorize);

    void deleteById(long id);
}
