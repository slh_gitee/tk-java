package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CommissionItemFetchDTO implements Serializable {

    private String title;

    private String picUrl;

    private String whitePicUrl;

    private Float originPrice;

    private String shopTitle;

    private Date listTime; //上架时间

    private Date delistTime;//下架时间

    private String description;

    private String summary;

    private int sales;

    private String shopcatPath;

    private Long shopcatId;

    private CommissionItemStatus status;

    //来源电商平台
    private EcomPlat ecomPlat;

    private Long sourceItemId;

    private List<Long> sourceCatIdList;

    private String sourceItemUrl;

    private String sourceItemClickUrl;

    private Long sourceCatId;

    private String itemPlace; //货源地

    private Float commissionRate;

    private Float couponValue;

    private Float couponThresholdAmount;//优惠券门槛金额

    private boolean coupon;

    private String couponClickUrl;

    private Date couponStartTime;

    private Date couponEndTime;

    private String couponInfo;

    private Integer couponTotalCount;

    private Integer couponRemainCount;

    private List<String> imgList;

    private Long channelId;

    private boolean recommend;

    private boolean freeShipment;

    //京东自营
    private boolean jdSale;

    //秒杀
    private boolean seckill;

    private String content;

    private boolean tbkZt;

    public boolean isTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(boolean tbkZt) {
        this.tbkZt = tbkZt;
    }

    public String getWhitePicUrl() {
        return whitePicUrl;
    }

    public void setWhitePicUrl(String whitePicUrl) {
        this.whitePicUrl = whitePicUrl;
    }

    public Float getCouponThresholdAmount() {
        return couponThresholdAmount;
    }

    public void setCouponThresholdAmount(Float couponThresholdAmount) {
        this.couponThresholdAmount = couponThresholdAmount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Long> getSourceCatIdList() {
        return sourceCatIdList;
    }

    public void setSourceCatIdList(List<Long> sourceCatIdList) {
        this.sourceCatIdList = sourceCatIdList;
    }

    public boolean isJdSale() {
        return jdSale;
    }

    public void setJdSale(boolean jdSale) {
        this.jdSale = jdSale;
    }

    public boolean isSeckill() {
        return seckill;
    }

    public void setSeckill(boolean seckill) {
        this.seckill = seckill;
    }

    public boolean isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(boolean freeShipment) {
        this.freeShipment = freeShipment;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }

    public List<String> getImgList() {
        return imgList;
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        if(this.getCouponThresholdAmount() == null) {
            return this.getOriginPrice();
        }
        return this.getOriginPrice() >= this.getCouponThresholdAmount() ? this.getOriginPrice() - this.couponValue : this.getOriginPrice();
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public Date getListTime() {
        return listTime;
    }

    public void setListTime(Date listTime) {
        this.listTime = listTime;
    }

    public Date getDelistTime() {
        return delistTime;
    }

    public void setDelistTime(Date delistTime) {
        this.delistTime = delistTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }



    public String getShopcatPath() {
        return shopcatPath;
    }

    public void setShopcatPath(String shopcatPath) {
        this.shopcatPath = shopcatPath;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }

    public CommissionItemStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionItemStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public String getSourceItemClickUrl() {
        return sourceItemClickUrl;
    }

    public void setSourceItemClickUrl(String sourceItemClickUrl) {
        this.sourceItemClickUrl = sourceItemClickUrl;
    }

    public Long getSourceCatId() {
        return sourceCatId;
    }

    public void setSourceCatId(Long sourceCatId) {
        this.sourceCatId = sourceCatId;
    }

    public String getItemPlace() {
        return itemPlace;
    }

    public void setItemPlace(String itemPlace) {
        this.itemPlace = itemPlace;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponClickUrl() {
        return couponClickUrl;
    }

    public void setCouponClickUrl(String couponClickUrl) {
        this.couponClickUrl = couponClickUrl;
    }

    public Date getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(Date couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public Date getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(Date couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Integer getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(Integer couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public Integer getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(Integer couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CommissionItemFetchDTO)) {
            return false;
        }
        CommissionItemFetchDTO commissionItemFetchDTO = (CommissionItemFetchDTO) obj;
        return commissionItemFetchDTO.getEcomPlat() == this.getEcomPlat()
                && commissionItemFetchDTO.getSourceItemId().equals(this.getSourceItemId());
    }

    @Override
    public int hashCode() {
        return this.getEcomPlat().hashCode() + this.getSourceItemId().hashCode();
    }
}
