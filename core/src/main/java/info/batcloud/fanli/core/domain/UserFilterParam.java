package info.batcloud.fanli.core.domain;

import java.util.Date;

public class UserFilterParam {

    private Date minCreateTime;

    public Date getMinCreateTime() {
        return minCreateTime;
    }

    public void setMinCreateTime(Date minCreateTime) {
        this.minCreateTime = minCreateTime;
    }
}
