package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.WeixinAccount;

public interface WeixinAccountService {

    WeixinAccount findAccountByOpenid(String openid);

}
