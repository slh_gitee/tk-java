package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum TimeUnit {

    YEAR(12), MONTH(1), FOREVER(-1);

    public int month;

    TimeUnit(int month) {
        this.month = month;
    }

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
