package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.ServiceItemStatus;
import info.batcloud.fanli.core.enums.TimeUnit;

import javax.persistence.*;
import java.util.Date;

@Entity
public class ServiceItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private Service service;

    private Date createTime;

    private float price;

    @Enumerated(EnumType.STRING)
    private TimeUnit timeUnit;

    private Date updateTime;

    @Enumerated(EnumType.STRING)
    private ServiceItemStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public ServiceItemStatus getStatus() {
        return status;
    }

    public void setStatus(ServiceItemStatus status) {
        this.status = status;
    }
}
