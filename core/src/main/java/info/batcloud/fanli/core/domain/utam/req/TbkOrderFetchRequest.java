package info.batcloud.fanli.core.domain.utam.req;

import info.batcloud.fanli.core.enums.TkOrderPayStatus;
import info.batcloud.fanli.core.enums.TkOrderQueryType;
import info.batcloud.fanli.core.enums.TkOrderScene;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class TbkOrderFetchRequest {

    private TkOrderQueryType queryType;
    private TkOrderPayStatus payStatus;
    private TkOrderScene scene;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    private String positionIndex;

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public TkOrderScene getScene() {
        return scene;
    }

    public void setScene(TkOrderScene scene) {
        this.scene = scene;
    }

    public String getPositionIndex() {
        return positionIndex;
    }

    public void setPositionIndex(String positionIndex) {
        this.positionIndex = positionIndex;
    }

    public TkOrderPayStatus getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(TkOrderPayStatus payStatus) {
        this.payStatus = payStatus;
    }

    public TkOrderQueryType getQueryType() {
        return queryType;
    }

    public void setQueryType(TkOrderQueryType queryType) {
        this.queryType = queryType;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

}
