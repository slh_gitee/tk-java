package info.batcloud.fanli.core.placeholder;

import info.batcloud.fanli.core.service.UserService;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class RelationIdPlaceholder extends AbstractPlaceholder {

    @Inject
    private UserService userService;

    @Override
    protected String replacePlaceholder(ReplaceContext context) {
        if(context.getUserId() == null) {
            return "";
        } else {
            return userService.findTaobaoRelationId(context.getUserId());
        }
    }

    @Override
    protected String getKey() {
        return "RELATION_ID";
    }

}
