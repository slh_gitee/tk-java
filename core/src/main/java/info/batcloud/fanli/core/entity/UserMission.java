package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.context.StaticContext;
import info.batcloud.fanli.core.enums.UserMissionStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserMission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Mission mission;

    private Date createTime;

    private int completeTimes;

    private Date completeTime;

    @Enumerated(EnumType.STRING)
    private UserMissionStatus status;

    public String getTitle() {
        return StaticContext.messageSource.getMessage("UserMission." + getMission().getType().name(),
                new Object[]{getMission().getTotalTimes(), getMission().getIntegral(), this.getCompleteTimes()}, null);
    }

    public Date getCompleteTime() {
        return completeTime;
    }

    public void setCompleteTime(Date completeTime) {
        this.completeTime = completeTime;
    }

    public void setMission(Mission mission) {
        this.mission = mission;
    }

    public Mission getMission() {
        return mission;
    }

    public int getCompleteTimes() {
        return completeTimes;
    }

    public void setCompleteTimes(int completeTimes) {
        this.completeTimes = completeTimes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public UserMissionStatus getStatus() {
        return status;
    }

    public void setStatus(UserMissionStatus status) {
        this.status = status;
    }
}
