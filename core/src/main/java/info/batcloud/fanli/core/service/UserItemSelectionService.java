package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.UserItemSelectionDTO;
import info.batcloud.fanli.core.enums.EcomPlat;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public interface UserItemSelectionService {

    long createUserItemSelection(ItemSelectionCreateParam param);

    void addShareTimes(long itemSelectionId, int times);

    void deleteById(long userId, long id);

    Paging<UserItemSelectionDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private Boolean recommend;

        private Sort sort;

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public enum Sort {
            ID_DESC,
            SHARE_TIMES_DESC
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Boolean getRecommend() {
            return recommend;
        }

        public void setRecommend(Boolean recommend) {
            this.recommend = recommend;
        }
    }

    class ItemSelectionCreateParam {
        private Long userId;
        @NotNull
        private List<Long> itemIdList = new ArrayList<>();
        private List<Long> sourceItemIdList = new ArrayList<>();
        private List<EcomPlat> ecomPlatList = new ArrayList<>();
        @NotNull
        private String content;

        public List<Long> getSourceItemIdList() {
            return sourceItemIdList;
        }

        public void setSourceItemIdList(List<Long> sourceItemIdList) {
            this.sourceItemIdList = sourceItemIdList;
        }

        public List<EcomPlat> getEcomPlatList() {
            return ecomPlatList;
        }

        public void setEcomPlatList(List<EcomPlat> ecomPlatList) {
            this.ecomPlatList = ecomPlatList;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public List<Long> getItemIdList() {
            return itemIdList;
        }

        public void setItemIdList(List<Long> itemIdList) {
            this.itemIdList = itemIdList;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

}
