package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum TkOrderScene implements EnumTitle {

    COMMON("1"), CHANNEL("2"), MEMBER("3");
    public String value;

    TkOrderScene(String value) {
        this.value = value;
    }


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
