package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.AdvSpaceItemDTO;

import java.util.List;

public interface AdvSpaceItemService {

    void add(AdvSpaceItemAddParam param);

    void deleteById(long id);

    List<AdvSpaceItemDTO> findByAdvSpaceId(long advSpaceId);

    class AdvSpaceItemAddParam {
        private Long advSpaceId;
        private List<Long> advIdList;

        public Long getAdvSpaceId() {
            return advSpaceId;
        }

        public void setAdvSpaceId(Long advSpaceId) {
            this.advSpaceId = advSpaceId;
        }

        public List<Long> getAdvIdList() {
            return advIdList;
        }

        public void setAdvIdList(List<Long> advIdList) {
            this.advIdList = advIdList;
        }
    }

}
