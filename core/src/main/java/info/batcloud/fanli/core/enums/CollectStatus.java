package info.batcloud.fanli.core.enums;

public enum CollectStatus {

    COLLECT("采集中"), SUCCESS("采集成功"), FAIL("采集失败");
    public String title;

    CollectStatus(String title) {
        this.title = title;
    }

}
