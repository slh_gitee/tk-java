package info.batcloud.fanli.core.operation.center.domain;

import java.io.Serializable;

public class Adv extends PageJump implements Serializable {

    private String title;

    private String slogan;

    //参数说明
    private String paramsDesc;

    public String getParamsDesc() {
        return paramsDesc;
    }

    public void setParamsDesc(String paramsDesc) {
        this.paramsDesc = paramsDesc;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


}
