package info.batcloud.fanli.core;

import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import info.batcloud.fanli.core.top.TaobaoClientConfig;
import info.batcloud.fanli.core.top.TbkConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static info.batcloud.fanli.core.constants.TaobaoClientConstants.TAOBAO_CLIENT_CONFIG_TBK;
import static info.batcloud.fanli.core.constants.TaobaoClientConstants.TAOBAO_CLIENT_ITEM_MANAGEMENT;
import static info.batcloud.fanli.core.constants.TaobaoClientConstants.TAOBAO_CLIENT_ITEM_TBK;

@Configuration
public class TopConfig {

    @Bean
    @ConfigurationProperties("taobao.item-management-client")
    public TaobaoClientConfig itemManagementClientConfig() {
        return new TaobaoClientConfig();
    }


    @Bean(TAOBAO_CLIENT_CONFIG_TBK)
    @ConfigurationProperties("taobao.tbk-client")
    public TaobaoClientConfig tbkClientConfig() {
        return new TaobaoClientConfig();
    }


    @Bean(TAOBAO_CLIENT_ITEM_MANAGEMENT)
    public TaobaoClient itemManagementClient() {
        TaobaoClientConfig config = itemManagementClientConfig();
        DefaultTaobaoClient client = new DefaultTaobaoClient(config.getApiUrl(), config.getClientId(),
                config.getClientSecret());
        return client;
    }

    @Bean(TAOBAO_CLIENT_ITEM_TBK)
    public TaobaoClient tbkClient() {
        TaobaoClientConfig config = tbkClientConfig();
        DefaultTaobaoClient client = new DefaultTaobaoClient(config.getApiUrl(), config.getClientId(),
                config.getClientSecret());
        return client;
    }

    @Bean
    @ConfigurationProperties("taobao.tbk")
    public TbkConfig tbkConfig() {
        return new TbkConfig();
    }

}
