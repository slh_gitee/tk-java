package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.helper.OSSImageHelper;

import java.io.Serializable;

public class Pic implements Serializable {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUrl() {
        return OSSImageHelper.toUrl(this.key);
    }
}
