package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.context.StaticContext;
import info.batcloud.fanli.core.enums.MissionStatus;
import info.batcloud.fanli.core.enums.MissionType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Mission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date createTime;

    @Enumerated(EnumType.STRING)
    private MissionStatus status;

    @Enumerated(EnumType.STRING)
    private MissionType type;

    //总次数
    private int totalTimes;

    /**
     * 该任务完成后获取的积分
     */
    private int integral;

    //奖励的现金
    private float rewardMoney;

    public float getRewardMoney() {
        return rewardMoney;
    }

    public void setRewardMoney(float rewardMoney) {
        this.rewardMoney = rewardMoney;
    }

    public MissionType getType() {
        return type;
    }

    public void setType(MissionType type) {
        this.type = type;
    }

    public String getTypeTitle() {
        return type == null ? null : type.getTitle();
    }

    public int getIntegral() {
        return integral;
    }

    public void setIntegral(int integral) {
        this.integral = integral;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.getType().name(), null, null);
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public MissionStatus getStatus() {
        return status;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public void setStatus(MissionStatus status) {
        this.status = status;
    }

    public int getTotalTimes() {
        return totalTimes;
    }

    public void setTotalTimes(int totalTimes) {
        this.totalTimes = totalTimes;
    }
}
