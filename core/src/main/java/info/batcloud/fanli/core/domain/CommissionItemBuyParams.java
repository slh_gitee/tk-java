package info.batcloud.fanli.core.domain;

import info.batcloud.fanli.core.enums.EcomPlat;

public class CommissionItemBuyParams<T> {

    private EcomPlat ecomPlat;

    private T data;

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
