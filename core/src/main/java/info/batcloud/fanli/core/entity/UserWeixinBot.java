package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.UserWeixinBotStatus;

import javax.persistence.*;
import java.util.Date;

@Entity
public class UserWeixinBot {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long userId;
    private Date createTime;
    private Date aliveTime;
    @Enumerated(EnumType.STRING)
    private UserWeixinBotStatus status;
    private String uin;
    private Date deadTime;
    private String context;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getAliveTime() {
        return aliveTime;
    }

    public void setAliveTime(Date aliveTime) {
        this.aliveTime = aliveTime;
    }

    public UserWeixinBotStatus getStatus() {
        return status;
    }

    public void setStatus(UserWeixinBotStatus status) {
        this.status = status;
    }

    public String getUin() {
        return uin;
    }

    public void setUin(String uin) {
        this.uin = uin;
    }

    public Date getDeadTime() {
        return deadTime;
    }

    public void setDeadTime(Date deadTime) {
        this.deadTime = deadTime;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
