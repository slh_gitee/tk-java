package info.batcloud.fanli.core.config;

public class FanliConfig {

    //用户购买的返利比例
    private Float rate;

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }
}
