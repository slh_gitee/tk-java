package info.batcloud.fanli.core.dto;


import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.Date;

public class FootmarkDTO {

    private Long id;
    private Long userId;

    private String itemPicUrl;

    private float itemOriginPrice;

    private EcomPlat itemEcomPlat;

    private String itemTitle;

    private float itemPrice;

    private long itemId;

    private String itemType;

    private Date createTime;

    public EcomPlat getItemEcomPlat() {
        return itemEcomPlat;
    }

    public void setItemEcomPlat(EcomPlat itemEcomPlat) {
        this.itemEcomPlat = itemEcomPlat;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public float getItemOriginPrice() {
        return itemOriginPrice;
    }

    public void setItemOriginPrice(float itemOriginPrice) {
        this.itemOriginPrice = itemOriginPrice;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
