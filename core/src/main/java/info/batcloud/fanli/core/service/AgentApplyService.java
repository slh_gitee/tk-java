package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.domain.AgentApplyBO;
import info.batcloud.fanli.core.enums.AgentApplyStatus;

public interface AgentApplyService {

    Paging<AgentApplyBO> search(SearchParam param);

    void verify(long id, AgentApplyStatus status);

    class SearchParam extends PagingParam {

        private String phone;

        private AgentApplyStatus status;

        public AgentApplyStatus getStatus() {
            return status;
        }

        public void setStatus(AgentApplyStatus status) {
            this.status = status;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

    }

}
