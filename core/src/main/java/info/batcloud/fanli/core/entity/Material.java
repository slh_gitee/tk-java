package info.batcloud.fanli.core.entity;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.domain.MaterialConfig;
import info.batcloud.fanli.core.enums.MaterialStatus;
import info.batcloud.fanli.core.helper.JSONHelper;

import javax.persistence.*;
import java.util.Date;

@Entity
@Cacheable
public class Material {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String configJson;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @Enumerated(EnumType.STRING)
    private MaterialStatus status;

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public MaterialStatus getStatus() {
        return status;
    }

    public void setStatus(MaterialStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConfigJson() {
        return configJson;
    }

    public void setConfigJson(String configJson) {
        this.configJson = configJson;
    }

    @Transient
    public MaterialConfig getConfig() {
        if (this.configJson == null) {
            return null;
        }
        return JSON.parseObject(JSONHelper.clean(this.configJson), MaterialConfig.class);
    }


}
