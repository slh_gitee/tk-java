package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.PhoneAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PhoneAccountRepository extends PagingAndSortingRepository<PhoneAccount, Long>{

    PhoneAccount findByPhone(String phone);

}
