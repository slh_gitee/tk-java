package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum TkOrderQueryType implements EnumTitle {

    CREATE_TIME(1), SETTLE_TIME(3), PAY_TIME(2);
    public long value;

    TkOrderQueryType(long value) {
        this.value = value;
    }


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, "", null);
    }

}
