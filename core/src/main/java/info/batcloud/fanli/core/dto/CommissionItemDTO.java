package info.batcloud.fanli.core.dto;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.CommissionItemStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.helper.UrlHelper;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class CommissionItemDTO implements Serializable {

    private Long id;

    private String title;

    private String picUrl;

    private String whitePicUrl;

    private Float price;

    private Float originPrice;

    private String shopTitle;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date listTime; //上架时间

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date delistTime;//下架时间

    private String description;

    private String summary;

    private int sales;

    private List<String> imgList;

    private String shopcatPath;

    private Long shopcatId;

    private String shopcatTitle;

    private CommissionItemStatus status;//状态

    //来源电商平台
    private EcomPlat ecomPlat;

    private Long sourceItemId;

    private String sourceItemUrl;

    private String sourceItemClickUrl;

    private Long sourceCatId;

    private String itemPlace; //货源地

    private Float commissionRate;

    private boolean highCommission;

    private boolean coupon;

    private String couponClickUrl;

    private String couponStartTime;

    private String couponEndTime;

    private String couponInfo;

    private Integer couponTotalCount;

    private Integer couponRemainCount;

    private Float couponValue;

    private Float shareCommission;

    private boolean freeShipment;

    private boolean tbkZt;

    public boolean isTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(boolean tbkZt) {
        this.tbkZt = tbkZt;
    }

    public String getWhitePicUrl() {
        return whitePicUrl;
    }

    public void setWhitePicUrl(String whitePicUrl) {
        this.whitePicUrl = whitePicUrl;
    }

    public boolean isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(boolean freeShipment) {
        this.freeShipment = freeShipment;
    }

    public String getShopcatTitle() {
        return shopcatTitle;
    }

    public void setShopcatTitle(String shopcatTitle) {
        this.shopcatTitle = shopcatTitle;
    }

    public Float getShareCommission() {
        return shareCommission;
    }

    public Float getFullCommission() {
        return price * commissionRate / 100;
    }

    public void setShareCommission(Float shareCommission) {
        this.shareCommission = shareCommission;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return UrlHelper.toUrl(picUrl);
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getListTime() {
        return listTime;
    }

    public void setListTime(Date listTime) {
        this.listTime = listTime;
    }

    public Date getDelistTime() {
        return delistTime;
    }

    public void setDelistTime(Date delistTime) {
        this.delistTime = delistTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public List<String> getImgList() {
        return imgList == null ? Collections.EMPTY_LIST
                : imgList.stream().map(o -> UrlHelper.toUrl(o)).collect(Collectors.toList());
    }

    public void setImgList(List<String> imgList) {
        this.imgList = imgList;
    }

    public String getShopcatPath() {
        return shopcatPath;
    }

    public void setShopcatPath(String shopcatPath) {
        this.shopcatPath = shopcatPath;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }

    public CommissionItemStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionItemStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public String getEcomPlatTitle() {
        return getEcomPlat() == null ? null : getEcomPlat().getTitle();
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public String getSourceItemClickUrl() {
        return sourceItemClickUrl;
    }

    public void setSourceItemClickUrl(String sourceItemClickUrl) {
        this.sourceItemClickUrl = sourceItemClickUrl;
    }

    public Long getSourceCatId() {
        return sourceCatId;
    }

    public void setSourceCatId(Long sourceCatId) {
        this.sourceCatId = sourceCatId;
    }

    public String getItemPlace() {
        return itemPlace;
    }

    public void setItemPlace(String itemPlace) {
        this.itemPlace = itemPlace;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public boolean isHighCommission() {
        return highCommission;
    }

    public void setHighCommission(boolean highCommission) {
        this.highCommission = highCommission;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getCouponClickUrl() {
        return couponClickUrl;
    }

    public void setCouponClickUrl(String couponClickUrl) {
        this.couponClickUrl = couponClickUrl;
    }

    public String getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(String couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public String getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(String couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public String getCouponInfo() {
        return couponInfo;
    }

    public void setCouponInfo(String couponInfo) {
        this.couponInfo = couponInfo;
    }

    public Integer getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(Integer couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public Integer getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(Integer couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    @Override
    public String toString() {
        return "CommissionItemDTO{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", whitePicUrl='" + whitePicUrl + '\'' +
                ", price=" + price +
                ", originPrice=" + originPrice +
                ", shopTitle='" + shopTitle + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", listTime=" + listTime +
                ", delistTime=" + delistTime +
                ", description='" + description + '\'' +
                ", summary='" + summary + '\'' +
                ", sales=" + sales +
                ", imgList=" + imgList +
                ", shopcatPath='" + shopcatPath + '\'' +
                ", shopcatId=" + shopcatId +
                ", shopcatTitle='" + shopcatTitle + '\'' +
                ", status=" + status +
                ", ecomPlat=" + ecomPlat +
                ", sourceItemId=" + sourceItemId +
                ", sourceItemUrl='" + sourceItemUrl + '\'' +
                ", sourceItemClickUrl='" + sourceItemClickUrl + '\'' +
                ", sourceCatId=" + sourceCatId +
                ", itemPlace='" + itemPlace + '\'' +
                ", commissionRate=" + commissionRate +
                ", highCommission=" + highCommission +
                ", coupon=" + coupon +
                ", couponClickUrl='" + couponClickUrl + '\'' +
                ", couponStartTime='" + couponStartTime + '\'' +
                ", couponEndTime='" + couponEndTime + '\'' +
                ", couponInfo='" + couponInfo + '\'' +
                ", couponTotalCount=" + couponTotalCount +
                ", couponRemainCount=" + couponRemainCount +
                ", couponValue=" + couponValue +
                ", shareCommission=" + shareCommission +
                ", freeShipment=" + freeShipment +
                ", tbkZt=" + tbkZt +
                '}';
    }
}
