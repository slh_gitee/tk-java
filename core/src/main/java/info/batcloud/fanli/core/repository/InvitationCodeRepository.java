package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.InvitationCode;
import org.springframework.data.repository.CrudRepository;

public interface InvitationCodeRepository extends CrudRepository<InvitationCode, Long> {
}
