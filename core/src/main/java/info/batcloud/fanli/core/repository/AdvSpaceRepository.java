package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.AdvSpace;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AdvSpaceRepository extends PagingAndSortingRepository<AdvSpace, Long>, JpaSpecificationExecutor<AdvSpace> {

    int countByCodeAndDeleted(String code, boolean deleted);
    int countByCodeAndDeletedAndIdIsNot(String code, boolean deleted, long id);
    AdvSpace findByCodeAndDeleted(String code, boolean deleted);

    List<AdvSpace> findByDeletedFalse();
}
