package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.RedPacket;
import info.batcloud.fanli.core.enums.RedPacketStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;

public interface RedPacketRepository extends PagingAndSortingRepository<RedPacket, Long>, JpaSpecificationExecutor<RedPacket> {

    List<RedPacket> findByStartTimeLessThanEqualAndEndTimeGreaterThanEqualAndStatus(Date startTime, Date endTime, RedPacketStatus status);

}
