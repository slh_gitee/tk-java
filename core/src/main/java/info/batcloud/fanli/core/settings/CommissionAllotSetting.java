package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class CommissionAllotSetting implements Serializable {

    private float ordinarySelfCommissionRate;//自购返佣金
    private float plusSelfCommissionRate;//自购返佣金

    private float plusCommissionRate;
    private float plusParentCommissionRate;
    private float plusGrandParentCommissionRate;

    private float carrierCommissionRate;
    private float carrierParentCommissionRate;

    private float chiefCommissionRate;
    private float chiefParentCommissionRate;

    //市级代理
    private float cityAgentCommissionRate;
    //县区级代理
    private float districtAgentCommissionRate;
    //服务商代理
    private float relationAgentCommissionRate;

    public float getOrdinarySelfCommissionRate() {
        return ordinarySelfCommissionRate;
    }

    public void setOrdinarySelfCommissionRate(float ordinarySelfCommissionRate) {
        this.ordinarySelfCommissionRate = ordinarySelfCommissionRate;
    }

    public float getPlusSelfCommissionRate() {
        return plusSelfCommissionRate;
    }

    public void setPlusSelfCommissionRate(float plusSelfCommissionRate) {
        this.plusSelfCommissionRate = plusSelfCommissionRate;
    }

    public float getRelationAgentCommissionRate() {
        return relationAgentCommissionRate;
    }

    public void setRelationAgentCommissionRate(float relationAgentCommissionRate) {
        this.relationAgentCommissionRate = relationAgentCommissionRate;
    }

    public float getPlusCommissionRate() {
        return plusCommissionRate;
    }

    public void setPlusCommissionRate(float plusCommissionRate) {
        this.plusCommissionRate = plusCommissionRate;
    }

    public float getPlusParentCommissionRate() {
        return plusParentCommissionRate;
    }

    public void setPlusParentCommissionRate(float plusParentCommissionRate) {
        this.plusParentCommissionRate = plusParentCommissionRate;
    }

    public float getPlusGrandParentCommissionRate() {
        return plusGrandParentCommissionRate;
    }

    public void setPlusGrandParentCommissionRate(float plusGrandParentCommissionRate) {
        this.plusGrandParentCommissionRate = plusGrandParentCommissionRate;
    }

    public float getCarrierParentCommissionRate() {
        return carrierParentCommissionRate;
    }

    public void setCarrierParentCommissionRate(float carrierParentCommissionRate) {
        this.carrierParentCommissionRate = carrierParentCommissionRate;
    }

    public float getCarrierCommissionRate() {
        return carrierCommissionRate;
    }

    public void setCarrierCommissionRate(float carrierCommissionRate) {
        this.carrierCommissionRate = carrierCommissionRate;
    }

    public float getChiefCommissionRate() {
        return chiefCommissionRate;
    }

    public void setChiefCommissionRate(float chiefCommissionRate) {
        this.chiefCommissionRate = chiefCommissionRate;
    }

    public float getChiefParentCommissionRate() {
        return chiefParentCommissionRate;
    }

    public void setChiefParentCommissionRate(float chiefParentCommissionRate) {
        this.chiefParentCommissionRate = chiefParentCommissionRate;
    }

    public float getCityAgentCommissionRate() {
        return cityAgentCommissionRate;
    }

    public void setCityAgentCommissionRate(float cityAgentCommissionRate) {
        this.cityAgentCommissionRate = cityAgentCommissionRate;
    }

    public float getDistrictAgentCommissionRate() {
        return districtAgentCommissionRate;
    }

    public void setDistrictAgentCommissionRate(float districtAgentCommissionRate) {
        this.districtAgentCommissionRate = districtAgentCommissionRate;
    }
}
