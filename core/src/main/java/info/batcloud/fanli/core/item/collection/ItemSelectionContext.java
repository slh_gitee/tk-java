package info.batcloud.fanli.core.item.collection;

public class ItemSelectionContext extends Context{

    private long itemSelectionId;

    public long getItemSelectionId() {
        return itemSelectionId;
    }

    public void setItemSelectionId(long itemSelectionId) {
        this.itemSelectionId = itemSelectionId;
    }
}
