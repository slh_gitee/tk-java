package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.UserServiceSubscriptionDTO;
import info.batcloud.fanli.core.enums.Service;
import info.batcloud.fanli.core.enums.TimeUnit;
import info.batcloud.fanli.core.enums.UserServiceSubscriptionStatus;

public interface UserServiceSubscriptionService {

    Paging<UserServiceSubscriptionDTO> search(UserServiceSubscriptionService.SearchParam param);

    UserServiceSubscriptionDTO addUserServiceSubscription(UserServiceSubscriptionService.UserServiceSubscriptionAddParam param);

    void setStatus(long id, UserServiceSubscriptionStatus status);
    void setStatus(long userId, Service service, UserServiceSubscriptionStatus status);

    boolean checkServiceValid(long userId, Service service);

    class UserServiceSubscriptionAddParam {
        private Long userId;
        private TimeUnit timeUnit;
        private Service service;
        private int timeValue;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }

        public void setTimeUnit(TimeUnit timeUnit) {
            this.timeUnit = timeUnit;
        }

        public int getTimeValue() {
            return timeValue;
        }

        public void setTimeValue(int timeValue) {
            this.timeValue = timeValue;
        }
    }

    class SearchParam extends PagingParam {

        private Long userId;
        private String phone;
        private UserServiceSubscriptionStatus status;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        private Service service;

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public UserServiceSubscriptionStatus getStatus() {
            return status;
        }

        public void setStatus(UserServiceSubscriptionStatus status) {
            this.status = status;
        }

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }
    }

}
