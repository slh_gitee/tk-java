package info.batcloud.fanli.core.item.collection;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.CommissionItemFetchDTO;
import info.batcloud.fanli.core.enums.ItemSelectionType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.function.Consumer;

@Service
public class TaobaoSelectionSelectionCollector extends AbstractItemSelectionCollector<TaobaoSelectionContext> {

    private static final Logger logger = LoggerFactory.getLogger(TaobaoSelectionSelectionCollector.class);

    @Inject
    private TbkDgOptimusMaterialSelectionCollector tbkDgMaterialOptionalSelectionCollector;

    @Override
    public CollectResult collect(TaobaoSelectionContext context, Consumer<Paging<CommissionItemFetchDTO>> pagingConsumer) {
        TbkDgOptimusMaterialContext dgContext = new TbkDgOptimusMaterialContext();
        dgContext.setMaterialId(31539L);
        dgContext.setFavoritesId(context.getSelectionId().toString());
        CollectResult result = this.tbkDgMaterialOptionalSelectionCollector.collect(dgContext, pagingConsumer);
        return result;
    }

    @Override
    public Class<TaobaoSelectionContext> getContextType() {
        return TaobaoSelectionContext.class;
    }

    @Override
    protected ItemSelectionType getItemSelectionType() {
        return ItemSelectionType.TAOBAO_SELECTION;
    }

}
