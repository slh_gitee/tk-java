package info.batcloud.fanli.core.helper;

import com.huaban.analysis.jieba.JiebaSegmenter;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class StringHelper {

    public static String protect(String str) {
        if (StringUtils.isBlank(str)) {
            return str;
        }
        //如果是手机号
        if (str.length() == 11) {
            return str.substring(0, 3) + "****" + str.substring(7);
        }
        int length = str.length();
        int subLength = length / 3;
        if (subLength <= 1) {
            return str.substring(0, 1) + StringUtils.repeat("*", length - 1);
        }
        return str.substring(0, subLength) + StringUtils.repeat("*", subLength) + str.substring(str.length() - subLength);
    }

    public static List<String> segment(String keywords) {
        JiebaSegmenter segmenter = new JiebaSegmenter();
        return segmenter.sentenceProcess(keywords);
    }

}
