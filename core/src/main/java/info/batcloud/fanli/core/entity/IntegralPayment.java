package info.batcloud.fanli.core.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("IntegralPayment")
public class IntegralPayment extends Payment{

    private float integral;

    /**
     * 积分等值的现金金额
     * */
    private float equivalentMoney;

    public float getEquivalentMoney() {
        return equivalentMoney;
    }

    public void setEquivalentMoney(float equivalentMoney) {
        this.equivalentMoney = equivalentMoney;
    }

    public float getIntegral() {
        return integral;
    }

    public void setIntegral(float integral) {
        this.integral = integral;
    }
}
