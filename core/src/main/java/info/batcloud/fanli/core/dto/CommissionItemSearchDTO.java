package info.batcloud.fanli.core.dto;

import com.alibaba.fastjson.annotation.JSONField;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.helper.UrlHelper;

import java.io.Serializable;

public class CommissionItemSearchDTO implements Serializable {

    private Long id;

    private String title;

    @JSONField(name = "pic_url")
    private String picUrl;

    @JSONField(name = "white_pic_url")
    private String whitePicUrl;

    private Float price;

    @JSONField(name = "ecom_plat")
    private EcomPlat ecomPlat;

    @JSONField(name = "commission_rate")
    private Float commissionRate; //返利比例

    private int sales;

    @JSONField(name = "origin_price")
    private Float originPrice;

    @JSONField(name = "coupon_value")
    private Float couponValue;

    @JSONField(name = "shop_title")
    private String shopTitle;

    private Float shareCommission;//分享赚的佣金量

    private Float rewardShareCommission;

    @JSONField(name = "free_shipment")
    private int freeShipment;

    @JSONField(name = "jd_sale")
    private int jdSale;

    private String description;

    private Long sourceItemId;

    private String sourceItemUrl;

    private boolean coupon;

    @JSONField(name = "tbk_zt")
    private boolean tbkZt;

    public boolean isTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(boolean tbkZt) {
        this.tbkZt = tbkZt;
    }

    public String getWhitePicUrl() {
        return whitePicUrl;
    }

    public void setWhitePicUrl(String whitePicUrl) {
        this.whitePicUrl = whitePicUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCoupon() {
        return coupon;
    }

    public void setCoupon(boolean coupon) {
        this.coupon = coupon;
    }

    public String getSourceItemUrl() {
        return sourceItemUrl;
    }

    public void setSourceItemUrl(String sourceItemUrl) {
        this.sourceItemUrl = sourceItemUrl;
    }

    public String getEcomPlatTitle() {
        return ecomPlat == null ? null : ecomPlat.getTitle();
    }

    public Long getSourceItemId() {
        return sourceItemId;
    }

    public void setSourceItemId(Long sourceItemId) {
        this.sourceItemId = sourceItemId;
    }

    public int getFreeShipment() {
        return freeShipment;
    }

    public int getJdSale() {
        return jdSale;
    }

    public void setJdSale(int jdSale) {
        this.jdSale = jdSale;
    }

    public int isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(int freeShipment) {
        this.freeShipment = freeShipment;
    }

    public Float getRewardShareCommission() {
        return rewardShareCommission;
    }

    public void setRewardShareCommission(Float rewardShareCommission) {
        this.rewardShareCommission = rewardShareCommission;
    }

    public Float getShareCommission() {
        return shareCommission;
    }

    public void setShareCommission(Float shareCommission) {
        this.shareCommission = shareCommission;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return UrlHelper.toUrl(picUrl);
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        return price == null ? null : Float.valueOf(price * 100).intValue() / 100f;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Float getOriginPrice() {
        return originPrice == null ? originPrice : Float.valueOf(originPrice * 100).intValue() / 100f;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public Float getCouponValue() {
        return couponValue == null ? couponValue : Float.valueOf(couponValue * 100).intValue() / 100f;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }
}
