package info.batcloud.fanli.core.service;

public interface ShortUrlService {

    String generate(String url);

}
