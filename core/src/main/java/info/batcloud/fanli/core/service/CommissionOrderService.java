package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.dto.CommissionOrderAddDTO;
import info.batcloud.fanli.core.dto.CommissionOrderDTO;
import info.batcloud.fanli.core.entity.CommissionOrder;
import info.batcloud.fanli.core.entity.User;
import info.batcloud.fanli.core.enums.CommissionOrderSort;
import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public interface CommissionOrderService {

    void saveCommissionOrderList(List<CommissionOrderAddDTO> list);

    void settle();

    void settleCommissionOrder(long commissionOrderId);

    int countValidCommissionOrderNumByUserId(long userId);

    Paging<CommissionOrderDTO> search(SearchParam param);

    Result allocateCommissionOrderToUser(long userId, long commissionOrderId);

    File exportCommissionOrder(ExportParam param) throws IOException;

    class ExportParam extends SearchParam {

        private int maxCount = 1000;

        public int getMaxCount() {
            return maxCount;
        }

        public void setMaxCount(int maxCount) {
            this.maxCount = maxCount;
        }
    }

    class SearchParam extends PagingParam {
        private String itemTitle;

        private Long itemId;
        private Long userId;
        private CommissionOrderStatus status;
        private EcomPlat ecomPlat;
        private String orderNo;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minCreateTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxCreateTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date minSettledTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date maxSettledTime;

        private Long commissionItemId;

        private boolean fetchAllotEarning;

        private CommissionOrderSort sort;

        public boolean isFetchAllotEarning() {
            return fetchAllotEarning;
        }

        public void setFetchAllotEarning(boolean fetchAllotEarning) {
            this.fetchAllotEarning = fetchAllotEarning;
        }

        public String getItemTitle() {
            return itemTitle;
        }

        public void setItemTitle(String itemTitle) {
            this.itemTitle = itemTitle;
        }

        public Long getItemId() {
            return itemId;
        }

        public void setItemId(Long itemId) {
            this.itemId = itemId;
        }

        public CommissionOrderSort getSort() {
            return sort;
        }

        public void setSort(CommissionOrderSort sort) {
            this.sort = sort;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public CommissionOrderStatus getStatus() {
            return status;
        }

        public void setStatus(CommissionOrderStatus status) {
            this.status = status;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public Date getMinCreateTime() {
            return minCreateTime;
        }

        public void setMinCreateTime(Date minCreateTime) {
            this.minCreateTime = minCreateTime;
        }

        public Date getMaxCreateTime() {
            return maxCreateTime;
        }

        public void setMaxCreateTime(Date maxCreateTime) {
            this.maxCreateTime = maxCreateTime;
        }

        public Date getMinSettledTime() {
            return minSettledTime;
        }

        public void setMinSettledTime(Date minSettledTime) {
            this.minSettledTime = minSettledTime;
        }

        public Date getMaxSettledTime() {
            return maxSettledTime;
        }

        public void setMaxSettledTime(Date maxSettledTime) {
            this.maxSettledTime = maxSettledTime;
        }

        public Long getCommissionItemId() {
            return commissionItemId;
        }

        public void setCommissionItemId(Long commissionItemId) {
            this.commissionItemId = commissionItemId;
        }
    }
}
