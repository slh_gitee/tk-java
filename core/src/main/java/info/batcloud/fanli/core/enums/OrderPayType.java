package info.batcloud.fanli.core.enums;
import info.batcloud.fanli.core.context.StaticContext;

public enum OrderPayType {


    MONEY, INTEGRAL, ALIPAY, TRANSFER;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
