package info.batcloud.fanli.core.domain.utam.res;

import info.batcloud.fanli.core.domain.ApiResponse;

public class ItemCollectResponse extends ApiResponse<ItemCollectResponse.Data> {

    public static class Data {
        private int totalNum;

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }
    }
}
