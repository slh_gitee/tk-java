package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Order;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
}
