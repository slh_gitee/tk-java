package info.batcloud.fanli.core.item.collection;

public class TaobaoSelectionContext extends Context {

    private Long selectionId;

    public Long getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(Long selectionId) {
        this.selectionId = selectionId;
    }
}
