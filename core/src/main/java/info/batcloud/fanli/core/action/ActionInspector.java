package info.batcloud.fanli.core.action;

import info.batcloud.fanli.core.action.domain.Action;

public interface ActionInspector<T extends Action> {

    void inspect(T mission);

}
