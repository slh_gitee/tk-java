package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum BundleJsPackageStatus implements EnumTitle{

    WAIT_PUBLISH, PUBLISHED;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
