package info.batcloud.fanli.core.mapper.stat;

import info.batcloud.fanli.core.domain.stat.CommissionOrderAllotEarning;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface CommissionOrderMapper {

    List<CommissionOrderAllotEarning> statAllotEarningByCommissionOrderIds(@Param("commissionOrderIds") List<Long> commissionOrderIds);

}
