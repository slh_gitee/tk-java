package info.batcloud.fanli.core.settings;

import java.io.Serializable;

/**
 * 签到设置
 * */
public class SignSetting implements Serializable {

    //签到周期
    private int signCycleTimes;

    public int getSignCycleTimes() {
        return signCycleTimes;
    }

    public void setSignCycleTimes(int signCycleTimes) {
        this.signCycleTimes = signCycleTimes;
    }
}
