package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.placeholder.ReplaceContext;

public interface PlaceholderService {

    String clean(ReplaceContext replaceContext);

}
