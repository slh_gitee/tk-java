package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.FundTransferOrderDTO;

public interface FundTransferOrderService {

    FundTransferOrderDTO findById(long id);

}
