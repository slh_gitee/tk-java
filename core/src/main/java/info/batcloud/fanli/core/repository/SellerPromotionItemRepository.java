package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.SellerPromotionItem;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SellerPromotionItemRepository extends PagingAndSortingRepository<SellerPromotionItem, Long>, JpaSpecificationExecutor<SellerPromotionItem> {
}
