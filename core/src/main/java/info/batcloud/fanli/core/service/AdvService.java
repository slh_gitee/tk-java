package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.AdvDTO;
import info.batcloud.fanli.core.operation.center.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface AdvService {

    void saveAdv(AdvAddParam param);

    void updateAdv(AdvUpdateParam param);

    void validById(long id);

    void invalidById(long id);

    void deleteById(long id);

    AdvDTO findValidById(long id);

    List<AdvDTO> findByIds(List<Long> ids);
    List<AdvDTO> findValidByIds(List<Long> ids);

    List<AdvDTO> filterByUserId(Long userId, List<AdvDTO> advList);

    Paging<AdvDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private String name;
        private Boolean valid;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getValid() {
            return valid;
        }

        public void setValid(Boolean valid) {
            this.valid = valid;
        }
    }

    class AdvUpdateParam {
        private Long id;
        private String name;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;
        private boolean valid;
        private String pic;
        private Page page;
        private String params;
        private int picWidth;
        private int picHeight;

        private Long crowdId;

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }

        public int getPicWidth() {
            return picWidth;
        }

        public void setPicWidth(int picWidth) {
            this.picWidth = picWidth;
        }

        public int getPicHeight() {
            return picHeight;
        }

        public void setPicHeight(int picHeight) {
            this.picHeight = picHeight;
        }

        public Page getPage() {
            return page;
        }

        public void setPage(Page page) {
            this.page = page;
        }

        public String getParams() {
            return params;
        }

        public void setParams(String params) {
            this.params = params;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }
    class AdvAddParam {

        private Page page;
        private String params;
        private String name;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endTime;
        private boolean valid;
        private String pic;

        private int picWidth;
        private int picHeight;
        private Long crowdId;

        public Long getCrowdId() {
            return crowdId;
        }

        public void setCrowdId(Long crowdId) {
            this.crowdId = crowdId;
        }

        public int getPicWidth() {
            return picWidth;
        }

        public void setPicWidth(int picWidth) {
            this.picWidth = picWidth;
        }

        public int getPicHeight() {
            return picHeight;
        }

        public void setPicHeight(int picHeight) {
            this.picHeight = picHeight;
        }

        public Page getPage() {
            return page;
        }

        public void setPage(Page page) {
            this.page = page;
        }

        public String getParams() {
            return params;
        }

        public void setParams(String params) {
            this.params = params;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Date getStartTime() {
            return startTime;
        }

        public void setStartTime(Date startTime) {
            this.startTime = startTime;
        }

        public Date getEndTime() {
            return endTime;
        }

        public void setEndTime(Date endTime) {
            this.endTime = endTime;
        }

        public boolean isValid() {
            return valid;
        }

        public void setValid(boolean valid) {
            this.valid = valid;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }

}
