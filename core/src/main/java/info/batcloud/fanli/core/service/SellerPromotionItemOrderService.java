package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.SellerPromotionItemOrderDTO;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemOrderStatus;

public interface SellerPromotionItemOrderService {

    RushOrderCreateResult rushCreateOrder(RushOrderCreateParam param);

    boolean ifLockedByUserIdAndSellerPromotionItemId(long userId, long sellerPromotionItemId);
    boolean ifLockedByUserId(long userId);
    boolean ifFillTradeNoByUserIdAndSellerPromotionItemId(long userId, long sellerPromotionItemId);

    void verify(VerifyParam param);

    void rebateById(long id);
    void rebateBySellerIdAndId(long sellerId, long id);

    void fillTradeNo(long userId, long orderId, String tradeNo);

    void releaseWaitFillTradeTimeoutOrder();

    void handleWaitVerifyTimeoutOrder();

    void handleVerifyFailTimeoutOrder();

    void autoRebateTimeoutOrder();

    SellerPromotionItemOrderDTO findByUserIdAndPromotionItemId(long userId, long sellerPromotionItemId);

    class VerifyParam {
        private Long id;
        private Long sellerId;
        private String verifyRemark;
        private boolean success;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getSellerId() {
            return sellerId;
        }

        public void setSellerId(Long sellerId) {
            this.sellerId = sellerId;
        }

        public String getVerifyRemark() {
            return verifyRemark;
        }

        public void setVerifyRemark(String verifyRemark) {
            this.verifyRemark = verifyRemark;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }
    }

    class RushOrderCreateResult extends Result {

    }

    Paging<SellerPromotionItemOrderDTO> search(SearchParam param);

    class SearchParam extends PagingParam {
        private Long userId;
        private Long sellerPromotionItemId;
        private Long sellerId;
        private SellerPromotionItemOrderStatus status;
        private Long sourceItemId;
        private EcomPlat ecomPlat;

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public Long getSourceItemId() {
            return sourceItemId;
        }

        public void setSourceItemId(Long sourceItemId) {
            this.sourceItemId = sourceItemId;
        }

        public Long getSellerId() {
            return sellerId;
        }

        public void setSellerId(Long sellerId) {
            this.sellerId = sellerId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getSellerPromotionItemId() {
            return sellerPromotionItemId;
        }

        public void setSellerPromotionItemId(Long sellerPromotionItemId) {
            this.sellerPromotionItemId = sellerPromotionItemId;
        }

        public SellerPromotionItemOrderStatus getStatus() {
            return status;
        }

        public void setStatus(SellerPromotionItemOrderStatus status) {
            this.status = status;
        }
    }

    class RushOrderCreateParam {
        private Long userId;
        private Long sellerPromotionItemId;

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public Long getSellerPromotionItemId() {
            return sellerPromotionItemId;
        }

        public void setSellerPromotionItemId(Long sellerPromotionItemId) {
            this.sellerPromotionItemId = sellerPromotionItemId;
        }
    }
}
