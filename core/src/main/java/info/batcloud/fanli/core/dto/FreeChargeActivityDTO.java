package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.domain.MaterialConfig;
import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.FreeChargeActivityStatus;
import info.batcloud.fanli.core.helper.OSSImageHelper;

import java.util.Date;

public class FreeChargeActivityDTO {

    private Long id;

    private String name;

    private String description;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updateTime;

    private ActivityFreq freq;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date lastDrawTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endTime;

    private String crowdName;

    private int maxUserDrawNum;

    private int drawNum;

    private Long crowdId;

    private String helpUrl;

    private FreeChargeActivityStatus status;

    private String markIcon;

    private Long materialId;

    private String materialName;

    private MaterialConfig materialConfig;

    private String btnTitle;

    private String confirmDescription;

    public String getConfirmDescription() {
        return confirmDescription;
    }

    public void setConfirmDescription(String confirmDescription) {
        this.confirmDescription = confirmDescription;
    }

    public String getBtnTitle() {
        return btnTitle;
    }

    public void setBtnTitle(String btnTitle) {
        this.btnTitle = btnTitle;
    }

    public Long getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Long materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public MaterialConfig getMaterialConfig() {
        return materialConfig;
    }

    public void setMaterialConfig(MaterialConfig materialConfig) {
        this.materialConfig = materialConfig;
    }

    public String getMarkIconUrl() {
        return OSSImageHelper.toUrl(this.getMarkIcon());
    }

    public String getMarkIcon() {
        return markIcon;
    }

    public void setMarkIcon(String markIcon) {
        this.markIcon = markIcon;
    }

    public String getHelpUrl() {
        return helpUrl;
    }

    public void setHelpUrl(String helpUrl) {
        this.helpUrl = helpUrl;
    }

    public int getMaxUserDrawNum() {
        return maxUserDrawNum;
    }

    public void setMaxUserDrawNum(int maxUserDrawNum) {
        this.maxUserDrawNum = maxUserDrawNum;
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public String getFreqTitle() {
        return freq == null ? null : freq.getTitle();
    }

    public int getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(int drawNum) {
        this.drawNum = drawNum;
    }

    public String getCrowdName() {
        return crowdName;
    }

    public void setCrowdName(String crowdName) {
        this.crowdName = crowdName;
    }

    public Long getCrowdId() {
        return crowdId;
    }

    public void setCrowdId(Long crowdId) {
        this.crowdId = crowdId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public ActivityFreq getFreq() {
        return freq;
    }

    public void setFreq(ActivityFreq freq) {
        this.freq = freq;
    }

    public Date getLastDrawTime() {
        return lastDrawTime;
    }

    public void setLastDrawTime(Date lastDrawTime) {
        this.lastDrawTime = lastDrawTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public FreeChargeActivityStatus getStatus() {
        return status;
    }

    public void setStatus(FreeChargeActivityStatus status) {
        this.status = status;
    }
}
