package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TaobaoPid;
import org.springframework.data.repository.CrudRepository;

public interface TaobaoPidRepository extends CrudRepository<TaobaoPid, Long> {

    TaobaoPid findTopByUsedOrderByIdAsc(boolean used);

}
