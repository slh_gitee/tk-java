package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;
import java.util.List;

@Single
public class TaobaoMaterialSetting implements Serializable {

    private List<Material> materialList;

    public List<Material> getMaterialList() {
        return materialList;
    }

    public void setMaterialList(List<Material> materialList) {
        this.materialList = materialList;
    }

    public static class Material {
        private String name;
        private Long id;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }
}
