package info.batcloud.fanli.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.CrowdStatus;
import info.batcloud.fanli.core.enums.UserLevel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Cacheable
public class Crowd {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date minCreateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date maxCreateTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date minActiveTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date maxActiveTime;

    @Enumerated(EnumType.STRING)
    private UserLevel userLevel;

    @Enumerated
    private CrowdStatus status;

    public String getUserLevelTitle() {
        return userLevel == null ? null : userLevel.getTitle();
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public CrowdStatus getStatus() {
        return status;
    }

    public void setStatus(CrowdStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getMinCreateTime() {
        return minCreateTime;
    }

    public void setMinCreateTime(Date minCreateTime) {
        this.minCreateTime = minCreateTime;
    }

    public Date getMaxCreateTime() {
        return maxCreateTime;
    }

    public void setMaxCreateTime(Date maxCreateTime) {
        this.maxCreateTime = maxCreateTime;
    }

    public Date getMinActiveTime() {
        return minActiveTime;
    }

    public void setMinActiveTime(Date minActiveTime) {
        this.minActiveTime = minActiveTime;
    }

    public Date getMaxActiveTime() {
        return maxActiveTime;
    }

    public void setMaxActiveTime(Date maxActiveTime) {
        this.maxActiveTime = maxActiveTime;
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(UserLevel userLevel) {
        this.userLevel = userLevel;
    }
}
