package info.batcloud.fanli.core.service;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public interface DdkService {

    void fetchOrder(OrderFetchParam param);

    boolean refreshAccessToken();

    class OrderFetchParam {
        private String pid;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startUpdateTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endUpdateTime;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public Date getStartUpdateTime() {
            return startUpdateTime;
        }

        public void setStartUpdateTime(Date startUpdateTime) {
            this.startUpdateTime = startUpdateTime;
        }

        public Date getEndUpdateTime() {
            return endUpdateTime;
        }

        public void setEndUpdateTime(Date endUpdateTime) {
            this.endUpdateTime = endUpdateTime;
        }

        @Override
        public String toString() {
            return String.format("[PID:%s,startUpdateTime:%s,endUpdateTime:%s]", new Object[]{this.pid, this.startUpdateTime, this.endUpdateTime});
        }
    }
}
