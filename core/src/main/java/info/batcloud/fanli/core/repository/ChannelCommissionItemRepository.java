package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.ChannelCommissionItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ChannelCommissionItemRepository extends CrudRepository<ChannelCommissionItem, Long>{

    List<ChannelCommissionItem> findByCommissionItemIdIn(List<Long> itemIds);
    ChannelCommissionItem findByCommissionItemIdAndChannelId(long commissionItemId, long channelId);

    List<ChannelCommissionItem> findByChannelId(long channelId);
    List<ChannelCommissionItem> findTop1000ByChannelId(long channelId);
}
