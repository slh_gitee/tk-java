package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.Gender;
import info.batcloud.fanli.core.enums.UserLevel;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nickname;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private Long cityId;

    private Long districtId;

    private Long provinceId;

    private String district;

    private String city;

    private String province;

    private String country;

    private String avatarUrl;

    private boolean locked;

    private Date createTime;

    private Date lastSignTime;

    private Date lastActiveTime;

    private int activeTimes;

    private String activeIp;

    //连续签到次数，总的
    private int continuousSignTimes;

    private String phone;

    private String password;

    //上级，如果有邀请的话
    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    private User superUser;

    //层级关系
    private String relationPath;

    private int relationDepth;

    private String taobaoPid;

    private Long taobaoAdzoneId;

    private String invitationCode;//邀请码

    private String weixinOpenId;

    private String weixinUnionId;

    private String pddPid;

    private Long jdPositionId;

    @Enumerated(EnumType.STRING)
    private UserLevel level;

    private Date levelExpireTime; //会员过期时间

    private Date levelChangeTime;

    private String alipayAccount;

    private Date loginTime;

    private int loginTimes;

    private String loginIp;

    private String openId;

    private boolean deleted;

    /**
     * 淘宝会员运营id
     */
    private String taobaoSpecialId;

    /**
     * 淘宝会员渠道id
     * */
    private String taobaoRelationId;

    public String getTaobaoRelationId() {
        return taobaoRelationId;
    }

    public void setTaobaoRelationId(String taobaoRelationId) {
        this.taobaoRelationId = taobaoRelationId;
    }

    public String getTaobaoSpecialId() {
        return taobaoSpecialId;
    }

    public void setTaobaoSpecialId(String taobaoSpecialId) {
        this.taobaoSpecialId = taobaoSpecialId;
    }

    public Long getJdPositionId() {
        return jdPositionId;
    }

    public void setJdPositionId(Long jdPositionId) {
        this.jdPositionId = jdPositionId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public Date getLevelChangeTime() {
        return levelChangeTime;
    }

    public void setLevelChangeTime(Date levelChangeTime) {
        this.levelChangeTime = levelChangeTime;
    }

    public String getWeixinUnionId() {
        return weixinUnionId;
    }

    public void setWeixinUnionId(String weixinUnionId) {
        this.weixinUnionId = weixinUnionId;
    }

    public int getActiveTimes() {
        return activeTimes;
    }

    public void setActiveTimes(int activeTimes) {
        this.activeTimes = activeTimes;
    }

    public String getActiveIp() {
        return activeIp;
    }

    public void setActiveIp(String activeIp) {
        this.activeIp = activeIp;
    }

    public Date getLastActiveTime() {
        return lastActiveTime;
    }

    public void setLastActiveTime(Date lastActiveTime) {
        this.lastActiveTime = lastActiveTime;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public int getLoginTimes() {
        return loginTimes;
    }

    public void setLoginTimes(int loginTimes) {
        this.loginTimes = loginTimes;
    }

    public String getLoginIp() {
        return loginIp;
    }

    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public String getPddPid() {
        return pddPid;
    }

    public void setPddPid(String pddPid) {
        this.pddPid = pddPid;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public int getRelationDepth() {
        return relationDepth;
    }

    public void setRelationDepth(int relationDepth) {
        this.relationDepth = relationDepth;
    }

    public Date getLevelExpireTime() {
        return levelExpireTime;
    }

    public void setLevelExpireTime(Date levelExpireTime) {
        this.levelExpireTime = levelExpireTime;
    }

    public UserLevel getLevel() {
        return level;
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public Long getTaobaoAdzoneId() {
        return taobaoAdzoneId;
    }

    public void setTaobaoAdzoneId(Long taobaoAdzoneId) {
        this.taobaoAdzoneId = taobaoAdzoneId;
    }

    public String getRelationPath() {
        return relationPath;
    }

    public String getWeixinOpenId() {
        return weixinOpenId;
    }

    public void setWeixinOpenId(String weixinOpenId) {
        this.weixinOpenId = weixinOpenId;
    }

    public List<Long> getRelationList() {
        return this.getRelationPath() == null
                ? new ArrayList<>() :
                Arrays.stream(this.getRelationPath().split("/"))
                        .map(p -> Long.valueOf(p))
                        .collect(Collectors.toList());
    }

    public void setRelationPath(String relationPath) {
        this.relationPath = relationPath;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTaobaoPid() {
        return taobaoPid;
    }

    public void setTaobaoPid(String taobaoPid) {
        this.taobaoPid = taobaoPid;
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public User getSuperUser() {
        return superUser;
    }

    public void setSuperUser(User superUser) {
        this.superUser = superUser;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getLastSignTime() {
        return lastSignTime;
    }

    public void setLastSignTime(Date lastSignTime) {
        this.lastSignTime = lastSignTime;
    }

    public int getContinuousSignTimes() {
        return continuousSignTimes;
    }

    public void setContinuousSignTimes(int continuousSignTimes) {
        this.continuousSignTimes = continuousSignTimes;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getGenderTitle() {
        return getGender() == null ? null : getGender().title;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
