package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//限时抢购
@Single
public class FlashSaleSetting implements Serializable {

    private List<Season> seasonList = new ArrayList<>();

    private String shareTitle;

    private String shareDescription;

    private String shareThumbUrl;

    private String shareUrl;

    private String shareText;

    private String rules;

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public String getShareText() {
        return shareText;
    }

    public void setShareText(String shareText) {
        this.shareText = shareText;
    }

    public List<Season> getSeasonList() {
        return seasonList;
    }

    public void setSeasonList(List<Season> seasonList) {
        this.seasonList = seasonList;
    }

    public String getShareTitle() {
        return shareTitle;
    }

    public void setShareTitle(String shareTitle) {
        this.shareTitle = shareTitle;
    }

    public String getShareDescription() {
        return shareDescription;
    }

    public void setShareDescription(String shareDescription) {
        this.shareDescription = shareDescription;
    }

    public String getShareThumbUrl() {
        return shareThumbUrl;
    }

    public void setShareThumbUrl(String shareThumbUrl) {
        this.shareThumbUrl = shareThumbUrl;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    //场次
    public static class Season implements Serializable{
        private String title;
        private String time;

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }

}
