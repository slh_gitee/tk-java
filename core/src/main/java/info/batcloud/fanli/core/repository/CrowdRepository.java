package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Crowd;
import info.batcloud.fanli.core.enums.CrowdStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CrowdRepository extends CrudRepository<Crowd, Long>, JpaSpecificationExecutor<Crowd> {

    List<Crowd> findByStatusNotOrderByIdDesc(CrowdStatus status);
    List<Crowd> findByStatusOrderByIdDesc(CrowdStatus status);

}
