package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.AgentType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue("DistrictAgent")
public class DistrictAgent extends Agent{

    @NotNull
    private Long cityId;
    @NotNull
    private Long districtId;

    @NotNull
    private Long provinceId;

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    @Override
    public AgentType getAgentType() {
        return AgentType.DISTRICT;
    }
}
