package info.batcloud.fanli.core;

import info.batcloud.fanli.core.constants.PddConstatns;
import info.batcloud.fanli.core.pdd.PddApp;
import info.batcloud.pdd.sdk.DefaultPddClient;
import info.batcloud.pdd.sdk.PddClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PddConfig {

    @Bean(name = PddConstatns.DDK_UNION)
    @ConfigurationProperties("pdd.ddk")
    public PddApp ddkApp() {
        return new PddApp();
    }


    @Bean(PddConstatns.CLIENT_DDK)
    public PddClient ddkClient() {
        PddApp pddApp = ddkApp();
        PddClient client = new DefaultPddClient(pddApp.getServerUrl(), pddApp.getClientId(), pddApp.getClientSecret());
        return client;
    }

}
