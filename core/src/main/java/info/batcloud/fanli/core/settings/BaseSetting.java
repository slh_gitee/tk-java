package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class BaseSetting implements Serializable {

    private String defaultUserNickname;
    private String defaultUserAvatarUrl;

    public String getDefaultUserNickname() {
        return defaultUserNickname;
    }

    public void setDefaultUserNickname(String defaultUserNickname) {
        this.defaultUserNickname = defaultUserNickname;
    }

    public String getDefaultUserAvatarUrl() {
        return defaultUserAvatarUrl;
    }

    public void setDefaultUserAvatarUrl(String defaultUserAvatarUrl) {
        this.defaultUserAvatarUrl = defaultUserAvatarUrl;
    }
}
