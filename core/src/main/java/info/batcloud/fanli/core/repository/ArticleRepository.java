package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Article;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ArticleRepository extends PagingAndSortingRepository<Article, Long>, JpaSpecificationExecutor<Article> {


}
