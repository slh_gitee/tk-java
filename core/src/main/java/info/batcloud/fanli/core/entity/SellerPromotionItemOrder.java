package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.SellerPromotionItemOrderStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class SellerPromotionItemOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Long sellerPromotionItemId;

    @NotNull
    private Long sellerId;

    @NotNull
    @Enumerated(EnumType.STRING)
    private EcomPlat ecomPlat;

    @NotNull
    private Long userId;

    @NotNull
    private Date createTime;

    private String outTradeNo; //外部订单号

    private Date updateTime;

    private String verifyRemark;

    @NotNull
    @Enumerated(EnumType.STRING)
    private SellerPromotionItemOrderStatus status;

    @Version
    private int version;

    private Float rebateFee;

    @NotNull
    private Date rebateDeadline;

    private Date verifyTime;

    private Date verifyDeadline; //审核超时时间

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Date getVerifyDeadline() {
        return verifyDeadline;
    }

    public void setVerifyDeadline(Date verifyDeadline) {
        this.verifyDeadline = verifyDeadline;
    }

    public Float getRebateFee() {
        return rebateFee;
    }

    public void setRebateFee(Float rebateFee) {
        this.rebateFee = rebateFee;
    }

    public Date getRebateDeadline() {
        return rebateDeadline;
    }

    public void setRebateDeadline(Date rebateDeadline) {
        this.rebateDeadline = rebateDeadline;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public SellerPromotionItemOrderStatus getStatus() {
        return status;
    }

    public void setStatus(SellerPromotionItemOrderStatus status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSellerPromotionItemId() {
        return sellerPromotionItemId;
    }

    public void setSellerPromotionItemId(Long sellerPromotionItemId) {
        this.sellerPromotionItemId = sellerPromotionItemId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }
}
