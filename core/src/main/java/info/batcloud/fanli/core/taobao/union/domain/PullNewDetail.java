package info.batcloud.fanli.core.taobao.union.domain;

import info.batcloud.fanli.core.excel.stereotype.XslCol;

import java.util.Date;

public class PullNewDetail {

    @XslCol(title = "注册时间")
    private Date registerTime;

    @XslCol(title = "激活时间")
    private Date activeTime;

    @XslCol(title = "首购时间")
    private Date firstBuyTime;

    @XslCol(title = "新人手机号")
    private String phone;

    @XslCol(title = "新人状态")
    private String status;

    @XslCol(title = "订单类型")
    private String orderType;

    @XslCol(title = "来源媒体ID")
    private Long unionId;

    @XslCol(title = "广告位ID")
    private Long adzoneId;

    @XslCol(title = "")
    private String orderNo;

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(Date activeTime) {
        this.activeTime = activeTime;
    }

    public Date getFirstBuyTime() {
        return firstBuyTime;
    }

    public void setFirstBuyTime(Date firstBuyTime) {
        this.firstBuyTime = firstBuyTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Long getUnionId() {
        return unionId;
    }

    public void setUnionId(Long unionId) {
        this.unionId = unionId;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
