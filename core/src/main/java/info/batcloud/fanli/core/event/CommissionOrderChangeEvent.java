package info.batcloud.fanli.core.event;

import org.springframework.context.ApplicationEvent;

public class CommissionOrderChangeEvent extends ApplicationEvent {
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the object on which the event initially occurred (never {@code null})
     */
    public CommissionOrderChangeEvent(Object source) {
        super(source);
    }
}
