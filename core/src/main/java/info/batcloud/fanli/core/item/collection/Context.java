package info.batcloud.fanli.core.item.collection;

public class Context {

    private Long shopcatId;
    private Long channelId;
    //是否清空频道商品
    private boolean clearChannelItem;
    private boolean recommend;
    private int maxPage;
    private Integer startPage = 1;
    private Integer pageSize = 20;

    public boolean isClearChannelItem() {
        return clearChannelItem;
    }

    public void setClearChannelItem(boolean clearChannelItem) {
        this.clearChannelItem = clearChannelItem;
    }

    public boolean isRecommend() {
        return recommend;
    }

    public void setRecommend(boolean recommend) {
        this.recommend = recommend;
    }

    public Integer getStartPage() {
        return startPage;
    }

    public void setStartPage(Integer startPage) {
        this.startPage = startPage;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public Long getShopcatId() {
        return shopcatId;
    }

    public void setShopcatId(Long shopcatId) {
        this.shopcatId = shopcatId;
    }

    public Long getChannelId() {
        return channelId;
    }

    public void setChannelId(Long channelId) {
        this.channelId = channelId;
    }
}
