package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum SellerPromotionItemStatus implements EnumTitle{

    WAIT_VERIFY, VERIFY_FAIL, WAIT_SALE, ONSALE, INSTOCK, DELETED;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
