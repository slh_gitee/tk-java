package info.batcloud.fanli.core.dto;

import java.util.Date;

public class TbPullNewDetailAddDTO {

    private Date registerTime;

    private Date bindTime;

    private Date buyTime;

    private String mobile;

    private int status;

    private int orderTkType;

    private Long tbTradeParentId;

    //广告位id
    private Long adzoneId;

    //媒体id
    private Long memberId;

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(Date bindTime) {
        this.bindTime = bindTime;
    }

    public Date getBuyTime() {
        return buyTime;
    }

    public void setBuyTime(Date buyTime) {
        this.buyTime = buyTime;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getOrderTkType() {
        return orderTkType;
    }

    public void setOrderTkType(int orderTkType) {
        this.orderTkType = orderTkType;
    }

    public Long getTbTradeParentId() {
        return tbTradeParentId;
    }

    public void setTbTradeParentId(Long tbTradeParentId) {
        this.tbTradeParentId = tbTradeParentId;
    }

    public Long getAdzoneId() {
        return adzoneId;
    }

    public void setAdzoneId(Long adzoneId) {
        this.adzoneId = adzoneId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }
}
