package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class WeixinSetting implements Serializable {

    private String appId;

    private String secret;

    private String gzhAppId;

    private String gzhSecret;

    private String gzhRedirectUrl;

    public String getGzhRedirectUrl() {
        return gzhRedirectUrl;
    }

    public void setGzhRedirectUrl(String gzhRedirectUrl) {
        this.gzhRedirectUrl = gzhRedirectUrl;
    }

    public String getGzhAppId() {
        return gzhAppId;
    }

    public void setGzhAppId(String gzhAppId) {
        this.gzhAppId = gzhAppId;
    }

    public String getGzhSecret() {
        return gzhSecret;
    }

    public void setGzhSecret(String gzhSecret) {
        this.gzhSecret = gzhSecret;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
