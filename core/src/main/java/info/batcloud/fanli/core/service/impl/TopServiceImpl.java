package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.constants.TaobaoClientConstants;
import info.batcloud.fanli.core.service.TopService;
import info.batcloud.fanli.core.top.TaobaoClientConfig;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class TopServiceImpl implements TopService {

    @Inject
    @Qualifier(TaobaoClientConstants.TAOBAO_CLIENT_CONFIG_TBK)
    private TaobaoClientConfig taobaoClientConfig;

    @Override
    public String generateAuthUrl(String state, String view) {
        String redirectUrl = "https://oauth.taobao.com/authorize?response_type=code&client_id=%s&redirect_uri=%s&state=%s&view=%s";
        try {
            redirectUrl = String.format(redirectUrl, new String[]{taobaoClientConfig.getClientId(), URLEncoder.encode(taobaoClientConfig.getRedirectUrl(), "utf8"), state, view});
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return redirectUrl;
    }
}
