package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum CommissionItemSort implements EnumTitle{

    NONE, PRICE_DESC, PRICE_ASC, SALES_DESC, LIST_TIME_DESC, COMMISSION_VALUE_DESC, ID_DESC, UPDATE_TIME_DESC;

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
