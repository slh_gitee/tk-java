package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.entity.Material;
import info.batcloud.fanli.core.enums.MaterialStatus;

import java.util.List;

public interface MaterialService {

    void createMaterial(MaterialCreateParam param);

    void updateMaterial(long id, MaterialUpdateParam param);

    void setStatus(long id, MaterialStatus status);

    List<Material> findAll();

    List<Material> findByStatus(MaterialStatus status);

    Material findById(long id);

    boolean checkCommissionItemInMaterial(long materialId, long commissionItemId);

    class MaterialCreateParam {
        private String name;
        private String configJson;
        private MaterialStatus status;

        public MaterialStatus getStatus() {
            return status;
        }

        public void setStatus(MaterialStatus status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getConfigJson() {
            return configJson;
        }

        public void setConfigJson(String configJson) {
            this.configJson = configJson;
        }
    }

    class MaterialUpdateParam {
        private String name;
        private String configJson;
        private MaterialStatus status;

        public MaterialStatus getStatus() {
            return status;
        }

        public void setStatus(MaterialStatus status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getConfigJson() {
            return configJson;
        }

        public void setConfigJson(String configJson) {
            this.configJson = configJson;
        }
    }

}
