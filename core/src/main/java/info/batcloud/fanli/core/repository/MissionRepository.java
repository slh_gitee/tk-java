package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Mission;
import info.batcloud.fanli.core.enums.MissionStatus;
import org.springframework.data.repository.CrudRepository;

import java.awt.print.Pageable;
import java.util.List;

public interface MissionRepository extends CrudRepository<Mission, Long> {

    List<Mission> findByStatusNotIn(MissionStatus... status);

    List<Mission> findByIdNotInAndStatus(List<Long> ids, MissionStatus status);

    List<Mission> findByStatus(MissionStatus status);
}
