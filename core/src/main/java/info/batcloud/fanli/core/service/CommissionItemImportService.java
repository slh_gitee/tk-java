package info.batcloud.fanli.core.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

public interface CommissionItemImportService {

    int importExcel(File file, ImportConfig config) throws FileNotFoundException;
    int importExcel(InputStream inputStream, ImportConfig config) throws FileNotFoundException;

    class ImportConfig {
        private Long channelId;
        private SourceFrom sourceFrom;
        private boolean xls;
        private boolean onlyCoupon;
        private boolean recommend;

        public boolean isRecommend() {
            return recommend;
        }

        public void setRecommend(boolean recommend) {
            this.recommend = recommend;
        }

        public boolean isOnlyCoupon() {
            return onlyCoupon;
        }

        public void setOnlyCoupon(boolean onlyCoupon) {
            this.onlyCoupon = onlyCoupon;
        }

        public boolean isXls() {
            return xls;
        }

        public void setXls(boolean xls) {
            this.xls = xls;
        }

        public SourceFrom getSourceFrom() {
            return sourceFrom;
        }

        public void setSourceFrom(SourceFrom sourceFrom) {
            this.sourceFrom = sourceFrom;
        }

        public Long getChannelId() {
            return channelId;
        }

        public void setChannelId(Long channelId) {
            this.channelId = channelId;
        }
    }

    enum SourceFrom {
        TBK_RECOMMEND_COUPON,
        JHS_99
    }

}
