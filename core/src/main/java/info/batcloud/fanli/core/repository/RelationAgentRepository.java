package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.RelationAgent;
import info.batcloud.fanli.core.enums.AgentStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RelationAgentRepository extends PagingAndSortingRepository<RelationAgent, Long>, JpaSpecificationExecutor<RelationAgent> {

    RelationAgent findByUserIdAndStatusIsNot(long userId, AgentStatus status);

}
