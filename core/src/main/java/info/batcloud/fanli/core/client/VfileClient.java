package info.batcloud.fanli.core.client;

import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.entity.Vfile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

public interface VfileClient {

    @GetMapping("/children/parentId/{parentId}")
    List<Vfile> childrenByParentId(@PathVariable("parentId") long parentId);

    @GetMapping("/children/code/{code}")
    List<Vfile> childrenByCode(@PathVariable("code") String code);

    @PostMapping("/dir")
    BusinessResponse<Vfile> createDir(@RequestParam("name") String name, @RequestParam("code") String code, @RequestParam("parentId") long parentId);
}
