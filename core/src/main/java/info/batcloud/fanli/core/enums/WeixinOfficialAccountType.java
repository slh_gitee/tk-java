package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum WeixinOfficialAccountType implements EnumTitle {

    SUB,SERVICE;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }
}
