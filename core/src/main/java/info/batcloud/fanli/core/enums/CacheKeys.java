package info.batcloud.fanli.core.enums;

public enum CacheKeys {

    SMS_PHONE,
    SMS_PHONE_CODE,
    COMMISSION_ITEM,
    TOP_USER_STATE;

    public String name(String value) {
        return this.name() + "_" + value;
    }
}

