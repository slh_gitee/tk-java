package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.CityAgentApply;
import info.batcloud.fanli.core.enums.AgentApplyStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CityAgentApplyRepository extends PagingAndSortingRepository<CityAgentApply, Long>, JpaSpecificationExecutor<CityAgentApply> {

    int countByUserIdAndStatus(long userId, AgentApplyStatus status);

}
