package info.batcloud.fanli.core.service.impl;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.AgentDTO;
import info.batcloud.fanli.core.dto.RelationAgentDTO;
import info.batcloud.fanli.core.constants.CacheNameConstants;
import info.batcloud.fanli.core.entity.RelationAgent;
import info.batcloud.fanli.core.entity.User;
import info.batcloud.fanli.core.enums.AgentStatus;
import info.batcloud.fanli.core.helper.PagingHelper;
import info.batcloud.fanli.core.repository.RelationAgentRepository;
import info.batcloud.fanli.core.repository.UserRepository;
import info.batcloud.fanli.core.service.RelationAgentService;
import info.batcloud.fanli.core.service.UserService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.Date;
import java.util.List;

import static info.batcloud.fanli.core.enums.AgentStatus.DELETED;

@Service
@CacheConfig(cacheNames = CacheNameConstants.AGENT)
public class RelationAgentServiceImpl implements RelationAgentService {

    @Inject
    private RelationAgentRepository relationAgentRepository;

    @Inject
    private UserRepository userRepository;

    @Inject
    private UserService userService;

    @Override
    public Paging<RelationAgentDTO> search(SearchParam param) {
        Specification<RelationAgent> specification = (root, query, cb) -> {
            Predicate predicate = cb.conjunction();
            List<Expression<Boolean>> expressions = predicate.getExpressions();
            if (param.getPerpetual() != null) {
                expressions.add(cb.equal(root.get("perpetual"), param.getPerpetual()));
            }

            if (StringUtils.isNotBlank(param.getPhone())) {
                expressions.add(cb.like(root.get("user").get("phone"), param.getPhone()));
            }
            if (param.getStartUpdateTime() != null) {
                expressions.add(cb.greaterThanOrEqualTo(root.get("updateTime"), param.getStartUpdateTime()));
            }
            if (param.getEndUpdateTime() != null) {
                expressions.add(cb.greaterThanOrEqualTo(root.get("updateTime"), param.getEndUpdateTime()));
            }
            if (param.getStatus() != null) {
                expressions.add(cb.equal(root.get("status"), param.getStatus()));
            } else {
                expressions.add(cb.notEqual(root.get("status"), DELETED));
            }
            return predicate;
        };
        Sort sort = new Sort(Sort.Direction.DESC, "updateTime");
        Pageable pageable = new PageRequest(param.getPage() - 1,
                param.getPageSize(), sort);
        Page<RelationAgent> page = relationAgentRepository.findAll(specification, pageable);
        return PagingHelper.of(page, item -> toAgent(item), param.getPage(), param.getPageSize());
    }

    @Override
    @Transactional
    @CacheEvict(key = "'IS_' + #param.getUserId() + '_AGENT_RELATION'")
    public RelationAgentDTO addAgent(AgentAddParam param) {
        User user = userRepository.findOne(param.getUserId());
        //如果当前user不是顶级用户，那么需要修改其为顶级用户
        if (user.getSuperUser() != null) {
            //这里需要修改user的层级关系,并且同步修改其所有下线的层级关系
            userService.changeSuperUser(param.getUserId(), null);
        }
        Date now = new Date();
        Date startTime;
        RelationAgent agent = relationAgentRepository.findByUserIdAndStatusIsNot(param.getUserId(), DELETED);
        if (agent == null) {
            agent = new RelationAgent();
            agent.setCreateTime(now);
            startTime = now;
        } else {
            if (agent.getStatus() != AgentStatus.VALID) {
                agent.setCreateTime(now);
                startTime = now;
            } else {
                startTime = agent.getExpireTime();
            }

        }
        agent.setUpdateTime(new Date());
        if (param.getYear() == -1) {
            //如果是-1那么说明是永久代理
            agent.setExpireTime(DateUtils.addYears(startTime, 100));
            agent.setPerpetual(true);
        } else {
            agent.setExpireTime(DateUtils.addYears(startTime, param.getYear()));
        }
        agent.setStatus(AgentStatus.VALID);
        agent.setUser(user);
        relationAgentRepository.save(agent);
        return toAgent(agent);
    }

    private RelationAgentDTO toAgent(RelationAgent entity) {
        RelationAgentDTO agentBO = new RelationAgentDTO();
        BeanUtils.copyProperties(entity, agentBO);
        AgentDTO.User user = new AgentDTO.User();
        BeanUtils.copyProperties(entity.getUser(), user);
        agentBO.setUser(user);
        return agentBO;
    }
}
