package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

import java.io.Serializable;

public enum UserLevel implements EnumTitle, Serializable {

    CHIEF(3, null), CARRIER(2, UserLevel.CHIEF), PLUS(1, CARRIER), ORDINARY(0, UserLevel.PLUS);

    public int level;
    public UserLevel next;

    UserLevel(int level, UserLevel next) {
        this.level = level;
        this.next = next;
    }

    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "."
                + this.name(), null, "", null);
    }

}
