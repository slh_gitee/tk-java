package info.batcloud.fanli.core.domain.utam.req;

import java.util.List;

public class ItemSelectionDeleteItemRequest {

    private Long selectionId;

    private List<Long> itemIds;

    public List<Long> getItemIds() {
        return itemIds;
    }

    public void setItemIds(List<Long> itemIds) {
        this.itemIds = itemIds;
    }

    public Long getSelectionId() {
        return selectionId;
    }

    public void setSelectionId(Long selectionId) {
        this.selectionId = selectionId;
    }
}
