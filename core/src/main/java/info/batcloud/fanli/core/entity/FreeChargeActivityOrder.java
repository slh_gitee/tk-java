package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.FreeChargeActivityOrderStatus;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FreeChargeActivityOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @Fetch(FetchMode.JOIN)
    private User user;

    private Date createTime;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @Fetch(FetchMode.JOIN)
    private CommissionItem item;

    private Date updateTime;

    @OneToOne(cascade = CascadeType.REFRESH)
    @Fetch(FetchMode.JOIN)
    private CommissionOrder commissionOrder;

    @Enumerated(EnumType.STRING)
    private FreeChargeActivityOrderStatus status;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @Fetch(FetchMode.JOIN)
    private FreeChargeActivity freeChargeActivity;

    @Version
    private int version;

    //免单金额
    private Float freeFee;

    public Float getFreeFee() {
        return freeFee;
    }

    public void setFreeFee(Float freeFee) {
        this.freeFee = freeFee;
    }

    public FreeChargeActivity getFreeChargeActivity() {
        return freeChargeActivity;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void setFreeChargeActivity(FreeChargeActivity freeChargeActivity) {
        this.freeChargeActivity = freeChargeActivity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public CommissionItem getItem() {
        return item;
    }

    public void setItem(CommissionItem item) {
        this.item = item;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public CommissionOrder getCommissionOrder() {
        return commissionOrder;
    }

    public void setCommissionOrder(CommissionOrder commissionOrder) {
        this.commissionOrder = commissionOrder;
    }

    public FreeChargeActivityOrderStatus getStatus() {
        return status;
    }

    public void setStatus(FreeChargeActivityOrderStatus status) {
        this.status = status;
    }
}
