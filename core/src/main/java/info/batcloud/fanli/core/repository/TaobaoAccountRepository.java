package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TaobaoAccount;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface TaobaoAccountRepository extends PagingAndSortingRepository<TaobaoAccount, Long> {

    TaobaoAccount findByUserId(long userId);

}
