package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.domain.utam.AuctionCode;
import info.batcloud.fanli.core.domain.utam.req.TbkOrderFetchRequest;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

public interface TaobaoUnionService {

    AuctionCode createAuctionCode(String pid, Long auctionId);

    boolean fetchOrder(TbkOrderFetchRequest param);
    ImportOrderResult importOrder(InputStream inputStream);

    boolean importRecommendCoupon(InputStream is);

    class ImportOrderResult {
        private int totalNum;

        public int getTotalNum() {
            return totalNum;
        }

        public void setTotalNum(int totalNum) {
            this.totalNum = totalNum;
        }
    }

    class FetchPullNewResultData {
        private boolean hasNext;

        private List<PullNewDetail> pagelist;

        public boolean isHasNext() {
            return hasNext;
        }

        public void setHasNext(boolean hasNext) {
            this.hasNext = hasNext;
        }

        public List<PullNewDetail> getPagelist() {
            return pagelist;
        }

        public void setPagelist(List<PullNewDetail> pagelist) {
            this.pagelist = pagelist;
        }
    }

    class Result<T> {
        private String invalidKey;
        private boolean ok;

        private Info info;

        private T data;

        public String getInvalidKey() {
            return invalidKey;
        }

        public void setInvalidKey(String invalidKey) {
            this.invalidKey = invalidKey;
        }

        public boolean isOk() {
            return ok;
        }

        public void setOk(boolean ok) {
            this.ok = ok;
        }

        public T getData() {
            return data;
        }

        public void setData(T data) {
            this.data = data;
        }

        public Info getInfo() {
            return info;
        }

        public void setInfo(Info info) {
            this.info = info;
        }
    }

    class PullNewDetail {
        private Long adzoneId;
        private String adzoneName;
        private Date bindTime;
        private Date buyTime;
        private Long memberId;
        private String memberNick;
        private String mobile;
        private int orderTkType;
        private Date registerTime;
        private int status;
        private Long tbTradeParentId;
        private String unionId;

        public Long getAdzoneId() {
            return adzoneId;
        }

        public void setAdzoneId(Long adzoneId) {
            this.adzoneId = adzoneId;
        }

        public String getAdzoneName() {
            return adzoneName;
        }

        public void setAdzoneName(String adzoneName) {
            this.adzoneName = adzoneName;
        }

        public Date getBindTime() {
            return bindTime;
        }

        public void setBindTime(Date bindTime) {
            this.bindTime = bindTime;
        }

        public Date getBuyTime() {
            return buyTime;
        }

        public void setBuyTime(Date buyTime) {
            this.buyTime = buyTime;
        }

        public Long getMemberId() {
            return memberId;
        }

        public void setMemberId(Long memberId) {
            this.memberId = memberId;
        }

        public String getMemberNick() {
            return memberNick;
        }

        public void setMemberNick(String memberNick) {
            this.memberNick = memberNick;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public int getOrderTkType() {
            return orderTkType;
        }

        public void setOrderTkType(int orderTkType) {
            this.orderTkType = orderTkType;
        }

        public Date getRegisterTime() {
            return registerTime;
        }

        public void setRegisterTime(Date registerTime) {
            this.registerTime = registerTime;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Long getTbTradeParentId() {
            return tbTradeParentId;
        }

        public void setTbTradeParentId(Long tbTradeParentId) {
            this.tbTradeParentId = tbTradeParentId;
        }

        public String getUnionId() {
            return unionId;
        }

        public void setUnionId(String unionId) {
            this.unionId = unionId;
        }
    }

    class AdzoneCreateParam {

        private String newAdzoneName;

        public String getNewAdzoneName() {
            return newAdzoneName;
        }

        public void setNewAdzoneName(String newAdzoneName) {
            this.newAdzoneName = newAdzoneName;
        }
    }

    class AuctionCodeCreateResult {
        private String invalidKey;
        private boolean ok;

        private AuctionCode data;
        private Info info;

        public String getInvalidKey() {
            return invalidKey;
        }

        public void setInvalidKey(String invalidKey) {
            this.invalidKey = invalidKey;
        }

        public boolean isOk() {
            return ok;
        }

        public void setOk(boolean ok) {
            this.ok = ok;
        }

        public AuctionCode getData() {
            return data;
        }

        public void setData(AuctionCode data) {
            this.data = data;
        }

        public Info getInfo() {
            return info;
        }

        public void setInfo(Info info) {
            this.info = info;
        }

    }

    class AdzoneCreateResult {
        private String invalidKey;
        private boolean ok;

        private Data data;
        private Info info;

        private String pid;

        public String getPid() {
            return pid;
        }

        public void setPid(String pid) {
            this.pid = pid;
        }

        public String getInvalidKey() {
            return invalidKey;
        }

        public void setInvalidKey(String invalidKey) {
            this.invalidKey = invalidKey;
        }

        public boolean isOk() {
            return ok;
        }

        public void setOk(boolean ok) {
            this.ok = ok;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public Info getInfo() {
            return info;
        }

        public void setInfo(Info info) {
            this.info = info;
        }

        public static class Data {
            private long adzoneId;
            private long siteId;

            public long getAdzoneId() {
                return adzoneId;
            }

            public void setAdzoneId(long adzoneId) {
                this.adzoneId = adzoneId;
            }

            public long getSiteId() {
                return siteId;
            }

            public void setSiteId(long siteId) {
                this.siteId = siteId;
            }
        }
    }

    class Info {
        private String message;
        private boolean ok;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public boolean isOk() {
            return ok;
        }

        public void setOk(boolean ok) {
            this.ok = ok;
        }
    }
}
