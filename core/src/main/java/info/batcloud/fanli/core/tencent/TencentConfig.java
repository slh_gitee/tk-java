package info.batcloud.fanli.core.tencent;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TencentConfig {

    @Bean
    @ConfigurationProperties("tencent.xg.android")
    public XgConfig androidXgConfig() {
        return new XgConfig();
    }

    @Bean
    @ConfigurationProperties("tencent.xg.ios")
    public XgConfig iosXgConfig() {
        return new XgConfig();
    }
}
