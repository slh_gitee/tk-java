package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.Date;

public class CommissionOrderAddDTO {

    private Date createTime;

    private Date platSettledTime;

    private String itemTitle;

    private String itemId;

    private String shopTitle;

    private int itemNum;

    private Float price;

    private CommissionOrderStatus status;

    private EcomPlat ecomPlat;

    /**
     * 预估佣金金额
     */
    private Float estimateCommissionFee; //预估佣金

    /**
     * 预估佣金比例, 百分比
     */
    private Float estimateCommissionRate;

    /**
     * 百分比
     * */
    private Float commissionRate;

    private Float commissionFee;

    private Float totalFee;

    private Float payFee;

    private String orderNo;

    private Long userId;

    public Date getPlatSettledTime() {
        return platSettledTime;
    }

    public void setPlatSettledTime(Date platSettledTime) {
        this.platSettledTime = platSettledTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public CommissionOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionOrderStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Float getEstimateCommissionFee() {
        return estimateCommissionFee;
    }

    public void setEstimateCommissionFee(Float estimateCommissionFee) {
        this.estimateCommissionFee = estimateCommissionFee;
    }

    public Float getEstimateCommissionRate() {
        return estimateCommissionRate;
    }

    public void setEstimateCommissionRate(Float estimateCommissionRate) {
        this.estimateCommissionRate = estimateCommissionRate;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(Float commissionFee) {
        this.commissionFee = commissionFee;
    }

    public Float getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Float totalFee) {
        this.totalFee = totalFee;
    }

    public Float getPayFee() {
        return payFee;
    }

    public void setPayFee(Float payFee) {
        this.payFee = payFee;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
