package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class IndexBannerSetting implements Serializable {

    //绑定的广告位id
    private Long advSpaceId;

    public Long getAdvSpaceId() {
        return advSpaceId;
    }

    public void setAdvSpaceId(Long advSpaceId) {
        this.advSpaceId = advSpaceId;
    }

}
