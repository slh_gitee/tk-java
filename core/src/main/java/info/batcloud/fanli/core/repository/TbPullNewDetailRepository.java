package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.TbPullNewDetail;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface TbPullNewDetailRepository extends CrudRepository<TbPullNewDetail, Long>, JpaSpecificationExecutor<TbPullNewDetail> {

    TbPullNewDetail findByMobileAndRegisterTime(String mobile, Date registerTime);

}
