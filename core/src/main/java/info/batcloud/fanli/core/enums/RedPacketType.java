package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum RedPacketType implements EnumTitle{

    NEWER(WalletFlowDetailType.NEWER_MONEY_RED_PACKET, WalletFlowDetailType.NEWER_INTEGRAL_RED_PACKET);

    public WalletFlowDetailType moneyWalletFlowDetailType;
    public WalletFlowDetailType integralWalletFlowDetailType;

    RedPacketType(WalletFlowDetailType moneyType, WalletFlowDetailType integralType) {
        this.moneyWalletFlowDetailType = moneyType;
        this.integralWalletFlowDetailType = integralType;
    }

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
