package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.settings.annotation.Single;

import java.io.Serializable;

@Single
public class JdUnionSetting implements Serializable {

    private long unionId;

    private String key;

    private Long positionId;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public long getUnionId() {
        return unionId;
    }

    public void setUnionId(long unionId) {
        this.unionId = unionId;
    }
}
