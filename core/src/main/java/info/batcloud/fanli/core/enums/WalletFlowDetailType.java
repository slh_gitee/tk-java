package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum WalletFlowDetailType {

    USER_SIGN,
    USER_INVITATION,
    USER_WITHDRAW,
    COMMISSION_SETTLEMENT,
    UPGRADE_PLUS_USER_LEVEL,
    USER_COMMISSION_INTEGRAL,
    PLUS_UPGRADE_ORDER_PAY,
    PROMOTION_COMMISSION,
    AGENT_PROMOTION_COMMISSION,
    SELLER_PROMOTION_ITEM_DEPOSIT,
    SELLER_PROMOTION_ITEM_REBATE,
    USER_REGISTER_REWARD,
    USER_INVITATION_REWARD,
    NEWER_INTEGRAL_RED_PACKET,
    NEWER_MONEY_RED_PACKET,
    COMMISSION_SETTLE_REWARD,
    COMMISSION_SETTLE_EXCHANGE_MONEY,
    USER_UPGRADE_REWARD,
    USER_WITHDRAW_VERIFY_FAIL_RETURN,
    FREE_CHARGE_ACTIVITY_REBATE;

    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "." + this.name(), null, null);
    }

}
