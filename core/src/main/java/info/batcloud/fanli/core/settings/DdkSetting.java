package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class DdkSetting implements Serializable {

    //默认的pid
    private String pid;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }
}
