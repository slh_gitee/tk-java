package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.enums.ItemSelectionStatus;
import info.batcloud.fanli.core.enums.ItemSelectionType;
import info.batcloud.fanli.core.item.collection.CollectResult;

import java.util.Date;
import java.util.List;

public interface ItemSelectionService {

    ItemSelectionDto addItemSelection(ItemSelectionAddParam param);

    void updateItemSelection(long id, ItemSelectionUpdateParam param);

    Paging<ItemSelectionDto> search(SearchParam searchParam);

    void setStatusById(long id, ItemSelectionStatus status);

    /**
     * 选品库收集商品
     * */
    CollectResult collectByItemSelectionId(long itemSelectionId);

    class SearchParam extends PagingParam {
        private Long channelId;
        private Long shopcatId;
        private Sort sort;
        private String name;
        private ItemSelectionType type;
        private Boolean recommend;
        private ItemSelectionStatus status;

        public Sort getSort() {
            return sort;
        }

        public void setSort(Sort sort) {
            this.sort = sort;
        }

        public enum Sort {
            FETCH_TIME_DESC, ID_DESC
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ItemSelectionType getType() {
            return type;
        }

        public void setType(ItemSelectionType type) {
            this.type = type;
        }

        public Boolean getRecommend() {
            return recommend;
        }

        public void setRecommend(Boolean recommend) {
            this.recommend = recommend;
        }

        public ItemSelectionStatus getStatus() {
            return status;
        }

        public void setStatus(ItemSelectionStatus status) {
            this.status = status;
        }

        public Long getChannelId() {
            return channelId;
        }

        public void setChannelId(Long channelId) {
            this.channelId = channelId;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }
    }

    class ItemSelectionDto {
        private Long id;
        private String name;
        private long channelId;
        private boolean clearChannelItem;
        private String channelName;
        private Long sourceSelectionId;
        private EcomPlat ecomPlat;
        private Long shopcatId;
        private String shopcatTitle;
        private boolean recommend;

        private boolean autoFetch;

        private String fetchUrl;

        private int maxPage;

        private Date createTime;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
        private Date lastFetchTime; //最后同步时间
        private int lastFetchNum;
        private String errorMsg;
        private ItemSelectionStatus status;

        private String config;

        private ItemSelectionType type;

        public boolean isClearChannelItem() {
            return clearChannelItem;
        }

        public void setClearChannelItem(boolean clearChannelItem) {
            this.clearChannelItem = clearChannelItem;
        }

        public boolean isRecommend() {
            return recommend;
        }

        public void setRecommend(boolean recommend) {
            this.recommend = recommend;
        }

        public ItemSelectionType getType() {
            return type;
        }

        public void setType(ItemSelectionType type) {
            this.type = type;
        }

        public String getTypeTitle() {
            return type == null ? null : type.getTitle();
        }

        public String getConfig() {
            return config;
        }

        public void setConfig(String config) {
            this.config = config;
        }

        public boolean isAutoFetch() {
            return autoFetch;
        }

        public void setAutoFetch(boolean autoFetch) {
            this.autoFetch = autoFetch;
        }

        public String getFetchUrl() {
            return fetchUrl;
        }

        public void setFetchUrl(String fetchUrl) {
            this.fetchUrl = fetchUrl;
        }

        public int getMaxPage() {
            return maxPage;
        }

        public void setMaxPage(int maxPage) {
            this.maxPage = maxPage;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public String getShopcatTitle() {
            return shopcatTitle;
        }

        public void setShopcatTitle(String shopcatTitle) {
            this.shopcatTitle = shopcatTitle;
        }

        public long getChannelId() {
            return channelId;
        }

        public void setChannelId(long channelId) {
            this.channelId = channelId;
        }

        public String getChannelName() {
            return channelName;
        }

        public void setChannelName(String channelName) {
            this.channelName = channelName;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getSourceSelectionId() {
            return sourceSelectionId;
        }

        public void setSourceSelectionId(Long sourceSelectionId) {
            this.sourceSelectionId = sourceSelectionId;
        }

        public EcomPlat getEcomPlat() {
            return ecomPlat;
        }

        public void setEcomPlat(EcomPlat ecomPlat) {
            this.ecomPlat = ecomPlat;
        }

        public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public Date getLastFetchTime() {
            return lastFetchTime;
        }

        public void setLastFetchTime(Date lastFetchTime) {
            this.lastFetchTime = lastFetchTime;
        }

        public int getLastFetchNum() {
            return lastFetchNum;
        }

        public void setLastFetchNum(int lastFetchNum) {
            this.lastFetchNum = lastFetchNum;
        }

        public String getErrorMsg() {
            return errorMsg;
        }

        public void setErrorMsg(String errorMsg) {
            this.errorMsg = errorMsg;
        }

        public ItemSelectionStatus getStatus() {
            return status;
        }

        public void setStatus(ItemSelectionStatus status) {
            this.status = status;
        }
    }

    class ItemSelectionAddParam {
        private String name;
        private ItemSelectionStatus status;
        private Long channelId;
        private boolean clearChannelItem;
        private Long shopcatId;
        private boolean recommend;
        private boolean autoFetch;
        private int maxPage;
        private ItemSelectionType type;

        private String config;

        public boolean isClearChannelItem() {
            return clearChannelItem;
        }

        public void setClearChannelItem(boolean clearChannelItem) {
            this.clearChannelItem = clearChannelItem;
        }

        public boolean isRecommend() {
            return recommend;
        }

        public void setRecommend(boolean recommend) {
            this.recommend = recommend;
        }

        public ItemSelectionType getType() {
            return type;
        }

        public void setType(ItemSelectionType type) {
            this.type = type;
        }

        public boolean isAutoFetch() {
            return autoFetch;
        }

        public void setAutoFetch(boolean autoFetch) {
            this.autoFetch = autoFetch;
        }

        public int getMaxPage() {
            return maxPage;
        }

        public void setMaxPage(int maxPage) {
            this.maxPage = maxPage;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public Long getChannelId() {
            return channelId;
        }

        public void setChannelId(Long channelId) {
            this.channelId = channelId;
        }

        public ItemSelectionStatus getStatus() {
            return status;
        }

        public void setStatus(ItemSelectionStatus status) {
            this.status = status;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getConfig() {
            return config;
        }

        public void setConfig(String config) {
            this.config = config;
        }
    }

    class ItemSelectionUpdateParam {
        private String name;
        private ItemSelectionStatus status;
        private Long channelId;
        private boolean clearChannelItem;
        private Long shopcatId;
        private boolean recommend;
        private boolean autoFetch;
        private int maxPage;

        private ItemSelectionType type;

        private String config;

        public boolean isClearChannelItem() {
            return clearChannelItem;
        }

        public void setClearChannelItem(boolean clearChannelItem) {
            this.clearChannelItem = clearChannelItem;
        }

        public boolean isRecommend() {
            return recommend;
        }

        public void setRecommend(boolean recommend) {
            this.recommend = recommend;
        }

        public ItemSelectionType getType() {
            return type;
        }

        public void setType(ItemSelectionType type) {
            this.type = type;
        }

        public String getConfig() {
            return config;
        }

        public void setConfig(String config) {
            this.config = config;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ItemSelectionStatus getStatus() {
            return status;
        }

        public void setStatus(ItemSelectionStatus status) {
            this.status = status;
        }

        public Long getChannelId() {
            return channelId;
        }

        public void setChannelId(Long channelId) {
            this.channelId = channelId;
        }

        public Long getShopcatId() {
            return shopcatId;
        }

        public void setShopcatId(Long shopcatId) {
            this.shopcatId = shopcatId;
        }

        public boolean isAutoFetch() {
            return autoFetch;
        }

        public void setAutoFetch(boolean autoFetch) {
            this.autoFetch = autoFetch;
        }

        public int getMaxPage() {
            return maxPage;
        }

        public void setMaxPage(int maxPage) {
            this.maxPage = maxPage;
        }
    }

}
