package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.UserWithdrawDTO;
import info.batcloud.fanli.core.domain.Result;
import info.batcloud.fanli.core.enums.UserWithdrawStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public interface UserWithdrawService {

    void withdraw(long userId, UserWithDrawParams params);

    Paging<UserWithdrawDTO> search(SearchParam param);

    UserWithdrawDTO findById(long id);

    Result withdrawFundTransferById(long id);

    String findUserLastAlipayAccount(long userId);

    Result withdrawManualFundTransfer(ManualFundTransferParam param);

    void verify(VerifyParam param);

    File exportXsl(ExportParam param) throws IOException;

    class ExportParam extends SearchParam {
        private int maxCount;

        public int getMaxCount() {
            return maxCount;
        }

        public void setMaxCount(int maxCount) {
            this.maxCount = maxCount;
        }
    }

    class ManualFundTransferParam {
        @NotNull
        private Long id;
        @NotNull
        private String alipayOrderId;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getAlipayOrderId() {
            return alipayOrderId;
        }

        public void setAlipayOrderId(String alipayOrderId) {
            this.alipayOrderId = alipayOrderId;
        }
    }

    class VerifyParam {
        private Long id;
        private boolean success;
        private String remark;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }

    class SearchParam extends PagingParam {
        private String userPhone;
        private UserWithdrawStatus status;

        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date createStartTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date createEndTime;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date createStartDate;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date createEndDate;

        public Date getCreateStartDate() {
            return createStartDate;
        }

        public void setCreateStartDate(Date createStartDate) {
            this.createStartDate = createStartDate;
        }

        public Date getCreateEndDate() {
            return createEndDate;
        }

        public void setCreateEndDate(Date createEndDate) {
            this.createEndDate = createEndDate;
        }

        public String getUserPhone() {
            return userPhone;
        }

        public void setUserPhone(String userPhone) {
            this.userPhone = userPhone;
        }

        public UserWithdrawStatus getStatus() {
            return status;
        }

        public void setStatus(UserWithdrawStatus status) {
            this.status = status;
        }

        public Date getCreateStartTime() {
            return createStartTime;
        }

        public void setCreateStartTime(Date createStartTime) {
            this.createStartTime = createStartTime;
        }

        public Date getCreateEndTime() {
            return createEndTime;
        }

        public void setCreateEndTime(Date createEndTime) {
            this.createEndTime = createEndTime;
        }
    }

    class UserWithDrawParams {
        @NotNull
        private Float money;
        @NotNull
        private String verifyCode;
        @NotNull
        private String payeeAccount;

        private String payeeName;

        public Float getMoney() {
            return money;
        }

        public void setMoney(Float money) {
            this.money = money;
        }

        public String getVerifyCode() {
            return verifyCode;
        }

        public void setVerifyCode(String verifyCode) {
            this.verifyCode = verifyCode;
        }

        public String getPayeeAccount() {
            return payeeAccount;
        }

        public void setPayeeAccount(String payeeAccount) {
            this.payeeAccount = payeeAccount;
        }

        public String getPayeeName() {
            return payeeName;
        }

        public void setPayeeName(String payeeName) {
            this.payeeName = payeeName;
        }
    }

}
