package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.MissionType;
import info.batcloud.fanli.core.enums.UserMissionStatus;

public class UserMissionDTO {

    private Long id;

    private Long userId;

    private long missionId;

    private String title;

    private int completeTimes;

    private MissionType missionType;

    private UserMissionStatus status;

    public MissionType getMissionType() {
        return missionType;
    }

    public void setMissionType(MissionType missionType) {
        this.missionType = missionType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getMissionId() {
        return missionId;
    }

    public void setMissionId(long missionId) {
        this.missionId = missionId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCompleteTimes() {
        return completeTimes;
    }

    public void setCompleteTimes(int completeTimes) {
        this.completeTimes = completeTimes;
    }

    public UserMissionStatus getStatus() {
        return status;
    }

    public void setStatus(UserMissionStatus status) {
        this.status = status;
    }
}
