package info.batcloud.fanli.core.service;

/**
 * @author lvling
 */
public interface FlashSaleRemindService {

    void scheduleRemind();

    void joinRemind(long flashSaleItemId, long userId);

    void exitRemind(long flashSaleItemId, long userId);

    boolean checkRemind(long flashSaleItemId, long userId);

    void doFlashSaleItemRemind(long flashSaleItemId);
    void doFlashSaleItemUserRemind(long flashSaleItemId, long userId);

}
