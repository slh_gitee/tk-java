package info.batcloud.fanli.core.settings;

import info.batcloud.fanli.core.helper.OSSImageHelper;
import info.batcloud.fanli.core.operation.center.domain.PageJump;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class IndexFloorSetting implements Serializable {

    private List<Floor> floorList = new ArrayList<>();

    public List<Floor> getFloorList() {
        return floorList;
    }

    public void setFloorList(List<Floor> floorList) {
        this.floorList = floorList;
    }

    public static class Floor implements Serializable {
        private String title;
        private boolean showTitle;
        private String titlePic;

        private int titlePicWidth;
        private int titlePicHeight;

        private List<Row> rowList = new ArrayList<>();

        public String getTitlePic() {
            return titlePic;
        }

        public void setTitlePic(String titlePic) {
            this.titlePic = titlePic;
        }

        public int getTitlePicWidth() {
            return titlePicWidth;
        }

        public void setTitlePicWidth(int titlePicWidth) {
            this.titlePicWidth = titlePicWidth;
        }

        public int getTitlePicHeight() {
            return titlePicHeight;
        }

        public void setTitlePicHeight(int titlePicHeight) {
            this.titlePicHeight = titlePicHeight;
        }

        public String getTitlePicUrl() {
            return OSSImageHelper.toUrl(this.getTitlePic());
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isShowTitle() {
            return showTitle;
        }

        public void setShowTitle(boolean showTitle) {
            this.showTitle = showTitle;
        }

        public List<Row> getRowList() {
            return rowList;
        }

        public void setRowList(List<Row> rowList) {
            this.rowList = rowList;
        }
    }

    public static class Row implements Serializable {

        private Integer height;

        private List<Showcase> showcaseList = new ArrayList<>();

        public List<Showcase> getShowcaseList() {
            return showcaseList;
        }

        public Integer getHeight() {
            return height;
        }


        public void setHeight(Integer height) {
            this.height = height;
        }
        public void setShowcaseList(List<Showcase> showcaseList) {
            this.showcaseList = showcaseList;
        }
    }

    public enum PicLayout {
        COVER, LEFT, RIGHT, CENTER, MIX
    }

    public static class Showcase extends PageJump {

        private String title;
        private boolean showTitle;
        private boolean alignCenter;
        private String pic;
        private int picWidth;
        private int picHeight;
        private String slogan;
        private Float width;
        //显示商品的数量
        private int itemShowNum;

        private int itemFetchNum;

        private boolean showPrice;

        private boolean showSalePrice;

        private String itemSearchParams;

        private boolean showItem;

        private PicLayout picLayout;

        private ShowType showType;

        private int rowItemNum;

        private int rowNum;

        public enum ShowType {
            LIST, SCROLL, SLIDER
        }

        public int getRowItemNum() {
            return rowItemNum;
        }

        public void setRowItemNum(int rowItemNum) {
            this.rowItemNum = rowItemNum;
        }

        public int getRowNum() {
            return rowNum;
        }

        public void setRowNum(int rowNum) {
            this.rowNum = rowNum;
        }

        public ShowType getShowType() {
            return showType;
        }

        public void setShowType(ShowType showType) {
            this.showType = showType;
        }

        public PicLayout getPicLayout() {
            return picLayout;
        }

        public void setPicLayout(PicLayout picLayout) {
            this.picLayout = picLayout;
        }

        public boolean isShowItem() {
            return showItem;
        }

        public void setShowItem(boolean showItem) {
            this.showItem = showItem;
        }

        public boolean isShowTitle() {
            return showTitle;
        }

        public void setShowTitle(boolean showTitle) {
            this.showTitle = showTitle;
        }

        public boolean isAlignCenter() {
            return alignCenter;
        }

        public void setAlignCenter(boolean alignCenter) {
            this.alignCenter = alignCenter;
        }

        public int getPicWidth() {
            return picWidth;
        }

        public void setPicWidth(int picWidth) {
            this.picWidth = picWidth;
        }

        public int getPicHeight() {
            return picHeight;
        }

        public void setPicHeight(int picHeight) {
            this.picHeight = picHeight;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getPicUrl() {
            return OSSImageHelper.toUrl(this.getPic());
        }

        public boolean isShowPrice() {
            return showPrice;
        }

        public void setShowPrice(boolean showPrice) {
            this.showPrice = showPrice;
        }

        public boolean isShowSalePrice() {
            return showSalePrice;
        }

        public void setShowSalePrice(boolean showSalePrice) {
            this.showSalePrice = showSalePrice;
        }

        public String getItemSearchParams() {
            return itemSearchParams;
        }

        public void setItemSearchParams(String itemSearchParams) {
            this.itemSearchParams = itemSearchParams;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSlogan() {
            return slogan;
        }

        public void setSlogan(String slogan) {
            this.slogan = slogan;
        }

        public Float getWidth() {
            return width;
        }

        public void setWidth(Float width) {
            this.width = width;
        }

        public int getItemShowNum() {
            return itemShowNum;
        }

        public void setItemShowNum(int itemShowNum) {
            this.itemShowNum = itemShowNum;
        }

        public int getItemFetchNum() {
            return itemFetchNum;
        }

        public void setItemFetchNum(int itemFetchNum) {
            this.itemFetchNum = itemFetchNum;
        }
    }
}
