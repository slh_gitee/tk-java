package info.batcloud.fanli.core.domain;

import java.util.ArrayList;
import java.util.List;

public class UserSignInfo {

    private long userId;

    //连续签到次数
    private int continuousSignTimes;

    //当前签到周期连续签到次数
    private int currentContinuousSignTimes;

    //当前签到积分
    private int currentSignIntegral;

    //下次签到积分
    private int nextSignIntegral;

    //当前是否签到
    private boolean signed;

    private int signCycleTimes;

    public int getSignCycleTimes() {
        return signCycleTimes;
    }

    public void setSignCycleTimes(int signCycleTimes) {
        this.signCycleTimes = signCycleTimes;
    }

    public int getCurrentContinuousSignTimes() {
        return currentContinuousSignTimes;
    }

    public void setCurrentContinuousSignTimes(int currentContinuousSignTimes) {
        this.currentContinuousSignTimes = currentContinuousSignTimes;
    }

    private List<SignItem> signItemList = new ArrayList<>();

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getContinuousSignTimes() {
        return continuousSignTimes;
    }

    public void setContinuousSignTimes(int continuousSignTimes) {
        this.continuousSignTimes = continuousSignTimes;
    }

    public int getCurrentSignIntegral() {
        return currentSignIntegral;
    }

    public void setCurrentSignIntegral(int currentSignIntegral) {
        this.currentSignIntegral = currentSignIntegral;
    }

    public int getNextSignIntegral() {
        return nextSignIntegral;
    }

    public void setNextSignIntegral(int nextSignIntegral) {
        this.nextSignIntegral = nextSignIntegral;
    }

    public boolean isSigned() {
        return signed;
    }

    public void setSigned(boolean signed) {
        this.signed = signed;
    }

    public List<SignItem> getSignItemList() {
        return signItemList;
    }

    public void setSignItemList(List<SignItem> signItemList) {
        this.signItemList = signItemList;
    }

    public static class SignItem {
        private int times;
        private int signIntegral;

        public int getTimes() {
            return times;
        }

        public void setTimes(int times) {
            this.times = times;
        }

        public int getSignIntegral() {
            return signIntegral;
        }

        public void setSignIntegral(int signIntegral) {
            this.signIntegral = signIntegral;
        }
    }
}
