package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.dto.CityAgentDTO;
import info.batcloud.fanli.core.enums.AgentStatus;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public interface CityAgentService {

    Paging<CityAgentDTO> search(SearchParam param);

    CityAgentDTO addAgent(AgentAddParam param);

    class AgentAddParam {
        private Long userId;
        private int year;
        private Long cityId;

        public Long getCityId() {
            return cityId;
        }

        public void setCityId(Long cityId) {
            this.cityId = cityId;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }
    }

    class SearchParam extends PagingParam {

        private String phone;
        private Boolean perpetual;

        private AgentStatus status;

        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date startUpdateTime;
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        private Date endUpdateTime;

        private String city;

        private Long cityId;

        public Long getCityId() {
            return cityId;
        }

        public void setCityId(Long cityId) {
            this.cityId = cityId;
        }
        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public AgentStatus getStatus() {
            return status;
        }

        public void setStatus(AgentStatus status) {
            this.status = status;
        }

        public Date getStartUpdateTime() {
            return startUpdateTime;
        }

        public void setStartUpdateTime(Date startUpdateTime) {
            this.startUpdateTime = startUpdateTime;
        }

        public Date getEndUpdateTime() {
            return endUpdateTime;
        }

        public void setEndUpdateTime(Date endUpdateTime) {
            this.endUpdateTime = endUpdateTime;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public Boolean getPerpetual() {
            return perpetual;
        }

        public void setPerpetual(Boolean perpetual) {
            this.perpetual = perpetual;
        }
    }

}
