package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.enums.MemberType;
import info.batcloud.fanli.core.enums.UserLevel;

import java.util.Date;

public class Member {

    private Long userId;
    private String nickname;
    private String phone;
    private String avatarUrl;

    private String city;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    private MemberType type;

    private UserLevel level;

    public String getLevelTitle() {
        return level == null ? null : level.getTitle();
    }

    public UserLevel getLevel() {
        return level;
    }

    public void setLevel(UserLevel level) {
        this.level = level;
    }

    public String getTypeTitle() {
        return getType() == null ? null : getType().getTitle();
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public MemberType getType() {
        return type;
    }

    public void setType(MemberType type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
