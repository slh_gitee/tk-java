package info.batcloud.fanli.core.helper;

import org.apache.commons.lang.StringUtils;

public class UrlHelper {

    public static String toUrl(String path) {
        if (StringUtils.isBlank(path)) {
            return null;
        }
        if (path.startsWith("//")) {
            return "http:" + path;
        }
        return path;
    }

    public static String toSSLUrl(String path) {
        if (StringUtils.isBlank(path)) {
            return null;
        }
        if (path.startsWith("//")) {
            return "https:" + path;
        }
        return path.replaceFirst("http:", "https:");
    }
}
