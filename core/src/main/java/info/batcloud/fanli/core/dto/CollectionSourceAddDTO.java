package info.batcloud.fanli.core.dto;

import info.batcloud.fanli.core.enums.CollectionSourceStatus;
import info.batcloud.fanli.core.enums.CollectionSourceType;

import javax.validation.constraints.NotNull;

public class CollectionSourceAddDTO {

    @NotNull
    private String title;

    @NotNull
    private CollectionSourceStatus status;

    private CollectionSourceType type;

    //抓取的最大页数
    @NotNull
    private int maxPage;

    //每页抓取的数量
    @NotNull
    private int pageSize;

    private String keyword;

    public CollectionSourceType getType() {
        return type;
    }

    public void setType(CollectionSourceType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CollectionSourceStatus getStatus() {
        return status;
    }

    public void setStatus(CollectionSourceStatus status) {
        this.status = status;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
