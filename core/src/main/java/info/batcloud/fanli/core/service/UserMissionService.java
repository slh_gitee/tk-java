package info.batcloud.fanli.core.service;

import info.batcloud.fanli.core.dto.UserMissionDTO;

import java.util.Date;
import java.util.List;

public interface UserMissionService {

    List<UserMissionDTO> assignMissionToUser(long userId);

    List<UserMissionDTO> findInProgressListByUserId(long userId);

    List<UserMissionDTO> findInProgressOrCompleteInDay(long userId, Date date);

}
