package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserAgentApplication;
import info.batcloud.fanli.core.enums.UserAgentApplicationStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;

public interface UserAgentApplicationRepository extends PagingAndSortingRepository<UserAgentApplication, Long>, JpaSpecificationExecutor<UserAgentApplication> {

    int countByApplyTimeGreaterThanAndStatus(Date startTime, UserAgentApplicationStatus status);

}
