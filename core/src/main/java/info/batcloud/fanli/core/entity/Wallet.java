package info.batcloud.fanli.core.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Wallet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;

    private Date createTime;

    private Date updateTime;

    //金币余额
    private float integral;

    //已消费的金币
    private float consumedIntegral;

    //获得的金币
    private float obtainedIntegral;

    //现金余额
    private float money;

    /**
     * 已消费的金额，用于购买商品，比如夺宝的商品等
     * */
    private float consumedMoney;

    /**
     * 提现的金额
     * */
    private float withdrawMoney;

    //总共获取的money
    private float obtainedMoney;

    @Version
    private int version;//版本

    public float getWithdrawMoney() {
        return withdrawMoney;
    }

    public void setWithdrawMoney(float withdrawMoney) {
        this.withdrawMoney = withdrawMoney;
    }

    public float getObtainedMoney() {
        return obtainedMoney;
    }

    public void setObtainedMoney(float obtainedMoney) {
        this.obtainedMoney = obtainedMoney;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public float getIntegral() {
        return integral;
    }

    public void setIntegral(float integral) {
        this.integral = integral;
    }

    public float getConsumedIntegral() {
        return consumedIntegral;
    }

    public void setConsumedIntegral(float consumedIntegral) {
        this.consumedIntegral = consumedIntegral;
    }

    public float getObtainedIntegral() {
        return obtainedIntegral;
    }

    public void setObtainedIntegral(float obtainedIntegral) {
        this.obtainedIntegral = obtainedIntegral;
    }

    public void setConsumedIntegral(int consumedIntegral) {
        this.consumedIntegral = consumedIntegral;
    }

    public void setObtainedIntegral(int obtainedIntegral) {
        this.obtainedIntegral = obtainedIntegral;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public float getConsumedMoney() {
        return consumedMoney;
    }

    public void setConsumedMoney(float consumedMoney) {
        this.consumedMoney = consumedMoney;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
}
