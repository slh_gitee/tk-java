package info.batcloud.fanli.core.tencent;

public class XgConfig {

    private String accessId;
    private String accessKey;
    private String secretKey;
    private IosEnv iosEnv;

    public IosEnv getIosEnv() {
        return iosEnv;
    }

    public void setIosEnv(IosEnv iosEnv) {
        this.iosEnv = iosEnv;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public enum IosEnv {
        DEV,PROD
    }
}
