package info.batcloud.fanli.core.service;

import com.ctospace.archit.common.pagination.Paging;
import com.ctospace.archit.common.pagination.PagingParam;
import info.batcloud.fanli.core.entity.TaobaoItemCat;

public interface TaobaoItemCatService {

    void syncFromTaobao();

    TaobaoItemCat findByName(String name, long parentCid);

    Paging<TaobaoItemCat> search(SearchParam findParam);

    class SearchParam extends PagingParam {
        private Long parentCid;
        private Long cid;
        private String name;
        private String relationPath;

        public String getRelationPath() {
            return relationPath;
        }

        public void setRelationPath(String relationPath) {
            this.relationPath = relationPath;
        }

        public Long getParentCid() {
            return parentCid;
        }

        public void setParentCid(Long parentCid) {
            this.parentCid = parentCid;
        }

        public Long getCid() {
            return cid;
        }

        public void setCid(Long cid) {
            this.cid = cid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
