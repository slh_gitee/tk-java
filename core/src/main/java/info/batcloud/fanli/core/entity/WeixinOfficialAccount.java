package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.WeixinOfficialAccountType;

import javax.persistence.*;

@Entity
public class WeixinOfficialAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Long userId;

    private String token;

    @Column(name = "encoding_aes_key")
    private String encodingAESKey;

    @Enumerated(EnumType.STRING)
    private WeixinOfficialAccountType type;

    private boolean deleted;

    public WeixinOfficialAccountType getType() {
        return type;
    }

    public void setType(WeixinOfficialAccountType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEncodingAESKey() {
        return encodingAESKey;
    }

    public void setEncodingAESKey(String encodingAESKey) {
        this.encodingAESKey = encodingAESKey;
    }
}
