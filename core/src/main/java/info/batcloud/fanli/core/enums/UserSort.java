package info.batcloud.fanli.core.enums;

import info.batcloud.fanli.core.context.StaticContext;

public enum UserSort implements EnumTitle {
    ID_DESC, ID_ASC, LOGIN_TIMES_DESC, LOGIN_TIMES_ASC, LOGIN_TIME_DESC, LOGIN_TIME_ASC, RELATION, ACTIVE_TIME_DESC;


    @Override
    public String getTitle() {
        return StaticContext.messageSource.getMessage(this.getClass().getSimpleName() + "."
                + this.name(), null, "", null);
    }
}
