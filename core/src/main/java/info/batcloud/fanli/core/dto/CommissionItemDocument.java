package info.batcloud.fanli.core.dto;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

public class CommissionItemDocument {

    private Long id;

    private String title;

    @JSONField(name = "pic_url")
    private String picUrl;

    private Float price;

    @JSONField(name = "origin_price")
    private Float originPrice;

    @JSONField(name = "shop_title")
    private String shopTitle;

    @JSONField(name = "list_time")
    private long listTime; //上架时间

    @JSONField(name = "delist_time")
    private long delistTime;//下架时间

    private String summary;

    private int sales;

    @JSONField(name = "shopcat_ids")
    private List<Long> shopcatIds;

    @JSONField(name = "channel_ids")
    private List<Long> channelIds; //频道id

    @JSONField(name = "ecom_plat")
    private String ecomPlat;

    @JSONField(name = "item_place")
    private String itemPlace; //货源地

    @JSONField(name = "commission_rate")
    private Float commissionRate;

    @JSONField(name = "high_commission")
    private int highCommission;

    private int coupon;

    @JSONField(name = "coupon_start_time")
    private long couponStartTime;

    @JSONField(name = "coupon_end_time")
    private long couponEndTime;

    @JSONField(name = "coupon_total_count")
    private int couponTotalCount;

    @JSONField(name = "coupon_remain_count")
    private int couponRemainCount;

    @JSONField(name = "coupon_value")
    private Float couponValue;

    private int recommend;

    @JSONField(name = "free_shipment")
    private int freeShipment;

    @JSONField(name = "jd_sale")
    private int jdSale;

    private String description;

    @JSONField(name = "has_description")
    private int hasDescription;

    @JSONField(name = "tbk_zt")
    private int tbkZt;

    public int getHasDescription() {
        return hasDescription;
    }

    public int getTbkZt() {
        return tbkZt;
    }

    public void setTbkZt(int tbkZt) {
        this.tbkZt = tbkZt;
    }

    public int isHasDescription() {
        return hasDescription;
    }

    public void setHasDescription(int hasDescription) {
        this.hasDescription = hasDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getFreeShipment() {
        return freeShipment;
    }

    public int getJdSale() {
        return jdSale;
    }

    public void setJdSale(int jdSale) {
        this.jdSale = jdSale;
    }

    public int isFreeShipment() {
        return freeShipment;
    }

    public void setFreeShipment(int freeShipment) {
        this.freeShipment = freeShipment;
    }

    public int getRecommend() {
        return recommend;
    }

    public void setRecommend(int recommend) {
        this.recommend = recommend;
    }

    public Float getCouponValue() {
        return couponValue;
    }

    public void setCouponValue(Float couponValue) {
        this.couponValue = couponValue;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getOriginPrice() {
        return originPrice;
    }

    public void setOriginPrice(Float originPrice) {
        this.originPrice = originPrice;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public int getSales() {
        return sales;
    }

    public void setSales(int sales) {
        this.sales = sales;
    }

    public List<Long> getShopcatIds() {
        return shopcatIds;
    }

    public void setShopcatIds(List<Long> shopcatIds) {
        this.shopcatIds = shopcatIds;
    }

    public List<Long> getChannelIds() {
        return channelIds;
    }

    public void setChannelIds(List<Long> channelIds) {
        this.channelIds = channelIds;
    }

    public String getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(String ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public String getItemPlace() {
        return itemPlace;
    }

    public void setItemPlace(String itemPlace) {
        this.itemPlace = itemPlace;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public long getCouponStartTime() {
        return couponStartTime;
    }

    public void setCouponStartTime(long couponStartTime) {
        this.couponStartTime = couponStartTime;
    }

    public long getCouponEndTime() {
        return couponEndTime;
    }

    public void setCouponEndTime(long couponEndTime) {
        this.couponEndTime = couponEndTime;
    }

    public long getCouponTotalCount() {
        return couponTotalCount;
    }

    public void setCouponTotalCount(int couponTotalCount) {
        this.couponTotalCount = couponTotalCount;
    }

    public int getCouponRemainCount() {
        return couponRemainCount;
    }

    public void setCouponRemainCount(int couponRemainCount) {
        this.couponRemainCount = couponRemainCount;
    }

    public long getListTime() {
        return listTime;
    }

    public void setListTime(long listTime) {
        this.listTime = listTime;
    }

    public long getDelistTime() {
        return delistTime;
    }

    public void setDelistTime(long delistTime) {
        this.delistTime = delistTime;
    }

    public int getHighCommission() {
        return highCommission;
    }

    public void setHighCommission(int highCommission) {
        this.highCommission = highCommission;
    }

    public int getCoupon() {
        return coupon;
    }

    public void setCoupon(int coupon) {
        this.coupon = coupon;
    }
}
