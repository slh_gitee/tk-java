package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.UserSetting;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserSettingRepository extends CrudRepository<UserSetting, Long> {

    UserSetting findByKeyAndUserId(String key, long userId);
    List<UserSetting> findByKey(String key);
}
