package info.batcloud.fanli.core.settings;

import java.io.Serializable;

public class RegisterSetting implements Serializable {

    //开始邀请验证，开启后，注册必须填写邀请码
    private boolean useInvitationCheck;

    //默认的邀请码
    private String defaultInvitationCode;

    //邀请码有效天数
    private int invitationValidDays;

    //注册现金奖励
    private float registerRewardMoney;

    //邀请人现金奖励
    private float inviterRewardMoney;

    public float getRegisterRewardMoney() {
        return registerRewardMoney;
    }

    public void setRegisterRewardMoney(float registerRewardMoney) {
        this.registerRewardMoney = registerRewardMoney;
    }

    public float getInviterRewardMoney() {
        return inviterRewardMoney;
    }

    public void setInviterRewardMoney(float inviterRewardMoney) {
        this.inviterRewardMoney = inviterRewardMoney;
    }

    public boolean isUseInvitationCheck() {
        return useInvitationCheck;
    }

    public void setUseInvitationCheck(boolean useInvitationCheck) {
        this.useInvitationCheck = useInvitationCheck;
    }


    public String getDefaultInvitationCode() {
        return defaultInvitationCode;
    }

    public void setDefaultInvitationCode(String defaultInvitationCode) {
        this.defaultInvitationCode = defaultInvitationCode;
    }

    public int getInvitationValidDays() {
        return invitationValidDays;
    }

    public void setInvitationValidDays(int invitationValidDays) {
        this.invitationValidDays = invitationValidDays;
    }
}
