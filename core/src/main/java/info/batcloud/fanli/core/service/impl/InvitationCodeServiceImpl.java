package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.entity.InvitationCode;
import info.batcloud.fanli.core.repository.InvitationCodeRepository;
import info.batcloud.fanli.core.service.InvitationCodeService;
import info.batcloud.fanli.core.service.SystemSettingService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service
public class InvitationCodeServiceImpl implements InvitationCodeService {

    @Inject
    private InvitationCodeRepository invitationCodeRepository;

    @Inject
    private SystemSettingService systemSettingService;

    @Override
    public void generate(long userId) {
        InvitationCode code = new InvitationCode();
        code.setUserId(userId);
    }
}
