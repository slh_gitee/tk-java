package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.BundleJsPackage;
import info.batcloud.fanli.core.enums.AppPlatform;
import info.batcloud.fanli.core.enums.BundleJsPackageStatus;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BundleJsPackageRepository extends PagingAndSortingRepository<BundleJsPackage, Long>, JpaSpecificationExecutor<BundleJsPackage> {

    BundleJsPackage findTopByPlatformAndPlatformVersionAndStatusOrderByIdDesc(AppPlatform platform, String appVersion, BundleJsPackageStatus status);
    BundleJsPackage findTopByPlatformAndStatusOrderByIdDesc(AppPlatform platform, BundleJsPackageStatus status);

    int countByPlatformAndPlatformVersionAndVersion(AppPlatform platform, String platformVersion, String version);


}
