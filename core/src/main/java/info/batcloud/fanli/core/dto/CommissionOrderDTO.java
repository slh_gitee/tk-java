package info.batcloud.fanli.core.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import info.batcloud.fanli.core.domain.stat.CommissionOrderAllotEarning;
import info.batcloud.fanli.core.enums.CommissionOrderStatus;
import info.batcloud.fanli.core.enums.EcomPlat;

import java.util.Date;

public class CommissionOrderDTO {

    private Long id;

    private String itemPicUrl;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date settledTime;

    private String itemTitle;

    private String itemId;

    private String shopTitle;

    private int itemNum;

    private Float price;

    private CommissionOrderStatus status;

    private EcomPlat ecomPlat;

    /**
     * 预估佣金金额
     */
    private Float estimateCommissionFee; //预估佣金

    /**
     * 预估佣金比例
     */
    private Float estimateCommissionRate;

    private Float commissionRate;

    private Float commissionFee;

    private Float payFee;

    private String orderNo;

    private Long userId;

    private String userAvatarUrl;

    private String userNickname;

    private String userPhone;

    private boolean allocated;

    private Float allotFee;

    public Float getAllotFee() {
        return allotFee;
    }

    public void setAllotFee(Float allotFee) {
        this.allotFee = allotFee;
    }

    public boolean isAllocated() {
        return allocated;
    }

    public void setAllocated(boolean allocated) {
        this.allocated = allocated;
    }

    private CommissionOrderAllotEarning allotEarning;

    public CommissionOrderAllotEarning getAllotEarning() {
        return allotEarning;
    }

    public void setAllotEarning(CommissionOrderAllotEarning allotEarning) {
        this.allotEarning = allotEarning;
    }

    public String getEcomPlatTitle() {
        return ecomPlat == null ? null : ecomPlat.getTitle();
    }

    public String getStatusTitle() {
        return status == null ? null : status.getTitle();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserAvatarUrl() {
        return userAvatarUrl;
    }

    public void setUserAvatarUrl(String userAvatarUrl) {
        this.userAvatarUrl = userAvatarUrl;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public Date getSettledTime() {
        return settledTime;
    }

    public void setSettledTime(Date settledTime) {
        this.settledTime = settledTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemPicUrl() {
        return itemPicUrl;
    }

    public void setItemPicUrl(String itemPicUrl) {
        this.itemPicUrl = itemPicUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getShopTitle() {
        return shopTitle;
    }

    public void setShopTitle(String shopTitle) {
        this.shopTitle = shopTitle;
    }

    public int getItemNum() {
        return itemNum;
    }

    public void setItemNum(int itemNum) {
        this.itemNum = itemNum;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public CommissionOrderStatus getStatus() {
        return status;
    }

    public void setStatus(CommissionOrderStatus status) {
        this.status = status;
    }

    public EcomPlat getEcomPlat() {
        return ecomPlat;
    }

    public void setEcomPlat(EcomPlat ecomPlat) {
        this.ecomPlat = ecomPlat;
    }

    public Float getEstimateCommissionFee() {
        return estimateCommissionFee;
    }

    public void setEstimateCommissionFee(Float estimateCommissionFee) {
        this.estimateCommissionFee = estimateCommissionFee;
    }

    public Float getEstimateCommissionRate() {
        return estimateCommissionRate;
    }

    public void setEstimateCommissionRate(Float estimateCommissionRate) {
        this.estimateCommissionRate = estimateCommissionRate;
    }

    public Float getCommissionRate() {
        return commissionRate;
    }

    public void setCommissionRate(Float commissionRate) {
        this.commissionRate = commissionRate;
    }

    public Float getCommissionFee() {
        return commissionFee;
    }

    public void setCommissionFee(Float commissionFee) {
        this.commissionFee = commissionFee;
    }

    public Float getPayFee() {
        return payFee;
    }

    public void setPayFee(Float payFee) {
        this.payFee = payFee;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
