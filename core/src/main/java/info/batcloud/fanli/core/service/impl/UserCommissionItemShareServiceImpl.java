package info.batcloud.fanli.core.service.impl;

import info.batcloud.fanli.core.domain.DdkTraceInfo;
import info.batcloud.fanli.core.dto.CommissionItemDTO;
import info.batcloud.fanli.core.dto.UserCommissionItemShareDTO;
import info.batcloud.fanli.core.enums.EcomPlat;
import info.batcloud.fanli.core.repository.PidCommissionShareRepository;
import info.batcloud.fanli.core.service.*;
import info.batcloud.fanli.core.settings.DdkAuthSetting;
import info.batcloud.fanli.core.settings.DdkSetting;
import info.batcloud.laxiaoke.open.request.pdd.ddk.DdkOauthGoodsPromUrlGenerateRequest;
import info.batcloud.pdd.sdk.PddClient;
import info.batcloud.pdd.sdk.domain.ddk.GoodsPromotionUrl;
import info.batcloud.pdd.sdk.request.ddk.OauthGoodsPromUrlGenerateRequest;
import info.batcloud.pdd.sdk.response.ddk.OauthGoodsPromUrlGenerateResponse;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Arrays;

@Service
public class UserCommissionItemShareServiceImpl implements UserCommissionItemShareService {

    private static Logger logger = LoggerFactory.getLogger(UserCommissionItemShareServiceImpl.class);

    @Inject
    private PidCommissionShareRepository pidCommissionShareRepository;

    @Inject
    private TaobaoUnionService taobaoUnionService;

    @Inject
    private ShortUrlService shortUrlService;

    @Inject
    private JdUnionService jdUnionService;

    @Inject
    private UserService userService;

    @Inject
    private CommissionItemService commissionItemService;

    @Inject
    private UrlMappingService urlMappingService;

    @Inject
    private JdUnionApiService jdUnionApiService;

    @Inject
    private SystemSettingService systemSettingService;

    @Inject
    private PddClient pddClient;

    @Override
    public UserCommissionItemShareDTO findValidUserCommissionItemShare(long commissionItemId, long userId) {
        CommissionItemDTO cid = commissionItemService.findById(commissionItemId);
        UserCommissionItemShareDTO dto = new UserCommissionItemShareDTO();
        switch (cid.getEcomPlat()) {
            case TAOBAO:
            case TMALL:
                String relationId = userService.findTaobaoRelationId(userId);
                String clickUrl = StringUtils.isNotEmpty(cid.getCouponClickUrl()) ? cid.getCouponClickUrl() : cid.getSourceItemClickUrl();
                if (clickUrl.indexOf("?") != -1) {
                    clickUrl += "&relationId=" + relationId;
                } else {
                    clickUrl += "?relationId=" + relationId;
                }
                dto.setClickUrl(clickUrl);
                dto.setShortUrl(clickUrl);
                dto.setCommissionItemId(commissionItemId);
                dto.setUserId(userId);
                dto.setTaobaoRelationId(relationId);
                return dto;
            case JD:
                String url = jdUnionService.getCouponUrl(userId, cid.getSourceItemId(), cid.getCouponClickUrl());
                url = StringUtils.defaultIfEmpty(url, cid.getCouponClickUrl());
                dto.setClickUrl(url);
                dto.setShortUrl(url);
                dto.setCommissionItemId(commissionItemId);
                dto.setUserId(userId);
                return dto;
            case PDD:
                OauthGoodsPromUrlGenerateRequest request = new OauthGoodsPromUrlGenerateRequest();
                DdkTraceInfo traceInfo = new DdkTraceInfo();
                traceInfo.setUserId(userId);
                request.setCustomParameters(traceInfo.toJson());
                request.setGenerateShortUrl(true);
                request.setGoodsIdList(Arrays.asList(cid.getSourceItemId()));
                DdkAuthSetting ddkAuthSetting = systemSettingService.findActiveSetting(DdkAuthSetting.class);
                DdkSetting ddkSetting = systemSettingService.findActiveSetting(DdkSetting.class);
                request.setPid(ddkSetting.getPid());
                OauthGoodsPromUrlGenerateResponse response = pddClient.execute(request, ddkAuthSetting.getAccessToken());
                GoodsPromotionUrl promotionUrl = response.getData().getGoodsPromotionUrlList().get(0);
                dto.setClickUrl(promotionUrl.getMobileUrl());
                dto.setShortUrl(promotionUrl.getMobileShortUrl());
                dto.setCommissionItemId(commissionItemId);
                dto.setUserId(userId);
                return dto;
            default:
                throw new RuntimeException("暂不支持" + cid.getEcomPlat().getTitle() + "宝贝分享");
        }
    }

    @Override
    public String findUserCommissionItemShareUrl(long commissionItemId, long userId) {
        CommissionItemDTO item = commissionItemService.findById(commissionItemId);
        if (item.getEcomPlat() == EcomPlat.JD) {
            //如果是京东的，直接返回转链后的url
            String url = jdUnionService.getCouponUrl(userId, item.getSourceItemId(), item.getCouponClickUrl());
            return url;
        } else if (item.getEcomPlat() == EcomPlat.PDD) {
            OauthGoodsPromUrlGenerateRequest request = new OauthGoodsPromUrlGenerateRequest();
            DdkTraceInfo traceInfo = new DdkTraceInfo();
            traceInfo.setUserId(userId);
            request.setCustomParameters(traceInfo.toJson());
            request.setGenerateShortUrl(true);
            request.setGoodsIdList(Arrays.asList(item.getSourceItemId()));
            DdkAuthSetting ddkAuthSetting = systemSettingService.findActiveSetting(DdkAuthSetting.class);
            DdkSetting ddkSetting = systemSettingService.findActiveSetting(DdkSetting.class);
            request.setPid(ddkSetting.getPid());
            OauthGoodsPromUrlGenerateResponse response = pddClient.execute(request, ddkAuthSetting.getAccessToken());
            return response.getData().getGoodsPromotionUrlList().get(0).getMobileShortUrl();
        }
        String url = urlMappingService.getCommissionItemShareUrl(commissionItemId, userId);
        return url;
    }
}
