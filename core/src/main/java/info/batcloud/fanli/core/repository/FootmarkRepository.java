package info.batcloud.fanli.core.repository;

import info.batcloud.fanli.core.entity.Footmark;
import info.batcloud.fanli.core.enums.FootmarkType;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FootmarkRepository extends PagingAndSortingRepository<Footmark, Long>, JpaSpecificationExecutor<Footmark> {

    Footmark findByUserIdAndItemIdAndType(long userId, long itemId, FootmarkType type);
    int countByUserIdAndItemIdAndType(long userId, long itemId, FootmarkType type);

    @Modifying
    void deleteByUserIdAndItemId(long userId, long itemId);

    @Modifying
    void deleteByUserId(long userId);
}
