package info.batcloud.fanli.core.entity;

import info.batcloud.fanli.core.enums.ActivityFreq;
import info.batcloud.fanli.core.enums.RedPacketStatus;
import info.batcloud.fanli.core.enums.WalletFlowDetailType;

import javax.persistence.*;
import java.util.Date;

@Entity
public class RedPacket {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private Date createTime;

    private Date updateTime;

    @Enumerated(EnumType.STRING)
    private ActivityFreq freq;

    private int validDayNum;//有效天数

    private int maxUserGrantNum;//最大发放次数

    /**
     * 总红包数量
     * */
    private int totalNum;

    /**
     * 已领取的数量
     * */
    private int drawNum;

    private int grantNum;

    private Date lastDrawTime;

    private float integral;//红包金额

    private float money;

    private Date startTime;

    private Date endTime;

    @ManyToOne(cascade = CascadeType.REFRESH)
    private Crowd crowd;

    @Enumerated(EnumType.STRING)
    private RedPacketStatus status;

    private WalletFlowDetailType moneyWalletFlowDetailType;

    private WalletFlowDetailType integralWalletFlowDetailType;

    public WalletFlowDetailType getMoneyWalletFlowDetailType() {
        return moneyWalletFlowDetailType;
    }

    public void setMoneyWalletFlowDetailType(WalletFlowDetailType moneyWalletFlowDetailType) {
        this.moneyWalletFlowDetailType = moneyWalletFlowDetailType;
    }

    public WalletFlowDetailType getIntegralWalletFlowDetailType() {
        return integralWalletFlowDetailType;
    }

    public void setIntegralWalletFlowDetailType(WalletFlowDetailType integralWalletFlowDetailType) {
        this.integralWalletFlowDetailType = integralWalletFlowDetailType;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public int getMaxUserGrantNum() {
        return maxUserGrantNum;
    }

    public void setMaxUserGrantNum(int maxUserGrantNum) {
        this.maxUserGrantNum = maxUserGrantNum;
    }

    public int getValidDayNum() {
        return validDayNum;
    }

    public void setValidDayNum(int validDayNum) {
        this.validDayNum = validDayNum;
    }

    public int getGrantNum() {
        return grantNum;
    }

    public void setGrantNum(int grantNum) {
        this.grantNum = grantNum;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Crowd getCrowd() {
        return crowd;
    }

    public void setCrowd(Crowd crowd) {
        this.crowd = crowd;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getTotalNum() {
        return totalNum;
    }

    public void setTotalNum(int totalNum) {
        this.totalNum = totalNum;
    }

    public int getDrawNum() {
        return drawNum;
    }

    public void setDrawNum(int drawNum) {
        this.drawNum = drawNum;
    }

    public Date getLastDrawTime() {
        return lastDrawTime;
    }

    public void setLastDrawTime(Date lastDrawTime) {
        this.lastDrawTime = lastDrawTime;
    }

    public float getIntegral() {
        return integral;
    }

    public void setIntegral(float integral) {
        this.integral = integral;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public RedPacketStatus getStatus() {
        return status;
    }

    public void setStatus(RedPacketStatus status) {
        this.status = status;
    }

    public ActivityFreq getFreq() {
        return freq;
    }

    public void setFreq(ActivityFreq freq) {
        this.freq = freq;
    }
}
