package info.batcloud.fanli.background.task.collector;

public interface Listener<T> {

    void onStart(T t);

    void onError(T t, String errorMsg);

    void onSuccess(T t);

}
