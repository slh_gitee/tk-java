package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.service.SellerPromotionItemOrderService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class SellerPromotionScheduler {

    @Inject
    private SellerPromotionItemOrderService sellerPromotionItemOrderService;

    /**
     * 抢购任务超时
     * */
    @Scheduled(cron = "${cron.sellerPromotionItemOrder.releaseWaitFillTradeTimeoutOrder}")
    public void releaseWaitFillTradeTimeoutOrder() {
        sellerPromotionItemOrderService.releaseWaitFillTradeTimeoutOrder();
    }

    /**
     * 待审核订单自动审核任务
     * */
    @Scheduled(cron = "${cron.sellerPromotionItemOrder.handleWaitVerifyTimeoutOrder}")
    public void handleWaitVerifyTimeoutOrder() {
        sellerPromotionItemOrderService.handleWaitVerifyTimeoutOrder();
    }

    /**
     * 审核失败订单自动取消任务
     * */
    @Scheduled(cron = "${cron.sellerPromotionItemOrder.handleVerifyFailTimeoutOrder}")
    public void handleVerifyFailTimeoutOrder() {
        sellerPromotionItemOrderService.handleVerifyFailTimeoutOrder();
    }

    /**
     * 自动处理返现任务
     * */
    @Scheduled(cron = "${cron.sellerPromotionItemOrder.autoRebateTimeoutOrder}")
    public void autoRebateTimeoutOrder() {
        sellerPromotionItemOrderService.autoRebateTimeoutOrder();
    }

}
