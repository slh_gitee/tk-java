package info.batcloud.fanli.background.task.scheduler;

import info.batcloud.fanli.core.entity.UserWithdraw;
import info.batcloud.fanli.core.enums.UserWithdrawStatus;
import info.batcloud.fanli.core.repository.UserWithdrawRepository;
import info.batcloud.fanli.core.service.UserWithdrawService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

@Component
public class WithdrawScheduler {

    @Inject
    private UserWithdrawService userWithdrawService;

    @Inject
    private UserWithdrawRepository userWithdrawRepository;

    @Scheduled(cron = "${cron.userWithdraw.fundTransfer}")
    public void fundTransfer() {
        List<UserWithdraw> userWithdrawList = userWithdrawRepository.findByStatusOrderByIdAsc(UserWithdrawStatus.WAIT_TRANSFER);
        for (UserWithdraw userWithdraw : userWithdrawList) {
            userWithdrawService.withdrawFundTransferById(userWithdraw.getId());
        }
    }

    @Scheduled(cron = "${cron.userWithdraw.retryFundTransfer}")
    public void fundRetryTransfer() {
        List<UserWithdraw> userWithdrawList = userWithdrawRepository.findByStatusOrderByIdAsc(UserWithdrawStatus.RETRY_TRANSFER);
        for (UserWithdraw userWithdraw : userWithdrawList) {
            userWithdrawService.withdrawFundTransferById(userWithdraw.getId());
        }
    }

}
