package info.batcloud.laxiaoke.open.request.pdd;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.TokenRefreshResponse;

public class TokenRefreshRequest implements LxkRequest<TokenRefreshResponse> {

    @Override
    public String getUri() {
        return "/ddk-api/refresh-token";
    }

    @Override
    public Class<TokenRefreshResponse> getResponseClass() {
        return TokenRefreshResponse.class;
    }
}
