package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class CouponGoods {

    private long cid;
    private long cid2;
    private long cid3;
    private long commissionShare;
    private List<Coupon> couponList;
    private String imageUrl;
    private long inOrderCount;
    private int isJdSale;
    private int isSeckill;
    private String materiaUrl;
    private float pcPrice;
    private long skuId;
    private String skuName;
    private long vid;
    private float wlCommissionShare;
    private float wlPrice;

    public long getCid() {
        return cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public long getCid2() {
        return cid2;
    }

    public void setCid2(long cid2) {
        this.cid2 = cid2;
    }

    public long getCid3() {
        return cid3;
    }

    public void setCid3(long cid3) {
        this.cid3 = cid3;
    }

    public long getCommissionShare() {
        return commissionShare;
    }

    public void setCommissionShare(long commissionShare) {
        this.commissionShare = commissionShare;
    }

    public List<Coupon> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<Coupon> couponList) {
        this.couponList = couponList;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public long getInOrderCount() {
        return inOrderCount;
    }

    public void setInOrderCount(long inOrderCount) {
        this.inOrderCount = inOrderCount;
    }

    public int getIsJdSale() {
        return isJdSale;
    }

    public void setIsJdSale(int isJdSale) {
        this.isJdSale = isJdSale;
    }

    public int getIsSeckill() {
        return isSeckill;
    }

    public void setIsSeckill(int isSeckill) {
        this.isSeckill = isSeckill;
    }

    public String getMateriaUrl() {
        return materiaUrl;
    }

    public void setMateriaUrl(String materiaUrl) {
        this.materiaUrl = materiaUrl;
    }

    public float getPcPrice() {
        return pcPrice;
    }

    public void setPcPrice(float pcPrice) {
        this.pcPrice = pcPrice;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public long getVid() {
        return vid;
    }

    public void setVid(long vid) {
        this.vid = vid;
    }

    public float getWlCommissionShare() {
        return wlCommissionShare;
    }

    public void setWlCommissionShare(float wlCommissionShare) {
        this.wlCommissionShare = wlCommissionShare;
    }

    public float getWlPrice() {
        return wlPrice;
    }

    public void setWlPrice(float wlPrice) {
        this.wlPrice = wlPrice;
    }

    public static class Coupon {
        private long beginTime;
        //券面额
        private float discount;
        //限额
        private float quota;
        private String link;
        private long endTime;

        public long getBeginTime() {
            return beginTime;
        }

        public void setBeginTime(long beginTime) {
            this.beginTime = beginTime;
        }

        public float getDiscount() {
            return discount;
        }

        public void setDiscount(float discount) {
            this.discount = discount;
        }

        public float getQuota() {
            return quota;
        }

        public void setQuota(float quota) {
            this.quota = quota;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public long getEndTime() {
            return endTime;
        }

        public void setEndTime(long endTime) {
            this.endTime = endTime;
        }
    }

}
