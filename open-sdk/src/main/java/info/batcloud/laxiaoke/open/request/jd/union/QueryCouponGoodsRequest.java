package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.QueryCouponGoodsResponse;

public class QueryCouponGoodsRequest extends AbstractLxkRequest<QueryCouponGoodsResponse> {

    private String skuIdList;
    private Integer pageIndex;
    private Integer pageSize;
    private Long cid3;
    private String goodsKeyword;
    private Double priceFrom;
    private Double priceTo;

    public String getSkuIdList() {
        return skuIdList;
    }

    public void setSkuIdList(String skuIdList) {
        this.skuIdList = skuIdList;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Long getCid3() {
        return cid3;
    }

    public void setCid3(Long cid3) {
        this.cid3 = cid3;
    }

    public String getGoodsKeyword() {
        return goodsKeyword;
    }

    public void setGoodsKeyword(String goodsKeyword) {
        this.goodsKeyword = goodsKeyword;
    }

    public Double getPriceFrom() {
        return priceFrom;
    }

    public void setPriceFrom(Double priceFrom) {
        this.priceFrom = priceFrom;
    }

    public Double getPriceTo() {
        return priceTo;
    }

    public void setPriceTo(Double priceTo) {
        this.priceTo = priceTo;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/query-coupon-goods";
    }

    @Override
    public Class<QueryCouponGoodsResponse> getResponseClass() {
        return QueryCouponGoodsResponse.class;
    }
}
