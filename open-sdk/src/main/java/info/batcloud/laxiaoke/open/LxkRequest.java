package info.batcloud.laxiaoke.open;

public interface LxkRequest<T extends LxkResponse> {

    String getUri();

    Class<T> getResponseClass();

}
