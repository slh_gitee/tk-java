package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.QueryOrderResponse;

public class QueryOrderRequest extends AbstractLxkRequest<QueryOrderResponse> {

    private long unionId;
    private String time;
    private int pageIndex;
    private int pageSize;

    public long getUnionId() {
        return unionId;
    }

    public void setUnionId(long unionId) {
        this.unionId = unionId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/query-order";
    }

    @Override
    public Class<QueryOrderResponse> getResponseClass() {
        return QueryOrderResponse.class;
    }
}
