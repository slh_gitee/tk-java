package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.laxiaoke.open.domain.jd.union.GetCodeByUnionIdResult;

public class CouponGetCodeByUnionIdResponse extends LxkResponse<GetCodeByUnionIdResult> {
}
