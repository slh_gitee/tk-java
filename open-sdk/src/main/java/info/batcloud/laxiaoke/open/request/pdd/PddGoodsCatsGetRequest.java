package info.batcloud.laxiaoke.open.request.pdd;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.PddGoodsCatsGetResponse;
import info.batcloud.pdd.sdk.PddRequest;
import info.batcloud.pdd.sdk.annotation.Param;
import info.batcloud.pdd.sdk.response.GoodsCatsGetResponse;

public class PddGoodsCatsGetRequest implements LxkRequest<PddGoodsCatsGetResponse> {

    private long parentCatId;

    public long getParentCatId() {
        return parentCatId;
    }

    public void setParentCatId(long parentCatId) {
        this.parentCatId = parentCatId;
    }

    @Override
    public String getUri() {
        return "/ddk-api/goods-cats-get";
    }

    @Override
    public Class<PddGoodsCatsGetResponse> getResponseClass() {
        return PddGoodsCatsGetResponse.class;
    }
}
