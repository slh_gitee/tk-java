package info.batcloud.laxiaoke.open.response.pdd.ddk;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.pdd.sdk.response.ddk.OrderListIncrementGetResponse;

public class DdkOrderListIncrementGetResponse extends LxkResponse<OrderListIncrementGetResponse> {
}
