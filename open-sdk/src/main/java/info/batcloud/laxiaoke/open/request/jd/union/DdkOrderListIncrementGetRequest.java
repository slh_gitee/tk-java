package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.ddk.DdkOrderListIncrementGetResponse;

public class DdkOrderListIncrementGetRequest implements LxkRequest<DdkOrderListIncrementGetResponse> {

    private long startUpdateTime;

    private long endUpdateTime;

    private String pid;

    private int pageSize;

    private int page;

    public long getStartUpdateTime() {
        return startUpdateTime;
    }

    public void setStartUpdateTime(long startUpdateTime) {
        this.startUpdateTime = startUpdateTime;
    }

    public long getEndUpdateTime() {
        return endUpdateTime;
    }

    public void setEndUpdateTime(long endUpdateTime) {
        this.endUpdateTime = endUpdateTime;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String getUri() {
        return "/ddk-api/order-list-increment-get";
    }

    @Override
    public Class<DdkOrderListIncrementGetResponse> getResponseClass() {
        return DdkOrderListIncrementGetResponse.class;
    }
}
