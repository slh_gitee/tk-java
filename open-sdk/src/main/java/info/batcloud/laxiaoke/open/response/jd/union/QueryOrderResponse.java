package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.laxiaoke.open.domain.jd.union.UnionOrderResult;

public class QueryOrderResponse extends LxkResponse<UnionOrderResult> {
}
