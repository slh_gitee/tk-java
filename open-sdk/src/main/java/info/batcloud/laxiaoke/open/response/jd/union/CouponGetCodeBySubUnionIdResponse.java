package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkResponse;
import info.batcloud.laxiaoke.open.domain.jd.union.GetCodeBySubUnionIdResult;

public class CouponGetCodeBySubUnionIdResponse extends LxkResponse<GetCodeBySubUnionIdResult> {
}
