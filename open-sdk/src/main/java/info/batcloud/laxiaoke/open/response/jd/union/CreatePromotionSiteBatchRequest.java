package info.batcloud.laxiaoke.open.response.jd.union;

import info.batcloud.laxiaoke.open.LxkRequest;

public class CreatePromotionSiteBatchRequest implements LxkRequest<CreatePromotionSiteBatchResponse> {

    private String spaceName;

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/create-promotion-site-batch";
    }

    @Override
    public Class<CreatePromotionSiteBatchResponse> getResponseClass() {
        return CreatePromotionSiteBatchResponse.class;
    }
}
