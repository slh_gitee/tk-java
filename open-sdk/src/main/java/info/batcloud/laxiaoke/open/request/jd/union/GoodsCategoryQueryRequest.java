package info.batcloud.laxiaoke.open.request.jd.union;

import info.batcloud.laxiaoke.open.AbstractLxkRequest;
import info.batcloud.laxiaoke.open.response.jd.union.GoodsCategoryQueryResponse;

public class GoodsCategoryQueryRequest extends AbstractLxkRequest<GoodsCategoryQueryResponse> {

    private Integer parentId;
    private Integer grade;

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    @Override
    public String getUri() {
        return "/jd-union-api/goods-category-query";
    }

    @Override
    public Class<GoodsCategoryQueryResponse> getResponseClass() {
        return GoodsCategoryQueryResponse.class;
    }
}
