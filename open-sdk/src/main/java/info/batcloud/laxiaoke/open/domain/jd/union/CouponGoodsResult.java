package info.batcloud.laxiaoke.open.domain.jd.union;

import java.util.List;

public class CouponGoodsResult {

    private int total;
    private List<CouponGoods> data;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CouponGoods> getData() {
        return data;
    }

    public void setData(List<CouponGoods> data) {
        this.data = data;
    }
}
