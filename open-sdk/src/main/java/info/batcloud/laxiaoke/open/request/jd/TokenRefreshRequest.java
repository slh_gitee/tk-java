package info.batcloud.laxiaoke.open.request.jd;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.jd.TokenRefreshResponse;

public class TokenRefreshRequest implements LxkRequest<TokenRefreshResponse> {

    @Override
    public String getUri() {
        return "/jd-union-api/refresh-token";
    }

    @Override
    public Class<TokenRefreshResponse> getResponseClass() {
        return TokenRefreshResponse.class;
    }
}
