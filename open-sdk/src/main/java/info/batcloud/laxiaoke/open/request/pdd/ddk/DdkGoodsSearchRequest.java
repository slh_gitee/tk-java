package info.batcloud.laxiaoke.open.request.pdd.ddk;

import info.batcloud.laxiaoke.open.LxkRequest;
import info.batcloud.laxiaoke.open.response.pdd.ddk.DdkGoodsSearchResponse;

public class DdkGoodsSearchRequest implements LxkRequest<DdkGoodsSearchResponse> {

    private String keyword;

    private Integer optId;

    private int page;

    private Integer pageSize;

    private int sortType;

    private boolean withCoupon;

    private Long catId;

    private String goodsIdList;

    private Long zsDuoId;

    private Integer merchantType;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Integer getOptId() {
        return optId;
    }

    public void setOptId(Integer optId) {
        this.optId = optId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public int getSortType() {
        return sortType;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
    }

    public boolean isWithCoupon() {
        return withCoupon;
    }

    public void setWithCoupon(boolean withCoupon) {
        this.withCoupon = withCoupon;
    }

    public Long getCatId() {
        return catId;
    }

    public void setCatId(Long catId) {
        this.catId = catId;
    }

    public String getGoodsIdList() {
        return goodsIdList;
    }

    public void setGoodsIdList(String goodsIdList) {
        this.goodsIdList = goodsIdList;
    }

    public Long getZsDuoId() {
        return zsDuoId;
    }

    public void setZsDuoId(Long zsDuoId) {
        this.zsDuoId = zsDuoId;
    }

    public Integer getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(Integer merchantType) {
        this.merchantType = merchantType;
    }

    @Override
    public String getUri() {
        return "/ddk-api/goods-search";
    }

    @Override
    public Class<DdkGoodsSearchResponse> getResponseClass() {
        return DdkGoodsSearchResponse.class;
    }
}
