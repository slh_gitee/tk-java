package info.batcloud.fanli.admin.controller.form;

import info.batcloud.fanli.core.enums.ShopcatStatus;

import javax.validation.constraints.NotNull;

public class ShopcatAddForm {

    @NotNull
    private String title;

    private String icon;

    private String keyword;

    private Boolean searchByKeyword;

    private long parentId;

    private Long refId;

    @NotNull
    private ShopcatStatus status;

    private Long taobaoCatId;

    private String taobaoCatName;

    private Long jdCatId;

    private String jdCatName;

    private Long pddCatId;

    private String pddCatName;

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public Long getPddCatId() {
        return pddCatId;
    }

    public void setPddCatId(Long pddCatId) {
        this.pddCatId = pddCatId;
    }

    public String getPddCatName() {
        return pddCatName;
    }

    public void setPddCatName(String pddCatName) {
        this.pddCatName = pddCatName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Boolean getSearchByKeyword() {
        return searchByKeyword;
    }

    public void setSearchByKeyword(Boolean searchByKeyword) {
        this.searchByKeyword = searchByKeyword;
    }

    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public ShopcatStatus getStatus() {
        return status;
    }

    public void setStatus(ShopcatStatus status) {
        this.status = status;
    }

    public Long getTaobaoCatId() {
        return taobaoCatId;
    }

    public void setTaobaoCatId(Long taobaoCatId) {
        this.taobaoCatId = taobaoCatId;
    }

    public String getTaobaoCatName() {
        return taobaoCatName;
    }

    public void setTaobaoCatName(String taobaoCatName) {
        this.taobaoCatName = taobaoCatName;
    }

    public Long getJdCatId() {
        return jdCatId;
    }

    public void setJdCatId(Long jdCatId) {
        this.jdCatId = jdCatId;
    }

    public String getJdCatName() {
        return jdCatName;
    }

    public void setJdCatName(String jdCatName) {
        this.jdCatName = jdCatName;
    }
}
