package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.enums.ItemSelectionStatus;
import info.batcloud.fanli.core.service.ItemSelectionService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/itemSelection")
public class ItemSelectionController {

    @Inject
    private ItemSelectionService itemSelectionService;

    @GetMapping("/search")
    public Object search(ItemSelectionService.SearchParam param) {
        return itemSelectionService.search(param);
    }

    @PostMapping
    @Permission(ManagerPermissions.ITEM_SELECTION_MANAGE)
    public Object add(ItemSelectionService.ItemSelectionAddParam param) {
        return itemSelectionService.addItemSelection(param);
    }

    @PutMapping("/{id}")
    @Permission(ManagerPermissions.ITEM_SELECTION_MANAGE)
    public Object update(@PathVariable long id, ItemSelectionService.ItemSelectionUpdateParam param) {
        itemSelectionService.updateItemSelection(id, param);
        return 1;
    }

    @PutMapping("/status/{id}/{status}")
    @Permission(ManagerPermissions.ITEM_SELECTION_MANAGE)
    public Object status(@PathVariable long id, @PathVariable ItemSelectionStatus status) {
        itemSelectionService.setStatusById(id, status);
        return 1;
    }

    @GetMapping("/collect/{id}")
    @Permission(ManagerPermissions.ITEM_SELECTION_MANAGE)
    public Object collect(@PathVariable long id) {
        return itemSelectionService.collectByItemSelectionId(id);
    }
}
