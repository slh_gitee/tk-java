package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.TaobaoPidService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/taobaoPid")
@Permission(ManagerPermissions.TAOBAO_PID_MANAGE)
public class PidController {

    @Inject
    private TaobaoPidService taobaoPidService;

    @PostMapping()
    public Object save(@RequestParam String value) {
        taobaoPidService.save(value);
        return true;
    }

}
