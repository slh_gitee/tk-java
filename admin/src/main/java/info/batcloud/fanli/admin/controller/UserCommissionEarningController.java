package info.batcloud.fanli.admin.controller;

import com.ctospace.archit.mybatis.plugin.interceptor.PagingHelper;
import info.batcloud.fanli.core.mapper.stat.UserCommissionEarningMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/user-commission-earning")
public class UserCommissionEarningController {

    @Inject
    private UserCommissionEarningMapper userCommissionEarningMapper;

    @GetMapping
    public Object userCommissionEarning(UserCommissionEarningMapper.UserEstimateStatParam param) {
        PagingHelper.setPage(param.getPage());
        PagingHelper.setPageSize(param.getPageSize());
        userCommissionEarningMapper.userCommissionEarning(param);
        return PagingHelper.take();
    }

}
