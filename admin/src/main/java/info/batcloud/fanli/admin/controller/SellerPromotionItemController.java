package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.SellerPromotionItemService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/sellerPromotionItem")
public class SellerPromotionItemController {

    @Inject
    private SellerPromotionItemService sellerPromotionItemService;

//    @PostMapping("")
//    public Object add(SellerPromotionItemService.SellerPromotionItemSaveParam param) {
//        this.sellerPromotionItemService.saveSellerPromotionItem(param);
//        return 1;
//    }

    @PutMapping("/{id}")
    @Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_MANAGE)
    public Object update(@PathVariable long id, SellerPromotionItemService.SellerPromotionItemUpdateParam param) {
        param.setId(id);
        this.sellerPromotionItemService.updateSellerPromotionItem(param);
        return 1;
    }

    @GetMapping("/search")
    public Object search(SellerPromotionItemService.SearchParam param) {
        return sellerPromotionItemService.search(param);
    }

    @DeleteMapping("/{id}")
    @Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_MANAGE)
    public Object delete(@PathVariable long id) {
        sellerPromotionItemService.deleteById(id, null);
        return true;
    }

    @PutMapping("/list/{id}")
    @Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_MANAGE)
    public Object list(@PathVariable long id) {
        sellerPromotionItemService.listById(id, null);
        return true;
    }

    @PutMapping("/delist/{id}")
    @Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_MANAGE)
    public Object delist(@PathVariable long id) {
        sellerPromotionItemService.delistById(id, null);
        return true;
    }

    @PutMapping("/verify/{id}")
    @Permission(ManagerPermissions.SELLER_PROMOTION_ITEM_MANAGE)
    public Object verify(@PathVariable long id, SellerPromotionItemService.VerifyParam param) {
        sellerPromotionItemService.verify(id, param);
        return true;
    }

}