package info.batcloud.fanli.admin.controller.form;

import info.batcloud.fanli.core.enums.ShopcatStatus;

import javax.validation.constraints.NotNull;

public class ShopcatUpdateForm {

    private String icon;

    private Long refId;

    @NotNull
    private String title;

    private String keyword;

    @NotNull
    private ShopcatStatus status;

    private boolean searchByKeyword;

    private Long taobaoCatId;

    private String taobaoCatName;

    private Long jdCatId;

    private String jdCatName;

    private Long pddCatId;

    private String pddCatName;

    public Long getRefId() {
        return refId;
    }

    public void setRefId(Long refId) {
        this.refId = refId;
    }

    public Long getPddCatId() {
        return pddCatId;
    }

    public void setPddCatId(Long pddCatId) {
        this.pddCatId = pddCatId;
    }

    public String getPddCatName() {
        return pddCatName;
    }

    public void setPddCatName(String pddCatName) {
        this.pddCatName = pddCatName;
    }

    public String getTaobaoCatName() {
        return taobaoCatName;
    }

    public void setTaobaoCatName(String taobaoCatName) {
        this.taobaoCatName = taobaoCatName;
    }

    public Long getJdCatId() {
        return jdCatId;
    }

    public void setJdCatId(Long jdCatId) {
        this.jdCatId = jdCatId;
    }

    public String getJdCatName() {
        return jdCatName;
    }

    public void setJdCatName(String jdCatName) {
        this.jdCatName = jdCatName;
    }

    public Long getTaobaoCatId() {
        return taobaoCatId;
    }

    public void setTaobaoCatId(Long taobaoCatId) {
        this.taobaoCatId = taobaoCatId;
    }

    public boolean isSearchByKeyword() {
        return searchByKeyword;
    }

    public void setSearchByKeyword(boolean searchByKeyword) {
        this.searchByKeyword = searchByKeyword;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public ShopcatStatus getStatus() {
        return status;
    }

    public void setStatus(ShopcatStatus status) {
        this.status = status;
    }
}
