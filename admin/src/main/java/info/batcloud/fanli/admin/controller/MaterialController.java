package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.MaterialStatus;
import info.batcloud.fanli.core.service.MaterialService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/material")
public class MaterialController {

    @Inject
    private MaterialService materialService;

    @PostMapping
    public Object create(MaterialService.MaterialCreateParam param) {
        materialService.createMaterial(param);
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, MaterialService.MaterialUpdateParam param) {
        materialService.updateMaterial(id, param);
        return 1;
    }

    @PutMapping("/status/{id}/{status}")
    public Object setStatus(@PathVariable long id, @PathVariable MaterialStatus status) {
        materialService.setStatus(id, status);
        return 1;
    }

    @GetMapping("/list")
    public Object list() {
        return materialService.findAll();
    }

    @GetMapping("/list/{status}")
    public Object listByStatus(@PathVariable MaterialStatus status) {
        return materialService.findByStatus(status);
    }

}
