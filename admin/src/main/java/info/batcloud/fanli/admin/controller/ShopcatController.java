package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.controller.form.ShopcatAddForm;
import info.batcloud.fanli.admin.controller.form.ShopcatUpdateForm;
import info.batcloud.fanli.core.dto.ShopcatDTO;
import info.batcloud.fanli.core.entity.Shopcat;
import info.batcloud.fanli.core.exception.BizException;
import info.batcloud.fanli.core.exception.ShopcatNameExistsException;
import info.batcloud.fanli.core.repository.ShopcatRepository;
import info.batcloud.fanli.core.service.ShopcatService;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import java.util.List;

@RestController
@RequestMapping("/api/shopcat")
public class ShopcatController {

    @Inject
    private ShopcatService shopcatService;

    @Inject
    private ShopcatRepository shopcatRepository;

    @PostMapping
    @Transactional
    public Object add(ShopcatAddForm form) {
        Shopcat shopcat = new Shopcat();
        shopcat.setIdx(shopcatRepository.countByParentId(form.getParentId()) + 1);
        shopcat.setKeyword(form.getKeyword());
        shopcat.setStatus(form.getStatus());
        shopcat.setTitle(form.getTitle());
        shopcat.setParentId(form.getParentId());
        shopcat.setSearchByKeyword(form.getSearchByKeyword() == null ? false : form.getSearchByKeyword());
        if (form.getRefId() != null) {
            shopcat.setRef(shopcatRepository.findOne(form.getRefId()));
        }
        try {
            shopcatService.saveShopcat(shopcat);
        } catch (ShopcatNameExistsException e) {
            throw new BizException("分类名称已经存在");
        }
        return 1;
    }

    @PutMapping("/{id}")
    public Object update(@PathVariable long id, ShopcatUpdateForm form) {
        Shopcat shopcat = new Shopcat();
        shopcat.setTitle(form.getTitle());
        shopcat.setKeyword(form.getKeyword());
        shopcat.setStatus(form.getStatus());
        shopcat.setId(id);
        if (form.getRefId() != null) {
            shopcat.setRef(shopcatRepository.findOne(form.getRefId()));
        }
        shopcat.setSearchByKeyword(form.isSearchByKeyword());
        try {
            shopcatService.updateShopcat(shopcat);
        } catch (ShopcatNameExistsException e) {
            throw new BizException("分类名称已经存在");
        }
        return 1;
    }

    @GetMapping("/no-icon/list")
    public Object noIconList() {
        return shopcatService.findValidNoIconList();
    }

    @PutMapping("/sort")
    @Transactional
    public Object sort(SortParam param) {
        for (int i = 0; i < param.getIdList().size(); i++) {
            Shopcat shopcat = shopcatRepository.findOne(param.getIdList().get(i));
            shopcat.setIdx(i);
            shopcatRepository.save(shopcat);
        }
        return 1;
    }

    public static class SortParam {
        private List<Long> idList;

        public List<Long> getIdList() {
            return idList;
        }

        public void setIdList(List<Long> idList) {
            this.idList = idList;
        }
    }

    @PutMapping("/icon/{id}")
    public Object setIcon(@PathVariable long id, @RequestParam String icon) {
        Shopcat sc = shopcatRepository.findOne(id);
        sc.setIcon(icon);
        shopcatRepository.save(sc);
        return 1;
    }

    @GetMapping("/list/{parentId}")
    public Object list(@PathVariable long parentId, @RequestParam(required = false) String title) {
        List<ShopcatDTO> shopcats;
        if (StringUtils.isNotBlank(title)) {
            shopcats = shopcatService.findValidByTitleLike("%" + title + "%");
        } else {
            shopcats = shopcatService.findValidByParentId(parentId);
        }
        for (ShopcatDTO shopcat : shopcats) {
            shopcat.setPathName(shopcatService.getPathName(shopcat.getPath()));
        }
        return shopcats;
    }

    @DeleteMapping("/{id}")
    public Object del(@PathVariable long id) {
        shopcatService.deleteById(id);
        return 1;
    }

}
