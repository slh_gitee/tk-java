package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.core.enums.UrlMappingCodes;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/urlMappingCodes")
public class UrlMappingCodesController {

    @GetMapping("/{key}")
    public UrlMappingVo help(@PathVariable String key) {
        try {
            UrlMappingCodes codes = UrlMappingCodes.valueOf(key);
            UrlMappingVo vo = new UrlMappingVo();
            vo.setCode(codes.name());
            vo.setTitle(codes.getTitle());
            vo.setHelp(codes.getHelp());
            return vo;
        } catch (Exception e) {
            return new UrlMappingVo();
        }
    }

    public static class UrlMappingVo {
        private String code;
        private String title;
        private String help;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getHelp() {
            return help;
        }

        public void setHelp(String help) {
            this.help = help;
        }
    }

}
