package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.controller.form.ChannelAddForm;
import info.batcloud.fanli.admin.controller.form.ChannelUpdateForm;
import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.ChannelService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/channel")
public class ChannelController {

    @Inject
    private ChannelService channelService;

    @GetMapping("/list")
    public Object list() {
        return channelService.findAll();
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @PostMapping
    public Object add(ChannelAddForm form) {
        return channelService.addChannel(form.getName());
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @PutMapping("/{id}")
    public Object update(@PathVariable long id, ChannelUpdateForm form) {
        channelService.updateChannel(id, form.getName());
        return 1;
    }

    @Permission(ManagerPermissions.CHANNEL_MANAGE)
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        channelService.deleteById(id);
        return 1;
    }
}
