package info.batcloud.fanli.admin.controller;

import com.ctospace.archit.common.pagination.Paging;
import info.batcloud.fanli.core.dto.CommissionItemSearchDTO;
import info.batcloud.fanli.core.item.collection.TaobaoSelectionSelectionCollector;
import info.batcloud.fanli.core.item.collection.TaobaoSelectionContext;
import info.batcloud.fanli.core.service.CommissionItemSearchService;
import info.batcloud.fanli.core.service.impl.CommissionItemSearchServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/commission-item/search")
public class CommissionItemSearchController {

    @Inject
    private CommissionItemSearchServiceImpl commissionItemSearchService;

    @Inject
    private TaobaoSelectionSelectionCollector taobaoSelectionCollector;

    @GetMapping
    public Object execute(Param param) {
        Paging<CommissionItemSearchDTO> paging = null;
        switch (param.getSearchType()) {
            case 0:
                paging = commissionItemSearchService.search(param); break;
            case 1:
                final Paging<CommissionItemSearchDTO> tmpPaging = Paging.empty(param.getPageSize());
                TaobaoSelectionContext context = new TaobaoSelectionContext();
                context.setPageSize(200);
                context.setStartPage(1);
                context.setSelectionId(param.getTaobaoSelectionId());
                taobaoSelectionCollector.collect(context, resultPaging ->
                        commissionItemSearchService.bindToCommissionItemSearchBOPaging(tmpPaging, resultPaging));
                paging = tmpPaging;
                paging.setTotal(paging.getResults().size());
                break;
        }
        return paging;
    }

    public static class Param extends CommissionItemSearchService.SearchParam {
        private int searchType;
        private Long taobaoSelectionId;

        public int getSearchType() {
            return searchType;
        }

        public void setSearchType(int searchType) {
            this.searchType = searchType;
        }

        public Long getTaobaoSelectionId() {
            return taobaoSelectionId;
        }

        public void setTaobaoSelectionId(Long taobaoSelectionId) {
            this.taobaoSelectionId = taobaoSelectionId;
        }
    }

}
