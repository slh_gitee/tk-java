package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.domain.BusinessResponse;
import info.batcloud.fanli.core.dto.FlashSaleItemDTO;
import info.batcloud.fanli.core.enums.FlashSaleItemStatus;
import info.batcloud.fanli.core.service.FlashSaleItemService;
import info.batcloud.fanli.core.service.FlashSaleRemindService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/flashSaleItem")
public class FlashSaleItemController {

    @Inject
    private FlashSaleItemService flashSaleItemService;

    @Inject
    private FlashSaleRemindService flashSaleRemindService;

    @GetMapping("/search")
    public Object search(FlashSaleItemService.SearchParam param) {
        return flashSaleItemService.search(param);
    }

   /* @GetMapping("/findbyid")
    public Object findById(Long id) {
        FlashSaleItemDTO findbyid= flashSaleItemService.findById(id);
        return findbyid;
    }*/

    @PutMapping("/{id}")
    public Object update(@PathVariable long id,FlashSaleItemService.FlashSaleItemUpdateParam param){
        flashSaleItemService.updateFlashSaleItem(id,param);
        return 1;
    }

    @PostMapping
    @Permission(ManagerPermissions.FLASH_SALE_ITEM_MANAGE)
    public Object save(FlashSaleItemService.FlashSaleItemAddParam param) {
        flashSaleItemService.addFlashSaleItem(param);
        return true;
    }

    @PutMapping("/status/{id}-{status}")
    @Permission(ManagerPermissions.DDK_AUTH_SETTING)
    public Object setStatus(@PathVariable long id, @PathVariable FlashSaleItemStatus status) {
        flashSaleItemService.setStatus(id, status);
        return true;
    }

    @PutMapping("/slogan/{id}")
    @Permission(ManagerPermissions.DDK_AUTH_SETTING)
    public Object setSlogan(@PathVariable long id, @RequestParam String slogan) {
        flashSaleItemService.setSlogan(id, slogan);
        return true;
    }


    @PutMapping("/remind/{id}")
    @PreAuthorize("hasRole('USER')")
    public Object doRemind(@PathVariable long id, @RequestParam(required = false) Long userId) {
        flashSaleRemindService.doFlashSaleItemRemind(id);
        return BusinessResponse.ok(true);
    }

    @GetMapping("/schedule-remind")
    public Object scheduleRemind() {
        flashSaleRemindService.scheduleRemind();
        return BusinessResponse.ok(true);
    }
}
