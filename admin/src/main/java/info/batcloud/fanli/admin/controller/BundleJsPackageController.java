package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.BundleJsPackageService;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/bundleJsPackage")
@Permission(ManagerPermissions.BUNDLE_JS)
public class BundleJsPackageController {

    @Inject
    private BundleJsPackageService bundleJsPackageService;

    @GetMapping("/search")
    public Object search(BundleJsPackageService.SearchParam param) {

        return bundleJsPackageService.search(param);
    }

    @PutMapping("/use-cache/{id}-{useCache}")
    public Object useCache(@PathVariable long id, @PathVariable boolean useCache) {
        bundleJsPackageService.setUseCache(id, useCache);
        return 1;
    }

    @PutMapping("/ios-show/{id}-{iosShow}")
    public Object iosShow(@PathVariable long id, @PathVariable boolean iosShow) {
        bundleJsPackageService.setIosShow(id, iosShow);
        return 1;
    }

    @PutMapping("/show-update/{id}-{showUpdate}")
    public Object showUpdate(@PathVariable long id, @PathVariable boolean showUpdate) {
        bundleJsPackageService.setShowUpdate(id, showUpdate);
        return 1;
    }

    @PostMapping
    public Object save(BundleJsPackageService.BundleJsPackageAddDto addDto) {
        bundleJsPackageService.saveBundleJsPackage(addDto);
        return 1;
    }

    @PutMapping("/{id}")
    public Object publish(@PathVariable long id) {
        bundleJsPackageService.publishBundleJsPackage(id);
        return 1;
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable long id) {
        bundleJsPackageService.deleteById(id);
        return 1;
    }


}
