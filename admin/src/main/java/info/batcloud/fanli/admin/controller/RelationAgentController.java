package info.batcloud.fanli.admin.controller;

import info.batcloud.fanli.admin.permission.ManagerPermissions;
import info.batcloud.fanli.admin.permission.annotation.Permission;
import info.batcloud.fanli.core.service.RelationAgentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/api/relationAgent")
public class RelationAgentController {

    @Inject
    private RelationAgentService agentService;

    @GetMapping("/search")
    public Object search(RelationAgentService.SearchParam param) {

        return agentService.search(param);
    }

    @PostMapping()
    @Permission(ManagerPermissions.RELATION_AGENT_MANAGE)
    public Object add(RelationAgentService.AgentAddParam param) {
        agentService.addAgent(param);
        return 1;
    }

}
