package info.batcloud.fanli.admin;

import com.zaxxer.hikari.HikariDataSource;
import info.batcloud.fanli.core.CommonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.sql.DataSource;

@SpringBootApplication
@EnableAutoConfiguration
@ServletComponentScan({"com.ctospace.archit.servlet", "info.batcloud.fanli"})
@EnableCaching
@ComponentScan({"info.batcloud.fanli"})
@EnableFeignClients(basePackages = "info.batcloud.fanli.core.client")
@EnableJpaRepositories(basePackages = {"info.batcloud.fanli.admin.repository"})
@EntityScan(basePackages = {"info.batcloud.fanli.core.entity", "info.batcloud.fanli.admin.entity"})
@Configuration
public class Application extends CommonConfig {

    @Bean
    @ConfigurationProperties(prefix = "datasource")
    public DataSource dataSource() {
        HikariDataSource dataSource = new HikariDataSource();
        return dataSource;
    }

    @Bean
    public CharacterEncodingFilter getCharacterEncodingFilter() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(true);
        return characterEncodingFilter;
    }
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
